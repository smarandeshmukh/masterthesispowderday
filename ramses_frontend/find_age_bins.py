import os
import sys
import fsps
import numpy as np
import matplotlib.pyplot as plt


if __name__=='__main__':

	#User input
	try:
		tol = float(sys.argv[1])
		out = sys.argv[2]
	except IndexError:
		print 'This calculates the age bins. The tolerance level dictates how close the SEDs from two neighbouring bins have to be.'
		print 'The user has to manually ensure that the SPS model used here and in the model input file generator is the same.'
		print 'Syntax: [script name] [tolerance] [output file]'
		print 'tolerance sets the criteria for the differences in the luminosities redder than the Lyman break. (in percentage)'
		raise IndexError('Check Syntax')
	except ValueError:
		raise ValueError('Tolerance has to be a float')

	
	#Get the available metallicites
	sp = fsps.StellarPopulation()
	available_z = np.array( sp.zlegend )

	#Parameters
	start_age = 1e-7 #in Gyr
	end_age = 13.7
	w_break = 911.268

	full_bin_space = []

	for i in range( len(available_z) )[:]:

		print 'Calculating age bins for z = ', available_z[i]

		#Generate the SP
		sp = fsps.StellarPopulation(sfh = 0, zmet = i + 1, add_neb_emission = True, dust_type = 1, add_agb_dust_model = True, add_stellar_remnants = True, add_dust_emission = True)
		
		#Calculate the luminosities for the first iteration
		age = start_age
		wave, flux = sp.get_spectrum( zmet = sp.params['zmet'], tage = age, peraa = True)
		iw_break = np.argmin( np.abs(wave - w_break) ) + 2
		lum1 = np.trapz( flux[iw_break:], wave[iw_break:] )

		#Define by how much the age should change
		add_age = 1
		age1 = age
		age2 = age + add_age

		#Initialize the arrays
		age_array = np.array([])

		#Append it to the age array
		age_array = np.append(age_array, age1)

		#Begin iteration
		while age1 < end_age:
			
			wave, flux = sp.get_spectrum( zmet = sp.params['zmet'], tage = age2, peraa = True)
			iw_break = np.argmin( np.abs(wave - w_break) ) + 2

			lum2 = np.trapz( flux[iw_break:], wave[iw_break:] )
			diff = np.abs( (lum2 - lum1) / lum1 )
			
			#Check if within tolerance
			if diff < tol:
				lum1 = lum2
				age1 = age2
				age_array = np.append(age_array, age1)
				add_age = 1.0
			else:
				add_age = add_age / 2.0

			age2 = age1 + add_age

		full_bin_space.append(age_array)
		print '\t Number of age bins:', len(age_array)

	max_length = 1
	for i in full_bin_space:
		if len(i) > max_length:
			max_length = len(i)

	print 'Max Length:', max_length

	#Now add extra zeros to all necessary arrays
	b = np.zeros(max_length)
	for array in full_bin_space:
		a = array
		if len(array) != max_length:
			a = np.append(array, np.zeros( max_length - len(array) ) )
		b = np.vstack( (b, a) )

	b = b[1:]
	
	np.savetxt(out, b)
