import numpy as np
import os
import sys
import h5py
from hyperion.util.constants import pc, lsun, msun, kpc
from hyperion.model import Model

#Important parameters
dust_to_metals = 0.4

#dust models locations
draine_55_30_A = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/d03_5.5_3.0_A.hdf5'
draine_40_40_A = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/d03_4.0_4.0_A.hdf5'
draine_31_60_A = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/d03_3.1_6.0_A.hdf5'
kmh94_31_full = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/kmh94_3.1_full.hdf5'
	#PAH dust files
big = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/big.hdf5'
usg = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/usg.hdf5'
vsg = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/vsg.hdf5'

#Which dust should I use?
dust_model = draine_31_60_A
PAH_flag = False

#In the case of PAH set the mass distribution of the different grain
mass_fractions_dust = np.array([0.8063, 0.1351, 0.0586]) #(big, vsg, usg)

def get_cell_walls(f):
	
	#Access the file and the gas coordinates and cell widths = smoothing length
	gas_coords = f['PartType0']['Coordinates'][:]
	gas_widths = f['PartType0']['SmoothingLength'][:]
	boxsize = f['Header'].attrs['Boxsize']
	h = f['Header'].attrs['HubbleParam']
	a = f['Header'].attrs['Time']
	print boxsize, h, a, gas_widths.shape

	#Rescale to cente of mass frame = center of box frame
	com_frame_coords = gas_coords - boxsize / 2
	#Correct for the h factor and change to physical coordinates
	trans_coords = com_frame_coords / h * a
	#Smoothing Length correction
	trans_widths = gas_widths / h * a

		
	#Get the extremeties of the walls
	cell_wall_1 = trans_coords + trans_widths[:, None] / 2
	cell_wall_2 = trans_coords - trans_widths[:, None] / 2
	
	#add them to one array
	cell_walls_x = np.append(cell_wall_1[:,0], cell_wall_2[:,0])
	cell_walls_y = np.append(cell_wall_1[:,1], cell_wall_2[:,1])
	cell_walls_z = np.append(cell_wall_1[:,2], cell_wall_2[:,2])

	#print trans_coords[:]
	#print cell_wall_1[:5]
	#print cell_wall_2[:5]
	#print np.amax(trans_widths), np.amin(trans_widths)
	
	#Remove duplicates
	cell_walls_unique_dirty_x =  np.unique( np.sort(cell_walls_x) )
	cell_walls_unique_dirty_y =  np.unique( np.sort(cell_walls_y) )
	cell_walls_unique_dirty_z =  np.unique( np.sort(cell_walls_z) )
	
	#Due to floating point errors, we need to merge walls that are actually the same but appear different
		#x
	cell_walls_clean_x = np.copy(cell_walls_unique_dirty_x)
	for i in range( len(cell_walls_unique_dirty_x) - 1 ):
		rel_dif = np.abs( cell_walls_unique_dirty_x[i] - cell_walls_unique_dirty_x[i+1] )
		if rel_dif  < 1e-3:
			cell_walls_clean_x[i] = np.nan	
	cell_walls_clean_x = cell_walls_clean_x[np.isfinite(cell_walls_clean_x)]
		#y
	cell_walls_clean_y = np.copy(cell_walls_unique_dirty_y)
	for i in range( len(cell_walls_unique_dirty_y) - 1 ):
		rel_dif = np.abs( cell_walls_unique_dirty_y[i] - cell_walls_unique_dirty_y[i+1] )
		if rel_dif  < 1e-3:
			cell_walls_clean_y[i] = np.nan	
	cell_walls_clean_y = cell_walls_clean_y[np.isfinite(cell_walls_clean_y)]
		#z
	cell_walls_clean_z = np.copy(cell_walls_unique_dirty_z)
	for i in range( len(cell_walls_unique_dirty_z) - 1 ):
		rel_dif = np.abs( cell_walls_unique_dirty_z[i] - cell_walls_unique_dirty_z[i+1] )
		if rel_dif  < 1e-3:
			cell_walls_clean_z[i] = np.nan	
	cell_walls_clean_z = cell_walls_clean_z[np.isfinite(cell_walls_clean_z)]
	
	#And return
	return cell_walls_clean_x, cell_walls_clean_y, cell_walls_clean_z

def set_dust_dens(f, x,y,z):

	#In the beginning set the density array to 0
	dens_arr = np.zeros( (len(x) - 1, len(y) - 1, len(z) - 1) )

	#Access the relevant particle fields
	coords = f['PartType0']['Coordinates'][:]
	rho_code = f['PartType0']['Density'][:]
	met = f['PartType0']['Metallicity'][:]

	#Do the coordinate transformations
	boxsize = f['Header'].attrs['Boxsize']
	h = f['Header'].attrs['HubbleParam']
	a = f['Header'].attrs['Time']
	coords = coords - boxsize / 2 #com frame
	coords = coords / h * a #h correction and convert to physical

	#Convert densities from (Msun/h) / (ckpc/h)**3 to g/cm3
	rho = rho_code * (msun / h) / (a*kpc/h)**3

	#We need to find in which cell each gas particle fits in and set the density there
	for i in range( len(coords) ):

		#Find the position of the left/bottom/near cell wall. Since the cell walls are sorted
		#we do pos_x - x and find the closest positive value to 0. Basically, we can
		#throw out all the negative values which will be at the tail end of the pos_x - x array.
		position = coords[i]

		x_diff = position[0] - x
		x_diff = x_diff[ x_diff > 0]
		x_ind = len(x_diff) - 1

		y_diff = position[1] - y
		y_diff = y_diff[ y_diff > 0]
		y_ind = len(y_diff) - 1

		z_diff = position[2] - z
		z_diff = z_diff[ z_diff > 0]
		z_ind = len(z_diff) - 1

		assert x[x_ind] < position[0]< x[x_ind + 1], 'Particle not in cell_x'
		assert y[y_ind] < position[1]< y[y_ind + 1], 'Particle not in cell_y'
		assert z[z_ind] < position[2]< z[z_ind + 1], 'Particle not in cell_z'

		dens_arr[x_ind, y_ind, z_ind] = rho[i] * met[i] 

	return dens_arr

if __name__ == '__main__':
	
	#User input
	try:
		hdf5name = sys.argv[1]
		out1 = sys.argv[2]
	except IndexError:
		print 'This sets up a model for the RT code from an hdf5file.'
		print 'Syntax: [script name] [hdf5file] [output]'
		raise IndexError('Check Syntax')
	assert os.path.exists(hdf5name), 'Could not find hdf5 file: %s' % hdf5name

	#Here access the hdf5 file
	f = h5py.File(hdf5name, 'r')
	print f.keys()
	print f['PartType0']['Coordinates'][:].shape
	print f['PartType4']['Coordinates'][:].shape

	#Get the cell walls here
	x,y,z = get_cell_walls(f)
	print x.shape, y.shape, z.shape

	#Set the density in each cell
	density_array = set_dust_dens(f, x,y,z).T

	#Initialize the model
	m = Model()

	#Convert the xyz to cgs and add the cartesian grid
	x_cgs = x * kpc
	y_cgs = y * kpc
	z_cgs = z * kpc
	m.set_cartesian_grid(x_cgs, y_cgs, z_cgs)

	#Add density grid to model.
	if PAH_flag == True:
		print 'Using the PAH models.'
		m.add_density_grid( mass_fractions_dust[0] * density, big)
		m.add_density_grid( mass_fractions_dust[1] * density, vsg)
		m.add_density_grid( mass_fractions_dust[2] * density, usg)
	else:
		print 'Using the Model:', dust_model
		m.add_density_grid( density_array, dust_model)

	#Here we add the luminosity sources
	

	#Junk
	source = m.add_point_source()
	source.luminosity = lsun
	source.temperature = 10000.
	source.position = (0., 0., 0.)
	
	image = m.add_peeled_images(sed=False)  # Images
	m.set_monochromatic(True, wavelengths=[0.2, 0.55, 1., 1.2, 1.4])
	m.set_n_initial_iterations(5)
	m.set_n_photons(initial = 1e6, imaging_sources=1e6, imaging_dust=1e6)
	image.set_viewing_angles( (0,45,90) , (0,90,180) )
	image.set_image_size(400, 400)
	image.set_image_limits( np.amin(x_cgs), np.amax(x_cgs), np.amin(x_cgs), np.amax(x_cgs) )

	#Parse the output file
	if os.path.splitext(out1)[1] != '.rtin':
		out1 = out1 + '.rtin'

	m.write(out1)

	"""
	x = np.array( [-pc, pc, pc/2] )
	y = np.linspace(-pc, pc, 3)
	z = np.linspace(-pc, pc, 3)
	m = Model()
	m.set_cartesian_grid(x,y,z)

	m.add_density_grid( 1e-27*np.ones( (len(x)-1,len(y)-1,len(z)-1) ), '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/kmh94copy.hdf5' )

	source = m.add_point_source()
	source.luminosity = lsun
	source.temperature = 10000.
	source.position = (0., 0., 0.)

	image = m.add_peeled_images(sed=False)  # Images
	m.set_monochromatic(True, wavelengths=[1., 1.2, 1.4])
	m.set_n_initial_iterations(5)
	m.set_n_photons(initial = 1e6, imaging_sources=1000, imaging_dust=1e5)
	image.set_viewing_angles( (0,45,90) , (0,90,180) )
	image.set_image_size(400, 400)
	image.set_image_limits( np.amin(x), np.amax(x), np.amin(x), np.amax(x) )

	#Parse the output file
	if os.path.splitext(out1)[1] != '.rtin':
		out1 = out1 + '.rtin'

	#Save the model
	m.write(out1)
	m.run( os.path.splitext(out1)[0] + '.rtout', mpi=True, n_processes = 64 )
	"""

