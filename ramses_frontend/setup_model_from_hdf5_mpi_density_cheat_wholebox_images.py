import numpy as np
import os
import sys
import h5py
from hyperion.util.constants import pc, lsun, msun, kpc
from hyperion.model import Model
from mpi4py import MPI
from astropy import cosmology
import matplotlib.pyplot as plt
import fsps
from astropy import units as u

#Important parameters
dust_to_metals = 0.4
age_bins = 20

#dust models locations
draine_55_30_A = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/d03_5.5_3.0_A.hdf5'
draine_40_40_A = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/d03_4.0_4.0_A.hdf5'
draine_31_60_A = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/d03_3.1_6.0_A.hdf5'
kmh94_31_full = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/kmh94_3.1_full.hdf5'
	#PAH dust files
big = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/big.hdf5'
usg = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/usg.hdf5'
vsg = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/hyperion-dust-0.1.0/dust_files/vsg.hdf5'

#Which dust should I use?
dust_model = draine_31_60_A
PAH_flag = True

#Filter Files
filter_paths = ['/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Subaru_FOCAS.B.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Subaru_FOCAS.V.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_u.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_g.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_r.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_i.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_z.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_IRAC.I1_3.6.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_IRAC.I2_4.5.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_IRAC.I3_5.8.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_IRAC.I4_8.0.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_MIPS.24mu.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_MIPS.70mu.dat',
				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_MIPS.160mu.dat'
#				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SCUBA2_450.dat',
#				'/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SCUBA2_850.dat'
				]

#In the case of PAH set the mass distribution of the different grain
mass_fractions_dust = np.array([0.8063, 0.1351, 0.0586]) #(big, vsg, usg)

def get_cell_walls(f):
	
	#Access the file and the gas coordinates and cell widths = smoothing length
	gas_coords = f['PartType0']['Coordinates'][:][::5]
	gas_widths = f['PartType0']['SmoothingLength'][:][::5]
	boxsize = f['Header'].attrs['BoxSize']
	h = f['Header'].attrs['HubbleParam']
	a = f['Header'].attrs['Time']

	#Rescale to cente of mass frame = center of box frame
	com_frame_coords = gas_coords - boxsize / 2
	#Correct for the h factor and change to physical coordinates
	trans_coords = com_frame_coords / h * a
	#Smoothing Length correction
	trans_widths = gas_widths / h * a

		
	#Get the extremeties of the walls
	cell_wall_1 = trans_coords + trans_widths[:, None] / 2
	cell_wall_2 = trans_coords - trans_widths[:, None] / 2
	
	#add them to one array
	cell_walls_x = np.append(cell_wall_1[:,0], cell_wall_2[:,0])
	cell_walls_y = np.append(cell_wall_1[:,1], cell_wall_2[:,1])
	cell_walls_z = np.append(cell_wall_1[:,2], cell_wall_2[:,2])

	#Remove duplicates
	cell_walls_unique_dirty_x =  np.unique( np.sort(cell_walls_x) )
	cell_walls_unique_dirty_y =  np.unique( np.sort(cell_walls_y) )
	cell_walls_unique_dirty_z =  np.unique( np.sort(cell_walls_z) )
	
	#Due to floating point errors, we need to merge walls that are actually the same but appear different
		#x
	cell_walls_clean_x = np.copy(cell_walls_unique_dirty_x)
	for i in range( len(cell_walls_unique_dirty_x) - 1 ):
		rel_dif = np.abs( cell_walls_unique_dirty_x[i] - cell_walls_unique_dirty_x[i+1] )
		if rel_dif  < 1e-3:
			cell_walls_clean_x[i] = np.nan	
	cell_walls_clean_x = cell_walls_clean_x[np.isfinite(cell_walls_clean_x)]
		#y
	cell_walls_clean_y = np.copy(cell_walls_unique_dirty_y)
	for i in range( len(cell_walls_unique_dirty_y) - 1 ):
		rel_dif = np.abs( cell_walls_unique_dirty_y[i] - cell_walls_unique_dirty_y[i+1] )
		if rel_dif  < 1e-3:
			cell_walls_clean_y[i] = np.nan	
	cell_walls_clean_y = cell_walls_clean_y[np.isfinite(cell_walls_clean_y)]
		#z
	cell_walls_clean_z = np.copy(cell_walls_unique_dirty_z)
	for i in range( len(cell_walls_unique_dirty_z) - 1 ):
		rel_dif = np.abs( cell_walls_unique_dirty_z[i] - cell_walls_unique_dirty_z[i+1] )
		if rel_dif  < 1e-3:
			cell_walls_clean_z[i] = np.nan	
	cell_walls_clean_z = cell_walls_clean_z[np.isfinite(cell_walls_clean_z)]

	#Finally, it may happen that one of the star particles do not fall in the grid. 
	#So extend the cell wall edges to the boxsize
	cell_walls_clean_x[0] = - boxsize/h * a / 2
	cell_walls_clean_x[-1] =  boxsize/h * a / 2
	cell_walls_clean_y[0] = - boxsize/h * a / 2
	cell_walls_clean_y[-1] =  boxsize/h * a / 2
	cell_walls_clean_z[0] = - boxsize/h * a / 2
	cell_walls_clean_z[-1] =  boxsize/h * a / 2
	
	#And return
	return cell_walls_clean_x, cell_walls_clean_y, cell_walls_clean_z

def set_dust_dens(f, x,y,z):

	#In the beginning set the density array to 0
	dens_arr = np.zeros( (len(x) - 1, len(y) - 1, len(z) - 1) )

	#Access the relevant particle fields
	coords = f['PartType0']['Coordinates'][:][::5]
	rho_code = f['PartType0']['Density'][:][::5]
	met = f['PartType0']['Metallicity'][:][::5]
	masses = f['PartType0']['Masses'][:][::5]
	hsml = f['PartType0']['SmoothingLength'][:][::5]

	#Do the coordinate transformations
	boxsize = f['Header'].attrs['BoxSize']
	h = f['Header'].attrs['HubbleParam']
	a = f['Header'].attrs['Time']
	coords = coords - boxsize / 2 #com frame
	coords = coords / h * a #h correction and convert to physical

	#Convert densities from (1e10*Msun/h) / (ckpc/h)**3 to g/cm3. We also cheat for the densities since there
	#seems to be an error in writing the gadget files at this time.
	rho_code_cheat = masses / hsml**3
	rho = rho_code_cheat * (1e10*msun / h) / (a*kpc/h)**3
	print 'Mean density:', np.mean(rho), 'Median Density:', np.median(rho)

	#We need to find in which cell each gas particle fits in and set the density there
	for i in range( len(coords) ):

		#Find the position of the closest left/bottom/near cell wall. Since the cell walls are sorted
		#we do pos_x - x and find the closest positive value to 0. Basically, we can
		#throw out all the negative values which will be at the tail end of the pos_x - x array.
		position = coords[i]

		x_diff = position[0] - x
		x_diff = x_diff[ x_diff > 0]
		x_ind = len(x_diff) - 1

		y_diff = position[1] - y
		y_diff = y_diff[ y_diff > 0]
		y_ind = len(y_diff) - 1

		z_diff = position[2] - z
		z_diff = z_diff[ z_diff > 0]
		z_ind = len(z_diff) - 1

		#Verify that particle is inside cell
		assert x[x_ind] < position[0]< x[x_ind + 1], 'Particle not in cell_x'
		assert y[y_ind] < position[1]< y[y_ind + 1], 'Particle not in cell_y'
		assert z[z_ind] < position[2]< z[z_ind + 1], 'Particle not in cell_z'

		dens_arr[x_ind, y_ind, z_ind] = rho[i] * met[i] * dust_to_metals

	return dens_arr

def add_sources(m, f, bin_age_file):
	
	#Access the stars
	stars = f['PartType4']
	coords = stars['Coordinates'][:]
	metallicities = stars['Metallicity'][:]
	age = stars['StellarFormationTime'][:]
	mass = stars['Masses'][:]

	#Coordinate transformations. Change to CoM frame which is the center of the box
	boxsize = f['Header'].attrs['BoxSize']
	h = f['Header'].attrs['HubbleParam']
	a = f['Header'].attrs['Time']
	coords = coords - boxsize / 2 #com frame
	coords = coords / h * a #h correction and convert to physical

	#We keep the masses in Msun
	mass = mass * 1.0e10 / h

	#Change the stellar ages from scale factor to Gyr. Stellar ages are written in terms of scale factor at the
	#formation time. Use current scale factor to get the age of each star particle. First get the 
	#current cosmology and blah blah
	a = f['Header'].attrs['Time']
	Om0 = f['Header'].attrs['Omega0']
	Ode0 = f['Header'].attrs['OmegaLambda']
	H0 = f['Header'].attrs['HubbleParam'] * 100.0
	cosmo = cosmology.LambdaCDM(H0 = H0, Om0 = Om0, Ode0 = Ode0)
	age_snap = cosmo.lookback_time( 1 / a - 1)
	print 'Lookback time to current snapshot:', age_snap

	age_wrt_today = cosmo.lookback_time( 1 / age - 1 ) #Gyr
	age_wrt_snapshot = age_wrt_today.value - age_snap.value #Gyr

	#Now the bin the ages. Get that from the file. See find_age_bins.py for details.
	agebins = np.loadtxt(bin_age_file)

	#Get the available metallicities
	test_sp = fsps.StellarPopulation()
	metal_available = np.array(test_sp.zlegend)

	#Initialize the parameter space of size ( len(metal_available), len(agebins) )
	sed_bin_ids = np.empty( (len(metal_available),len(agebins[0])), dtype=list)
	
	#Now iterate through each particle and get the closest the metal index and age index. Populate the
	#2d (metal,age) bins with the star ids.
	for i in range(len(metallicities)):
		#Get the closest  Z index
		Z_index = np.argmin( np.abs(metal_available - metallicities[i]) )
		#Get the closest agebin index using the metallicities
		age_index = np.argmin( np.abs(age_wrt_snapshot[i] - agebins[Z_index]) )

		if sed_bin_ids[Z_index, age_index] != None:
			sed_bin_ids[Z_index, age_index].append(i)
		else:
			sed_bin_ids[Z_index, age_index] = [i]

	z_bin_has_star = np.array([])
	age_bin_has_star = np.array([])
	#Find all the non None values in the (metal,age) array:
	for row in range( len(sed_bin_ids) ):
		for col in range( len(sed_bin_ids[row]) ):
			if sed_bin_ids[row, col] != None:
				z_bin_has_star = np.append(z_bin_has_star, int(row))
				age_bin_has_star = np.append(age_bin_has_star, int(col))

	no_of_sed_bins = len(z_bin_has_star)
	print 'Number of SED bins:', no_of_sed_bins

	#Now start assigning the SEDs. Divide into chunks for mpi.
	comm_master = MPI.COMM_WORLD
	slaves = min(comm_master.size - 1, no_of_sed_bins)
	chunk_indices = np.append([0], no_of_sed_bins * np.arange(1, slaves + 1) / slaves)
	
	#Send to slaves
	for i in range(len(chunk_indices) - 1 ):
		send_indices = np.array( [chunk_indices[i], chunk_indices[i+1]] )
		parcel = np.array( [ z_bin_has_star[send_indices[0]:send_indices[1] ], age_bin_has_star[send_indices[0]:send_indices[1]], send_indices[0], send_indices[1] ] )
		comm_master.send(parcel, dest = i+1)

	print 'All packets have been sent from the master node. Generating SEDs'

	seds_received = 0
	
	#Receive the SEDs from the slaves
	while seds_received != no_of_sed_bins:
		info = MPI.Status()
		gift = comm.recv(source = MPI.ANY_SOURCE, status = info)
		seds_received += 1

		#Now process the received SEDs
		wavelengths_mu = gift[0]
		flux = gift[1]
		l = 10**(gift[2])
		z_id = gift[3]
		age_id = gift[4]

		particles_in_this_bin =  sed_bin_ids[z_id, age_id]


		#Create the point source collection
		source  = m.add_point_source_collection()
		source.luminosity = float(l) * float(lsun) * np.array( mass[ particles_in_this_bin ], dtype = float)
		source.position = coords[ particles_in_this_bin ] * kpc
		source.spectrum = (3*10**14/wavelengths_mu[::-1], flux[::-1])

	m.set_sample_sources_evenly(True)

	return m

def read_viewing_angles(filename, cat):

	halo_id_col = 0
	num_angle_col = 9
	
	#Load the catalog
	c = np.loadtxt(cat)
	haloid = c[:,halo_id_col]
	
	#Try and get the haloid from the hdf5 filename. We expect the filename to like /../../gal1234.hdf5 so 1234 would be haloid
	gal_haloid = int( os.path.basename(filename).split('.')[0][-4:] )

	#Get the right index from the catalog file and the number of angles
	index =  np.where( gal_haloid == haloid)[0][0]
	num_of_angles = int( c[index][num_angle_col] )
	thetas = c[index][num_angle_col + 1: num_angle_col + 1 + num_of_angles]
	phis = c[index][num_angle_col + 1 + num_of_angles: num_angle_col + 1 + 2*num_of_angles]

	return np.array( [ thetas, phis ] )

if __name__ == '__main__':

	comm = MPI.COMM_WORLD

	if comm.rank == 0:
		
		#User input
		try:
			hdf5name = sys.argv[1]
			out1 = sys.argv[2]
			bin_age_file = sys.argv[3]
		except IndexError:
			print 'This sets up a model for the RT code from an hdf5file.'
			print 'Syntax: [script name] [hdf5file] [output] [bin_age_file] [optional=catalog file]'
			raise IndexError('Check Syntax')
		assert os.path.exists(hdf5name), 'Could not find hdf5 file: %s' % hdf5name
		assert os.path.exists(bin_age_file), 'Could not find hdf5 file: %s' % bin_age_file
		#You need at least 2 cores for the mpi to run. Node 0 is the master. (Like real master = assigns work but
		#does nothing himself)
		assert comm.size > 1, 'You need at least 2 (current %d) cores for the mpi to run.' % comm.size

		#Here access the hdf5 file
		f = h5py.File(hdf5name, 'r')
		print 'Number of cores:', comm.size
		print 'Number of gas particles:', len(f['PartType0']['Coordinates'][:][::5])
		print 'Number of stars:', len(f['PartType4']['Coordinates'][:])

		#Get the cell walls here
		x,y,z = get_cell_walls(f)
		print 'Cartesian grid shape and extents:',  x.shape, y.shape, z.shape,
		print (np.amin(x), np.amax(x)), (np.amin(y), np.amax(y)), (np.amin(z), np.amax(z))

		#Set the density in each cell
		density_array = set_dust_dens(f, x,y,z).T

		#Initialize the model
		m = Model()

		#Convert the xyz to cgs and add the cartesian grid
		x_cgs = x * kpc
		y_cgs = y * kpc
		z_cgs = z * kpc
		m.set_cartesian_grid(x_cgs, y_cgs, z_cgs)

		#Add density grid to model.
		if PAH_flag == True:
			print 'Using the PAH models.'
			m.add_density_grid( mass_fractions_dust[0] * density_array, big)
			m.add_density_grid( mass_fractions_dust[1] * density_array, vsg)
			m.add_density_grid( mass_fractions_dust[2] * density_array, usg)
		else:
			print 'Using the Model:', dust_model
			m.add_density_grid( density_array, dust_model)

		#Here we add the luminosity sources and have fun with mpi
		m = add_sources(m, f, bin_age_file)
		
		#Check if there an catalog file to find the necessary viewing angles. These would be a tuple = (thetas,phis)
		viewing_angles = np.array( [ [0,45,90], [0,0,0] ] )
		try:
			catalog = sys.argv[4]
			viewing_angles = read_viewing_angles(hdf5name, catalog)
			print 'Using viewing angles from file:',
		except:
			print 'No viewing angles read. Using default:',

		print viewing_angles

		#RT settings
		image = m.add_peeled_images(sed=False, image = True)  # SEDs
		m.set_n_initial_iterations(5)
		m.set_raytracing(False)
		m.set_n_photons(initial = 1e9, imaging=1e10)
		m.set_convergence(True,percentile=95.,absolute=2,relative=1.1)
		
		image.set_viewing_angles( viewing_angles[0], viewing_angles[1] )
		image.set_image_size(1000, 1000)
		image.set_image_limits( np.amin(x_cgs), np.amax(x_cgs), np.amin(x_cgs), np.amax(x_cgs) )


		#Load the filter files here
		for ifilter in range( len(filter_paths) ):
			
			fil_file = np.loadtxt( filter_paths[ifilter]  )
			wavelength_array = fil_file[:,0] / 10000
			fil_trans_array = fil_file[:,1]
			#wavelength_array = np.array( [0.4, 0.45, 0.5, 0.55, 0.6] )
			#fil_trans_array = np.array( [0.0, 50.0, 100.0, 60.0, 0.] )
			mean_wavelength = np.trapz( wavelength_array * fil_trans_array, wavelength_array) / np.trapz( fil_trans_array, wavelength_array)
			print mean_wavelength

			#Convolution with filter
			fil = image.add_filter()
			fil.name = filter_paths[ifilter].split('/')[-1]
			print filter_paths[ifilter].split('/')[-1]
			fil.spectral_coord = wavelength_array *  u.micron
			fil.transmission = fil_trans_array * u.percent
			fil.detector_type = 'energy'
			fil.alpha = 1.0
			fil.central_spectral_coord = mean_wavelength * u.micron


#		image.set_image_size(400, 400)
#		image.set_image_limits( np.amin(x_cgs), np.amax(x_cgs), np.amin(x_cgs), np.amax(x_cgs) )

		#Parse the output file
		if os.path.splitext(out1)[1] != '.rtin':
			out1 = out1 + '.rtin'

		m.write(out1)

		#-------------------------------------
		#Test if all sources are within grid
		f = h5py.File(out1)
		sources = f['Sources']
		geometry = f['Grid/Geometry']
		walls_extent_1 = np.array( [geometry['walls_1'][0][0], geometry['walls_1'][-1][0]] )
		walls_extent_2 = np.array( [geometry['walls_2'][0][0], geometry['walls_2'][-1][0]] )
		walls_extent_3 = np.array( [geometry['walls_3'][0][0], geometry['walls_3'][-1][0]] )
		
		print 'Wall Extents:', walls_extent_1, walls_extent_2, walls_extent_3

		for s in sources.values()[:]:
			xs = s['position'][:, 0]
			ys = s['position'][:, 1]
			zs = s['position'][:, 2]

			a1 = np.where( np.logical_or( xs < walls_extent_1[0], xs > walls_extent_1[1]) )[0]
			a2 = np.where( np.logical_or( ys < walls_extent_2[0], ys > walls_extent_2[1]) )[0]
			a3 = np.where( np.logical_or( ys < walls_extent_2[0], ys > walls_extent_2[1]) )[0]

			assert len(a1) == 0 and len(a2) == 0 and len(a3) == 0, 'Sources are not within the grid'
		#-------------------------------------

		print 'Done'

	#The slaves work here. They simply calculate the SEDs for different metallicities
	else:
		post = comm.recv(source = 0, tag = MPI.ANY_TAG)
		age_file = np.loadtxt(sys.argv[3])

		for i in range( len(post[0]) ):
			zmet_id = int(post[0][i])
			age_bin_id = int(post[1][i])
			star_age = age_file[zmet_id][age_bin_id]
			#This is the CRITICAL part. We decide here the type of SEDs we use
			sp = fsps.StellarPopulation(sfh=0, zmet = zmet_id + 1, add_neb_emission = True,  add_agb_dust_model = True, add_stellar_remnants = True, add_dust_emission = True, dust_type=2, dust1 = 1.0, dust2 = 0.2)
			wave, flux = sp.get_spectrum(tage = star_age, peraa = False )
			return_parcel = np.array( [wave/10000, flux, sp.log_lbol, zmet_id, age_bin_id] )
			comm.send(return_parcel, dest = 0)
		if comm.rank in [-1,-2,-3]:
			plt.plot(np.log10(wave), np.log10(flux*3.0*10**18/wave), label = 'Age: %f, met: %f' % (sp.params['tage'], sp.params['zmet']))
			plt.legend(loc = 2)
			plt.ylim( (-5, 5))
			plt.show()
