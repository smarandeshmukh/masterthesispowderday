#!/bin/bash

exe_file='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/ramses_frontend/setup_model_from_hdf5_mpi_density_cheat.py'
hdf5folder='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/04072016/snippets'
agebins_file='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/01082016/age_bins_with_dust1.gz'
cat_file='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/catalogs/cat_corr_h.txt'
hyp_inp_root_fol='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/05092016'

hdf5list=($hdf5folder/*.hdf5)

start=$[0]
#end=$[90]

#Uncomment for processing all files
end=${#hdf5list[@]}

sublist=${hdf5list[@]:$start:$end}
meh=($sublist)
num_of_files=${#meh[@]}
current=$[1]

#Get processor info
max_cores=$[`nproc --all`]
total_cores=$[`nproc --all`]

echo 'Total number of cores in this system:' $total_cores
echo 'Total number of files to process:' $num_of_files

#Uncomment this for setting a custom number for max cores
#max_cores=$[30]
echo 'Maximum number of cores to use:' $max_cores
min_cores=$[16]
echo 'Minimum number of cores to use:' $min_cores

#Remove existing directory and recreate it. (You can uncomment this)
#rm -rf $hyp_inp_root_fol
#mkdir $hyp_inp_root_fol

for galaxy in $sublist
do
	
	#-----------
	#Get number of free cores in this part
	load=(`cat /proc/loadavg`)
	echo 'Current Load:' $load
	free_cores_float=`bc -l <<< "scale=2; $total_cores-$load"`
	free_cores=${free_cores_float%.*}
	use_cores=32
	#-----------

	#Get galname from hdf5. Expect hdf5 to be like 'somewhere/gal1234.hdf5'
	filename=$(basename $galaxy)
	galname=${filename%.*}
	galid=${galname:(-4)}
	mkdir -p $hyp_inp_root_fol/$galid

	echo 'Galaxy name:' $galname
	mpiexec -n $use_cores python $exe_file $galaxy $hyp_inp_root_fol/$galid/$galname $agebins_file $cat_file

	echo 'Finished file' $current 'of' $num_of_files
	current=$[$current+1]
	echo

done

