#!/bin/bash

hyp_root_fol='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/31082016'
prefix='gal*.rtin'
output_fol='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/31082016'

inp_list=($hyp_root_fol/*)

start=$[130]
end=$[10]

#Uncomment for processing all files
#end=${#hdf5list[@]}

sublist=${inp_list[@]:$start:$end}
meh=($sublist)
num_of_files=${#meh[@]}
current=$[1]

#Get processor info
max_cores=$[`nproc --all`]
total_cores=$[`nproc --all`]

echo 'Total number of cores in this system:' $total_cores
echo 'Total number of files to process:' $num_of_files

#Uncomment this for setting a custom number for max cores
#max_cores=$[30]
echo 'Maximum number of cores to use:' $max_cores
min_cores=$[16]
echo 'Minimum number of cores to use:' $min_cores


for folname in $sublist
do
	inpname=$folname/$prefix
	#-----------
	#Get number of free cores in this part
	load=(`cat /proc/loadavg`)
	echo 'Current Load:' $load
	free_cores_float=`bc -l <<< "scale=2; $total_cores-$load"`
	free_cores=${free_cores_float%.*}
	use_cores=16
	echo 'Number of cores being used:' $use_cores
	#-----------

	#Get galname from hdf5. Expect hdf5 to be like 'somewhere/gal1234.hdf5'
	filename=$(basename $inpname)
	galname=${filename%.*}
	galid=${galname:(-4)}
	mkdir -p $output_fol/$galid

	echo 'Galaxy name:' $galname $output_fol/$galid/$galname.rtout
	hyperion -f -m $use_cores $inpname $output_fol/$galid/$galname.rtout

	echo 'Finished file' $current 'of' $num_of_files
	current=$[$current+1]
	echo
done
