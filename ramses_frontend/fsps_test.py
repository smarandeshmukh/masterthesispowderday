import os
import sys
import numpy as np
import fsps
import mpi4py
import matplotlib.pyplot as plt

if __name__=='__main__':

	print 'Hello World'

	use_met_index = 6

#	age = 3.0

	sp = fsps.StellarPopulation(sfh=0, zmet=use_met_index, add_neb_emission = True, dust_type = 1, add_agb_dust_model = True, add_stellar_remnants = True, dust1 = 1.0, dust2 = 0.2, add_dust_emission= True)

	fig = plt.figure(figsize = (16,9))
	ax = fig.add_subplot(1,1,1)

	for age in [0.008, 0.1608, 1, 2, 5, 8]:
		print
		sed = sp.get_spectrum(peraa = True, tage = age)
	#	print type(sed)

		wavelengths = sed[0]
		flux = sed[1]

	#	print type(wavelengths), type(flux)

	#	print wavelengths.shape, flux.shape

		stellar_mass = sp.stellar_mass
	#	print stellar_mass.shape
		
		lum = np.trapz(flux, wavelengths)

		print 'Luminosity:', lum

		print 'Total stellar mass:', np.sum(stellar_mass)

	#	print 'Metallicities:', sp.zlegend

#		print 'Metallicity index used and met used:', sp.params["zmet"], sp.zlegend[sp.params["zmet"]-1]

		print 'Age of SP:', age

#		print 'SFR:', sp.sfr

		print 'Dust Mass:', sp.dust_mass

		ax.plot( np.log10(wavelengths), np.log10(flux*wavelengths), label = 'Age = %f Gyr' % age )

	ax.set_ylim(-4,4)
	ax.legend(loc = 2)
	ax.grid(True, alpha = 0.4)

	ax.set_xlabel('$\mathrm{Wavelength\ log} (\lambda) \mathrm{[\dot{A}]}$ ', size = 24)
	ax.set_ylabel('$\mathrm{Flux\ log}\ (\lambda f_\lambda) $', size = 24)

	print wavelengths.shape, flux.shape
	print np.amax(flux), np.amin(flux)

	plt.show()
