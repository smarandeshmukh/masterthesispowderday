import numpy as np
import os
import sys
import h5py
import get_angles2

def main():

	#Check user input
	try:
		cat = sys.argv[1]
		hdf5name = sys.argv[2]
		out = sys.argv[3]
	except IndexError:
		print "Not enough arguments!"
		print 'Syntax: [script name] [catalog file] [hdf5 file] [output file]'
		raise Exception('Wrong inputs!')

	#check if hdf5 file exists
	if not os.path.exists(hdf5name):
		raise Exception('hdf5 file does not exist:' + hdf5name)
	#check if catalog file exists
	if not os.path.exists(cat):
		raise Exception('catalog file does not exist:' + cat)

	#Print user inputs
	print 'Your catalog file:', cat
	print 'HDF5 file:', hdf5name
	print 'Output catalog:', out

	#handle extra arguments here
	propdict = {}
	propdict['smass_col'] = 64
	propdict['rvir_col'] = 11
	propdict['pos_col'] = [5,6,7]
	propdict['vcm_col'] = [8,9,10]
	propdict['mass_threshold'] = 1e8
	propdict['gal_radius_f'] = 0.1

	#Load catalog into numpy array
	c = np.loadtxt(cat)
	smass_o = c[:, propdict['smass_col']]

	#Get mass selected indiced
	imasscut = np.where(smass_o > propdict['mass_threshold'])[0]
	print 'Inital mass selection. Mass threshold =', propdict['mass_threshold']
	print 'Number of halos above this mass threshold', imasscut.shape[0]
	#Get appropriate pos, and rvir from catalog
	gal_rvir = c[imasscut, propdict['rvir_col'] ] * propdict['gal_radius_f']
	print 'Galaxy radius definintion (fraction of halo virial radius):', propdict['gal_radius_f']

	#Get centers of each halo
	xc = c[imasscut, propdict['pos_col'][0]]
	yc = c[imasscut, propdict['pos_col'][1]]
	zc = c[imasscut, propdict['pos_col'][2]]
	#Get velocity of CoM of halos. Need this for calculation orientation of halo
	vxcm = c[imasscut, propdict['vcm_col'][0]]
	vycm = c[imasscut, propdict['vcm_col'][1]]
	vzcm = c[imasscut, propdict['vcm_col'][2]]

	#open hdf5 file
	f = h5py.File(hdf5name, 'r')
	spos = f['PartType4']['Coordinates'][:]
	smass = f['PartType4']['Masses'][:]
	h = f['Header'].attrs['HubbleParam']

	#Create the empty lists for storing pos, vel, rvir, angles and halo ids
	gal_pos = []
	gal_vel = []
	gal_rad = []
	gal_ang_theta = []
	gal_ang_phi = []
	halo_id = []
	gal_smass = []
	#User angles for viewing galaxy in galaxy frame
	utheta = np.array([0,45,90])
	uphi = np.array([0,0,0])
	#User angles for viewing galaxy in box frame
	def_theta = np.array([0,90,90])
	def_phi = np.array([0,0,90])
	#Need this to let the catalog know number of angles defined
	gal_num_angles = []

	#query the hdf5 file for particles within the galaxy for each mass selected halo
	print 'Selecting halos.....'
	for i in range( len(xc[:]) ):
		#Get center of ith halo
		center = [xc[i], yc[i], zc[i]]
		vcm = [ vxcm[i], vycm[i], vzcm[i] ]
		#Get particle indices within galaxy radius
		in_gal_bool = (np.sum((spos - center)**2, 1)) <= gal_rvir[i]**2
		in_gal_index = np.where(in_gal_bool == True)[0]
		#sum the mass to get the stellar mass within virial radius
		m = np.sum(smass[in_gal_index]) * 1e10 / h
		#if m is greater than threshold insert append into list with haloid,pos,vel,radius and angular info
		if m > propdict['mass_threshold']:
			gal_pos.append( np.array(center) )
			gal_vel.append( np.array(vcm) )
			gal_rad.append( gal_rvir[i] )
			#Powderday needs all angles to be defined in box frame. So convert galaxy angles to this frame
			theta, phi = get_angles2.main(gdgt_file=hdf5name, thetas=utheta, phis=uphi, rvir=gal_rvir[i], center=center, vcm=vcm, quiet=True)
			theta = np.append(theta, def_theta)
			phi = np.append(phi, def_phi)
			gal_num_angles.append(theta.shape[0])
			gal_ang_theta.append(theta)
			gal_ang_phi.append(phi)
			halo_id.append(imasscut[i])
			gal_smass.append(m)
			#Here add fields about position, vel, rvir, and angles
	
	print 'Done....'
	print 'Number of galaxies with M > mass_threshold (inside galaxy):', len(gal_smass)
	print 'Converting data to numpy and gathering.'

	#Convert lists to numpy arrays
	gal_pos = np.array(gal_pos)
	gal_vel = np.array(gal_vel)
	gal_rad = np.array(gal_rad)
	gal_num_angles = np.array(gal_num_angles)
	gal_ang_theta = np.array(gal_ang_theta)
	gal_ang_phi = np.array(gal_ang_phi)
	halo_id = np.array(halo_id)
	gal_smass = np.array(gal_smass)
	#Gather all arrays into a single numpy array
	table_cat =  np.concatenate( (halo_id[:,None],gal_smass[:,None],gal_pos,gal_vel,gal_rad[:,None],gal_num_angles[:,None],gal_ang_theta,gal_ang_phi), axis=1 )
	
	#Save to file
	print 'Saving new catalog....'
	np.savetxt(out, table_cat, header='id(0) star_mass(1) x(2) y(3) z(4) vx(5) vy(6) vz(7) gal_rad(8) gal_num_angles(9) (gal_ang_theta*[gal_num_angles]) (gal_ang_phi*[gal_num_angles])')
	print 'Finished!'

if __name__ == "__main__":
	main()
