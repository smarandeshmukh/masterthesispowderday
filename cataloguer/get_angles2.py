import sys
import h5py
import numpy as np
import math
import sympy


def main( gdgt_file=" ", thetas=np.array([0,  0,  0, 45, 45, 45, 90, 90, 90]), phis=np.array([0, 45, 90,  0, 45, 90,  0, 45, 90]), rvir=191.76, vir=0.1, center=np.array([9501.15954857, 2435.85220354,4776.67266284]), vcm=np.array([1.47,14.78,6.03]), quiet=True):

	#If you are using the terminal, the syntax is:
	#python [script name] [gadget hdf5 file]

	#Some global variables

	#If there is terminal input = standalone script
	if __name__ == '__main__':
		print "Stand alone script = Using Terminal Input"
		try:
			gdgt_file = sys.argv[1]
		except IndexError:
			print "Not enough arguments."
			print "Syntax: [script name] [gadget hdf5 file]"
			print 'Note: parameters are hard coded into the script.'
			raise Exception('Terminal Input Error')
	
	if not quiet:
		print "Your GADGET .hdf5 file is:", gdgt_file

	#Helper functions
	def get_center():
		return center

	def get_rvir():
		return rvir

	def get_vcm():
		return vcm

	def read_user_angles(theta, phi):
		#Convert the angle the user inputs to a cartesian unit vector
		t_rad = theta*2*sympy.pi/360
		p_rad = phi*2*sympy.pi/360
		vector = np.array( [sympy.sin(t_rad)*sympy.cos(p_rad), sympy.sin(t_rad)*sympy.sin(p_rad), sympy.cos(t_rad)] )
		vector = np.array([float(element) for element in vector])
		unit_vector = vector/np.linalg.norm(vector)
		return unit_vector

	def get_code_angles(vector, operator):
		#Convert user defined angles to the sim frame with the rotation operator
		unew = np.asarray(np.dot(operator, vector)).flatten()
		norm = np.linalg.norm(vector)
		theta = np.arccos(unew[2]/norm)
		if unew[0] == 0:
			phi = np.pi/2 * np.sign(unew[1])
		else:
			phi = np.arctan2(unew[1],unew[0])
		return np.degrees(theta), np.degrees(phi)

	#Main program STARTS HERE!!!
	f = h5py.File(gdgt_file, 'r')

	#Read .hdf5 file. I need only star particles.
	part_type = "PartType4" #Stars
	group = f[part_type]

	#First need to identify particles inside my galaxy
	coords = np.zeros(group['Coordinates'].shape)
	group['Coordinates'].read_direct(coords)
	R_gal = get_rvir()*vir

	if not quiet:
		print "You center is at:", get_center()
		print "You galaxy radius is:", R_gal
		print "The total number of star particles are:", coords.shape[0]

	#Basically select all particles inside the sphere of radius R_gal
	ids = np.where( np.sqrt( np.sum( (coords[:] - get_center() )**2, 1) < R_gal) )
	if not quiet:
		print "Number of particles inside your galaxy:",  len(ids[0])

	#Now get the vel and masses of particles inside r_gal
	vel = np.zeros(group['Velocities'].shape)
	mass = np.zeros(group['Masses'].shape)

	group['Velocities'].read_direct(vel)
	group['Masses'].read_direct(mass)

	gal_coords = coords[ids]
	gal_vel = vel[ids]
	gal_mass = mass[ids]

	#Next shift the coords and vel to the center of mass frame
	rel_coords = gal_coords[:] - get_center()
	rel_vel = gal_vel[:] - get_vcm()

	#Finally calculate the angular momenetum of each particle and sum to get total
	if not quiet:
		print "Calculating angular momentum..."
	ang_mom = np.array( [gal_mass[i]*np.cross(rel_coords[i], rel_vel[i]) for i in range(rel_coords.shape[0])] )
	tot_ang_mom = np.sum(ang_mom, 0)

	L = tot_ang_mom/np.linalg.norm(tot_ang_mom)
	if not quiet:
		print "The angular momentum of your galaxy is:", tot_ang_mom
		print "Normalized angular momentum vector:", L

	#For the user's comfort, we should allow him to work in the galaxy coordinate frame.
	#To do that we calculate the rotation operator, that allows easy transitions from one basis to another
	#Calculate the rotation operator from the simulation basis to the L basis
	iden = np.asmatrix(np.identity(3))
	e_z = np.array([0,0,1])
	c = np.dot(e_z, L)
	v =  np.cross(e_z, L)
	v_anti = np.array([ [0,-v[2],v[1]], [v[2],0,-v[0]], [-v[1],v[0],0] ])
	v_anti = np.asmatrix(v_anti)
	s = np.linalg.norm(v)
	R = iden + v_anti + np.dot(v_anti, v_anti) * (1-c)/(s**2)
	if not quiet:
		print "Calculating rotation operator:"
		print R

	#Convert the angles that the user wants to vectors
	a_v = [ read_user_angles(thetas[i],phis[i]) for i in range(len(thetas)) ]

	#Rotate these vector with the operator and convert to angles again. These can have repetitions.
	c_v = [ get_code_angles(a_v[i], R) for i in range(len(thetas)) ]

	#If the user is not careful there will be duplicates. Remove them, but then you
	#will lose order
	c_v_unsorted = list(set(c_v))

	#Choose which set you want to use (duplicates vs unsorted) and transpose for better presentation
	angles = np.asarray(c_v)
	angles = angles.transpose()
	if not quiet:
		print "Angles that you want (galaxy frame):"
		print "Thetas", thetas
		print "Phis", phis
		print "Angles that the code wants (box frame):"
		print "Thetas:", angles[0]
		print "Phis:", angles[1]

	#Close file
	f.close()

	#return thetas and phis
	return angles[0], angles[1]


if __name__=='__main__':
	main()
