import numpy as np
import h5py
import sys
import os
import shutil


def main():

	quiet = False

	try:
		cat = sys.argv[1]
		hdf5name = sys.argv[2]
		out = sys.argv[3]
	except:
		print 'Syntax: [script name] [catalog file] [hdf5 file] [output root folder]'
		raise Exception()

	if not quiet:
		print 'Catalog:', cat
		print 'Hdf5 file:', hdf5name
		print 'Output root folder:', out

	#Load catalog into numpy and important arrays
	c = np.loadtxt(cat)
	haloid = c[:,0]
	center = c[:,2:5]
	r = c[:,8]
	vel = c[:,5:8]
	
	#Load hdf5 file
	h = h5py.File(hdf5name, 'r')
	
	#Gas stuff
	g_coords = h['PartType0']['Coordinates'][:]
	g_dens = h['PartType0']['Density'][:]
	g_met = h['PartType0']['Metallicity'][:]
	g_smooth = h['PartType0']['SmoothingLength'][:]
	g_partids = h['PartType0']['ParticleIDs'][:]
	g_m = h['PartType0']['Masses'][:]
	#Star stuff
	s_coords = h['PartType4']['Coordinates'][:]
	s_m = h['PartType4']['Masses'][:]
	s_met = h['PartType4']['Metallicity'][:]
	s_partids = h['PartType4']['ParticleIDs'][:]
	s_age = h['PartType4']['StellarFormationTime'][:]
	s_vel = h['PartType4']['Velocities'][:]
	#Header
	

	#Clean the output folder
	if os.path.exists(out):
		shutil.rmtree(out)
	os.makedirs(out)

	#Iterate through all halos and select particles within rgal = sphere.
	for gal in range(len(haloid))[:]:
		outname = str("%04d" % haloid[gal])
		path = os.path.join(out) #, outname)
		new_hdf5_path = os.path.join(path, 'gal' + outname + '.hdf5')
		star_ids = np.where( np.sum( (s_coords - center[gal])**2, 1 ) < r[gal]**2 )[0]
		gas_ids = np.where( np.sum( (g_coords - center[gal])**2, 1 ) < r[gal]**2 )[0]
		#os.mkdir(path)
		o = h5py.File(new_hdf5_path, 'a')

		gas = o.create_group('PartType0')
		stars = o.create_group('PartType4')

		stars.create_dataset('Coordinates', data = s_coords[ star_ids ] - center[gal] + r[gal])
		stars.create_dataset('Masses', data = s_m[ star_ids ])
		stars.create_dataset('Metallicity', data = s_met[ star_ids ])
		stars.create_dataset('ParticleIDs', data = s_partids[ star_ids ])
		stars.create_dataset('StellarFormationTime', data = s_age[ star_ids ])
		stars.create_dataset('Velocities', data = s_vel[ star_ids ] - vel[gal])

		gas.create_dataset('Coordinates', data = g_coords[gas_ids] - center[gal] + r[gal])
		gas.create_dataset('Masses', data = g_m[gas_ids] )
		gas.create_dataset('Metallicity', data = g_met[gas_ids] )
		gas.create_dataset('ParticleIDs', data = g_partids[gas_ids] )
		gas.create_dataset('Density', data = g_dens[gas_ids] )
		gas.create_dataset('SmoothingLength', data = g_smooth[gas_ids] )

		h['Header'].copy(source=h['Header'], dest = o)
		del o['Header'].attrs['NumPart_Total']
		del o['Header'].attrs['NumPart_ThisFile']
		del o['Header'].attrs['BoxSize']

		o['Header'].attrs['NumPart_Total'] = [len(gas_ids), 0,0,0,len(star_ids),0]
		o['Header'].attrs['NumPart_ThisFile'] = [len(gas_ids), 0,0,0,len(star_ids),0]
		o['Header'].attrs['Boxsize'] = 2*r[gal]
		
		if not quiet:
			#print outname
			print path
			#print np.amin( stars['Coordinates'][:,0] ), np.amax( stars['Coordinates'][:,0] ), center[gal], r[gal]
			#print np.amin( stars['Coordinates'][:,1] ), np.amax( stars['Coordinates'][:,1] )
			#print np.amin( stars['Coordinates'][:,2] ), np.amax( stars['Coordinates'][:,2] )
			#print np.amin( gas['Coordinates'][:,0] ), np.amax( gas['Coordinates'][:,0] ), center[gal], r[gal]

		o.close()

	h.close()

if __name__ == '__main__':
	main()
