#!/bin/bash

echo Hello World

exe='/vol/aibn1058/data1/sdeshmukh/powderday2/pd/pd_front_end_pipeline_unc.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/03052016'
master_file='master'
model_file='model'
folders=`ls $inp_root`

echo Number of folders ${#folders[*]}
echo ${folders[0]}

start=$[0]
target=$[5]
machine='Science01'


full_list=($inp_root/*/)
sub_list=${full_list[@]:$start:$target}
counter=$[0]
current=$[0]


for fol in $sub_list
do
	echo $fol
#	echo -e ' \t ' `ls $fol`
	time python $exe $fol $master_file $model_file
	counter=$[$counter + $start +1]
	current=$[$current + 1]
	echo $counter $machine ": " $current " of " $target>>status_03052016.txt
done

#echo $sub_list
