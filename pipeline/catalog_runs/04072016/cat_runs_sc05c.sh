#!/bin/bash

echo Hello World

exe='/vol/aibn1058/data1/sdeshmukh/powderday2/pd/pd_front_end_pipeline_unc.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/04072016/params'
master_file='master'
model_file='model'
folders=`ls $inp_root`

echo Number of folders ${#folders[*]}
echo ${folders[0]}

start=$[61]
target=$[102]
machine='Science05'


full_list=($inp_root/*/)
sub_list=${full_list[@]:$start:$target}
counter=$[$start]
current=$[0]


for fol in $sub_list
do
	echo $fol
	start_time=`date +%s`
#	echo -e ' \t ' `ls $fol`
	time python $exe $fol $master_file $model_file
	end_time=`date +%s`
	current=$[$current + 1]
	echo $counter $machine ": " $current " of " $target "time taken: " $[$end_time-$start_time] "for halo: " $fol>>status_04072016.txt
	counter=$[$counter+1]
done

#echo $sub_list
