#!/bin/bash

echo Hello World

exe='/vol/aibn1058/data1/sdeshmukh/powderday2/pd/pd_front_end_pipeline_unc.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/29042016'
master_file='master'
model_file='model'
folders=`ls $inp_root`

echo Number of folders ${#folders[*]}
echo ${folders[0]}
full_list=($inp_root/*/)
sub_list=${full_list[@]:0:3}
counter=$[0]


for fol in $sub_list
do
	#echo $fol
#	echo -e ' \t ' `ls $fol`
	python $exe $master_file $model_file
	counter=$[$counter +1]
	echo $counter>>status.txt
done

#echo $sub_list
