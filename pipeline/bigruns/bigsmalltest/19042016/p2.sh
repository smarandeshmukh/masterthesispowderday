#!/bin/bash

bin_py='/vol/aibn1058/data1/sdeshmukh/powderday2/pd/pd_front_end_with_angles.py'
root_dir='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/19042016/2'
master_file='master'
model_file='model'
echo 'Front end location:' $bin_py
echo 'Master and model file location:' $root_dir
echo 'Master file:' $master_file
echo 'Model file:' $model_file

time python $bin_py $root_dir $master_file $model_file
