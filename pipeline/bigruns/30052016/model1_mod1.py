Auto_TF_file = 'snap'+'.logical'
Auto_dustdens_file = 'snap'+'.dustdens'

#SED wavelength and reolution
wav_res = 1000
wav_min = 0.0008
wav_max = 1000.0

#snapshot parameters
Gadget_snap_num = 200
"""
#hydro_dir = '/vol/aibn1058/data1/sdeshmukh/powderday2/pd/examples/gadget/'
hydro_dir = '/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/'

#Gadget_snap_name = 'snapshot_'+snapnum_str+'.hdf5'
Gadget_snap_name = 'huge.hdf5'

#where the files should go
PD_output_dir = '/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs'



#===============================================
#FILE I/O
#===============================================
inputfile = PD_output_dir+'/hugerun2'+'.rtin'
outputfile = PD_output_dir+'/hugerun2'+'.rtout'


#===============================================
#GRID POSITIONS
#===============================================
x_cent = 6000.0
y_cent = 6000.0
z_cent = 6000.0

THETAS = 0,45,90,0,45,90
PHIS = 0,0,0,90,90,90

wav_res = 300
wav_min = 0.0005
wav_max = 800.0
"""
