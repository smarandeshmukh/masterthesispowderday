import os
import sys
import numpy as np
import importlib
import shutil
import h5py


def main():

	halo_id_col = 0
	center_col = np.array([2,3,4])
	radius_col = 8
	num_angles_col = 9

	master_prefix = 'master.py'
	model_prefix = 'model.py'

	def zoom_box_len():
		hdf5file = h5py.File(hdf5name, 'r')
		h = hdf5file['Header'].attrs['HubbleParam']
		a = hdf5file['Header'].attrs['Time']
		return a*f[:,radius_col] / h #Need from comobing to proper = pkpc/h

	def n_ref():
		return 1

	def halo_id():
		return f[:,halo_id_col].astype(int)

	def center():
		cx = f[:,center_col[0]]
		cy = f[:,center_col[1]]
		cz = f[:,center_col[2]]
		return (np.array([cx, cy, cz])).transpose()

	def thetas():
		num_angles = int(f[0, num_angles_col])
		for angle in range(num_angles):
			if angle == 0:
				t = f[:, num_angles_col + angle + 1]
				t = t[:, None].transpose()
#				print t.shape
			else:
				new_thetas = f[:, num_angles_col + angle + 1]
				new_thetas = new_thetas[:, None].transpose()
				t = np.append(t, new_thetas, axis = 0)

		return t.transpose()
	
	def phis():
		num_angles = int(f[0, num_angles_col])
		for angle in range(num_angles):
			if angle == 0:
				t = f[:, num_angles_col + angle + num_angles  + 1]
				t = t[:, None].transpose()
#				print t.shape
			else:
				new_phis = f[:, num_angles_col + angle + num_angles + 1]
				new_phis = new_phis[:, None].transpose()
				t = np.append(t, new_phis, axis = 0)

		return t.transpose()
	prefix = ''

	#Check user input
	try:
		cat = sys.argv[1]
		root_fol = sys.argv[2]
		mas_sample = sys.argv[3]
		mod_sample =sys.argv[4]
		hdf5name = sys.argv[5]
		out_fol = sys.argv[6]
	except:
		print 'Syntax [script] [catalogue] [input_files_root_fol] [master_sample_file] [model_sample_file] [hdf5_file] [output_file_root_folder]'
		raise Exception('Not enough inputs')

	if len(sys.argv) > 7:
		print 'Extra arguments detected.'
		args = sys.argv[7:]
		if 'prefix' in args:
			prefix = args[args.index('prefix') + 1]

	#Print user input
	print 'Catalogue file:', cat
	print 'Root folder:', root_fol
	print 'Master sample:', mas_sample
	print 'Model sample:', mod_sample
	print 'Hdf5 file:', hdf5name
	print 'Prefix:', prefix
	print 'Output root folder:', out_fol

	#Access catalogue
	f = np.loadtxt(cat)
	num_galaxies = len(f)
	print 'Number of galaxies in your catalog:', num_galaxies
	
#	print thetas()[0:5], (thetas()).shape
#	print phis()[0:5], (phis()).shape

	#Open sample master file
	master = open(mas_sample)
	model = open(mod_sample)

	#Parse hdf5 file path
	hdf5path, hdf5file = os.path.split(hdf5name)
	hdf5path = hdf5path + '/'
	hdf5file = h5py.File(hdf5name, 'r')
	h = hdf5file['Header'].attrs['HubbleParam']
	a = hdf5file['Header'].attrs['Time']

	print 'Hdf5 path:', hdf5path
	print 'Hdf5 file:', hdf5file

	#Create the output folder
	if not os.path.exists(out_fol):
		os.makedirs(out_fol)
	else:
		shutil.rmtree(out_fol)
		os.makedirs(out_fol)
	
	for index in range( len(halo_id()) ):
		num = str(halo_id()[index]).zfill(4)
		path = os.path.join(root_fol,str(num))
		out_subdir = os.path.join(out_fol, num)
		if os.path.exists(path):
			print 'Removing existing', path
			shutil.rmtree(path)
		os.makedirs(path)
		if not os.path.exists(out_subdir):
			os.makedirs(out_subdir)

		#Create the master file
		ma = open(os.path.join(path,master_prefix), 'w')
		#Copy sample master file
		for line in master.readlines():
			ma.write(line)
			master.seek(0,0)
		#Add galaxy specific info
		ma.write('n_ref = ' + str(n_ref()) + ' #when n_particles > n_ref, octree refines further\n' )
		ma.write('zoom_box_len =' + str(zoom_box_len()[index]) + ' #kpc; so the box will be +/- zoom_box_len from the center')
		ma.close()
		
		#Create the model file
		mo = open(os.path.join(path,model_prefix), 'w')
		#Copy sample model file
		for line in model.readlines():
			mo.write(line)
			model.seek(0,0)

		#Write file locations onto model file
		#This is old
			#mo.write("hydro_dir = '" + str(hdf5path) + "'\n")
			#mo.write("Gadget_snap_name = '" + str(hdf5file) + "'\n")
		#This is new
		mo.write("hydro_dir = '" + '/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/04072016/snippets/'  + "'\n")
		mo.write("Gadget_snap_name = '" + 'gal'  + str(num) + ".hdf5'\n")

		mo.write("#where the files should go\n")
		mo.write("PD_output_dir = '" + str(out_subdir) + "/'\n")
		mo.write("inputfile = PD_output_dir+'gal'+'" + num + "'+'.rtin'\n")
		mo.write("outputfile = PD_output_dir+'gal'+'" + num + "'+'.rtout'\n")
		mo.write("#========\n#GRID POSITIONS AND ANGLES\n#========\n")
		
		#Center position
		c = center()[index]

		mo.write("x_cent = " + str( zoom_box_len()[index]*h/a ) + '\n')
		mo.write("y_cent = " + str( zoom_box_len()[index]*h/a ) + '\n')
		mo.write("z_cent = " + str( zoom_box_len()[index]*h/a ) + '\n')
		mo.write("THETAS = " + str( list(thetas()[index]) )[1:-1] + '\n' )
		mo.write("PHIS = " + str( list(phis()[index]) )[1:-1] + '\n' )

		mo.write("#LOGFILES\n")
		mo.write("logfile_sed = PD_output_dir+'log_sed' + '" + num + "'+'.txt'\n")
		mo.write("logfile_im = PD_output_dir+'log_im' + '" + num + "'+'.txt'\n")

		mo.close()
	"""
	o = open('test.py', 'w')

	#Copy sample master file
	for line in master.readlines():
		o.write(line)

	o.write('n_ref = ' + str(n_ref()) + ' #when n_particles > n_ref, octree refines further\n' )
	o.write('zoom_box_len =' + str(zoom_box_len()[0]) + ' #kpc; so the box will be +/- zoom_box_len from the center')

	master.close()
	o.close()
	"""












if __name__=='__main__':
	main()
