import numpy as np
import sys
import os

def calc_ratio(h, n):

	#Access all data columns. The 0th is the haloid.
	h = h[:, 1:]
	n = n[:, 1]
	#Transform so that the shape is compatible
	n = n[:, None]

	answer = h/n

	print answer

	return answer


if __name__=='__main__':
	
	#User input
	try:
		sfr_hist_file = sys.argv[1]
		sfr_norm_file = sys.argv[2]
		out = sys.argv[3]
	except IndexError:
		print 'This script will show the sfr history normailized with the integrated sfr at some characterstic time'
		print 'Syntax: [script name] [sfr hist file] [sfr norm file]'
		print 'Only the 2nd column from [sfr norm file] will be used'
		raise IndexError('Check your input')
	#Check
	assert os.path.exists(sfr_hist_file), '[sfr_hist_file] does not exist: %s' % sfr_hist_file
	assert os.path.exists(sfr_norm_file), '[sfr_norm_file] does not exist: %s' % sfr_norm_file

	hist = np.loadtxt(sfr_hist_file)
	norm = np.loadtxt(sfr_norm_file)
	assert len(hist) == len(norm), 'Input files have different shapes!!!'

	#Calculate the sfr_norm
	sfr_hist_norm = calc_ratio(hist, norm)
	print sfr_hist_norm.shape
	#Get the haloids
	haloids = norm[:,0]
	#print haloids
	
	
	#Print to file
	o = open(out, 'w')
	#Header
	year_bins = open(sfr_hist_file, 'r').readlines()[1]
	o.write('#The normalized sfr rates the galaxies\n')
	o.write('#Files used: History: %s  Norm: %s \n' % (sfr_hist_file, sfr_norm_file) )
	o.write(year_bins)

	for i in range(len(haloids)):
		o.write(str(haloids[i]) + ' '  + ' '.join(map(str, sfr_hist_norm[i]))  + '\n' )


