import numpy as np
from astropy.io import fits
from astropy.convolution import Gaussian2DKernel, convolve
import sys
import os



def main():
	print 'hi'


	try:
		inp = sys.argv[1]
		out = sys.argv[2]
		sigma = float(sys.argv[3])/3600
	except:
		raise Exception

	f = fits.open(inp)
#	print f.info()
#	print f[0].header
	data = f[0].data
	h = f[0].header
	print h['lat_min'], sigma
	print type(data), data.shape
	print h['NAXIS1']
	sigma_new = float(h['NAXIS1']) * sigma / (2*h['lat_max'])
	print sigma_new

	kernel = Gaussian2DKernel(stddev = sigma_new, x_size = 109, y_size = 109)
#	print kernel, kernel.array.shape, kernel.array
	new_data = convolve(data, kernel)
	fits.writeto(out, new_data, header = h, clobber = True, checksum = True)

	print np.sum(data), np.sum(new_data)

	f.close()







if __name__=='__main__':
	main()
