#!/bin/bash

echo 'Hello'

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/plot_seds.py'
root_fol='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/04072016'
out_name=''


a=($root_fol/*/*rtout.sed*)

echo 'Executable path:' $exe
echo 'Root folder of all image folders:' $root_fol

echo 'Number of images to make:' ${#a[@]}

current=$[1]

for sed_file in ${a[@]}
do
	output1=${sed_file%/*}$out_name/nu_l_nu.pdf
	output2=${sed_file%/*}$out_name/l_nu.pdf
	echo 'Processing file no. ' $current ' of ' ${#a[@]}
	python $exe $sed_file $output1 $output2 True
	current=$[$current + 1]
done
