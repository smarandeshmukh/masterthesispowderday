import numpy as np
import os
import sys
import glob

def main():

	try:
		inp_fol = sys.argv[1]
		mag_basename = sys.argv[2]
		inclination = int(sys.argv[3])
		filters = sys.argv[4]
		output = sys.argv[5]
	except:
		print 'Wrong Syntax'
		raise Exception('Wrong Syntax')

	print 'Input folder:', inp_fol
	print 'Magnitude file name:', mag_basename
	print 'Inclination id:', inclination
	print 'Filter ids:', filters
	print 'Output:', output

	mag_f_list = []

	filters = np.array( (filters.strip('[]')).split(',') ).astype(int)
	x_filters = np.array( [f for f in filters[:2] if f >= 0] )
	y_filters = np.array( [f for f in filters[2:] if f >= 0] )
	print x_filters, y_filters

	for root, subFolder, files in os.walk(inp_fol):
			for item in files:
				if item == mag_basename:
					mag_f_list.append(os.path.join(root, item))

	mag_f_list = np.array( mag_f_list )

	print mag_f_list.shape

#	filters = filters.split(',')
#	x_filters = np.array( filters[:2] )
#	y_filters = np.array(filters[2:])
#	print x_filters, y_filters

	x_mag_list = []
	y_mag_list = []

	for magfile in mag_f_list:
		d = np.loadtxt(magfile, dtype=str)
		dcol = (d[:, 1:]).astype(float)
		d = dcol[:, inclination]

		x_mag = d[x_filters[0]]
		if len(x_filters) == 2:
			x_mag = d[x_filters[0]] - d[x_filters[1]]
		y_mag = d[y_filters[0]]
		if len(y_filters) == 2:
			y_mag = d[y_filters[0]] - d[y_filters[1]]
	
		x_mag_list.append(x_mag)
		y_mag_list.append(y_mag)
	
	filter_name_list = (np.loadtxt(magfile, dtype=str))[:,0]

	x_filters_names = [filter_name_list[i] for i in x_filters  ]
	if len(x_filters_names) == 2:
		x_filters_names = str(x_filters_names[0]) + '-' + str(x_filters_names[1])
	else:
		x_filters_names = str(x_filters_names[0])
	
	y_filters_names = [filter_name_list[i] for i in y_filters  ]
	if len(y_filters_names) == 2:
		y_filters_names = str(y_filters_names[0]) + '-' + str(y_filters_names[1])
	else:
		y_filters_names = str(y_filters_names[0])

	print x_filters_names, y_filters_names

	o = open(output, 'w')
	o.close()
	o = open(output, 'a')
	o.write('#' + x_filters_names + ' ' + y_filters_names + ' (AB)' + '\n')

	for i in range( len(x_mag_list) ):
		o.write( str(x_mag_list[i]) + ' ' + str(y_mag_list[i]) + '\n')

	o.close()

	print (np.loadtxt(magfile, dtype=str))[:,0]


if __name__=='__main__':
	main()
