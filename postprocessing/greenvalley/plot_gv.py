import numpy as np
import matplotlib.pyplot as plt
import sys
import os

def main():
	try:
		inp = sys.argv[1]
		out1 = sys.argv[2]
		out2 = sys.argv[3]
		set_limit = sys.argv[4]
	except:
		raise Exception('Wrong Synatx')

	d = np.loadtxt(inp)
	x = d[:,0]
	y = d[:,1]

	f = open(inp)
	line1 = f.readline()
	f.close()

	line1 = line1[1:]
	strings = line1.split()
	print strings

	fig = plt.figure(figsize = (16,9))

	ax1 = fig.add_subplot(1,1,1)
	ax1.set_xlabel(strings[0])
	ax1.set_ylabel(strings[1])
	#whether to set limits
	if set_limit == 'True':
		ax1.set_xlim([-0.5,2])
		ax1.set_ylim([-0.5,6.5])
	ax1.plot(x,y, '+')
	fig.savefig(out1)

	fig2 = plt.figure(figsize = (16,9))
	ax2 = fig2.add_subplot(1,1,1)
	"""
	H, xedges, yedges = np.histogram2d(x,y,bins=10)
	ax2.scatter(x,y, alpha = 0.8)
	extent = [xedges[0],xedges[-1],yedges[0],yedges[-1]]
	uptop = np.amax(H)
	cset1 = ax2.contour(H,extent=extent, levels=[0.6*uptop])
	"""

	ax2.hist2d(x,y, bins = 15, cmin = 2)
#	ax2.colorbar()
	fig2.savefig(out2)






if __name__=='__main__':
	main()
