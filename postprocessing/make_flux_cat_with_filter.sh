#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/get_filt_flux_at_z.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/30052016'
filter_file='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/HST_WFPC2.f606w_V.dat'
hdf5file='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/hugemod.hdf5'
inclination='0'
output='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/30052016/flux_cat_V_z3109_with1500_2.txt'

file_list=($inp_root/*/*rtout.sed)

echo 'Number of files to process:' ${#file_list[@]}

rm -f $output

current=$[1]

echo '#Haloid(0) M(1) m(2) f_nu[erg/s/cm2/Hz](3) mono_lambda(4) M_mono(5) lambda_mean(6)'>>$output

for file in ${file_list[@]}
do
	echo 'Processing file no. ' $current ' of ' ${#file_list[@]}
	python $exe $file $filter_file $hdf5file $inclination $output True
	current=$[$current + 1]
		
done

