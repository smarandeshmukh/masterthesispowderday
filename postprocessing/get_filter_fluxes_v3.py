import numpy as np
from hyperion.model import ModelOutput
import os
import sys
from scipy.interpolate import interp1d
from scipy.integrate import simps
import matplotlib.pyplot as plt
from astropy import constants  as const

def main():
	
	quiet = True

	try:
		inp = sys.argv[1]
		fil_fol = sys.argv[2]
		output = sys.argv[3]
	except:
		print 'Not enough inputs'
		print 'Syntax:', '[script name] [input sed file] [filter root folder] [output file]'
		raise Exception()

	#Print user input here
	if not quiet:
		print 'Input sed:', inp
		print 'Filter transmission file:', fil_fol

	z = 3.109

	#Access sed file
	m = ModelOutput(inp)
	sed = m.get_sed(aperture=-1, distance=None, inclination='all', units='ergs/s')
	w = sed.wav
	nu = sed.nu
	flux = sed.val

	#Erase and open output file
	o = open(output, 'w')
	o.close()
	o = open(output, 'a')
	#Write header
	o.write('#Filter name #Faceon #45deg #Edge #sim_x #sim_y #sim_z \n')
	o.write('#Magnitudes are from the AB system \n')

	for filter_name in os.listdir(fil_fol):
		full_path = os.path.join(fil_fol, filter_name)

		#Access filter transmission file
		t = np.loadtxt(full_path, dtype=float)
		#Change units to microns and DO NOT correct for redshift
		t[:,0] = ( t[:,0] / 10**4 ) #/ (1. + z)

		#interpolate the filter transmission
		filt_intp = interp1d(t[:,0], t[:,1], kind='linear', bounds_error=False, fill_value=0)

		#Calculate the mean wavelength
		lambda_mean = np.trapz( t[:,1]*t[:,0], t[:,0] ) / np.trapz(t[:,1], t[:,0])
		#Convert to angstrom
		lambda_mean = lambda_mean * 10000

		if not quiet:
			print full_path, 'mean lambda:', lambda_mean

		mags_string = ''

		for inclination in range( len(flux) ):

			if not quiet:
				print '\t', inclination

			#change flux to flux density
			flux_dens = flux[inclination,:] / w

			#interpolate sed
			sed_intp = interp1d(w, flux_dens, kind='linear', bounds_error=False, fill_value=0)
			
			#Set resolution
			wavelength_coverage = np.arange(np.amin(w), np.amax(w), 0.001)
			
			#Calculate flux going through filter
			fil_flux = simps( sed_intp(wavelength_coverage) * filt_intp(wavelength_coverage), wavelength_coverage )

			#Define constant c
			c = const.c.to('micron/s').value

			#Divide by norm to get flux density
			flux_in_band_dens_nu = fil_flux / np.abs( simps( filt_intp(wavelength_coverage), (c)/ wavelength_coverage ) )
			mag = -2.5*np.log10( ( flux_in_band_dens_nu*10**23 / ( 4*3.14*(3.08567*10**19)**2 ) ) / 3631 )

			mags_string = mags_string + str(mag) + ' '

		#Write to file
		o.write(filter_name + ' ' + mags_string + '\n')

	#Close output file
	o.close()

if __name__=='__main__':
	main()
