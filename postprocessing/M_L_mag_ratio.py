import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy import stats


def main():

	try:
		mass_file = sys.argv[1]
		mag_file = sys.argv[2]
		out = sys.argv[3]
	except:
		raise Exception

	mag = np.loadtxt(mag_file)
	mass = np.loadtxt(mass_file)

	print mass.shape, mag.shape

	mass_arr = mass[:,1]
	mag_arr = mag[:,5]

	#Do linear regression to get linear fit
	outputs = stats.linregress(mag_arr,np.log10(mass_arr) )
	slope = outputs[0]
	intercept = outputs[1]
	sigma = outputs[4]
	

	#Acces literature data
	l = np.loadtxt('Gonzalez2011.csv')
	l_err_up = np.loadtxt('Gonzalez2011_err_up.csv')
	l_err_down = np.loadtxt('Gonzalez2011_err_down.csv')
	l_fit = np.loadtxt('Gonzalez2011_err_fit.csv')

	#Get slope of fit
	m = (l_fit[0,1] - l_fit[1,1]) / (l_fit[0,0] - l_fit[1,0])
	p = l_fit[0,1] - m*l_fit[0,0]
	print m,p
	
	y_err_up = np.abs(l_err_up[:,1] - l[:,1])
	yerr_down = np.abs(l_err_down[:,1] - l[:,1])

	real_w = int(mag[0,4] * 10000)

	fig =plt.figure( figsize=(16,9) )
	ax = fig.add_subplot(1,1,1)

	ax.set_yscale("linear")

	#Plot our data
	ax.scatter(mag_arr, np.log10(mass_arr), label = 'STD_DYN_3, slope = ' + str("%.2f" % slope) , color='r')
	ax.plot([np.amin(mag_arr), np.amax(mag_arr)], [np.amin(mag_arr)*slope + intercept, np.amax(mag_arr)*slope + intercept], 'r', linestyle='dotted' )

	#Plot literature
	ax.errorbar(l[:,0], l[:,1], yerr=(y_err_up, yerr_down), color = 'b', label = 'Gonzalez et al. 2011, z=4, slope = ' + str("%.2f" % m), linestyle='None')
	ax.plot(l_fit[:,0], l_fit[:,1], 'b', linestyle='dotted')
	#Labels
	ax.set_xlabel('$M_{' + str(real_w) + '}$', size = 24)
	ax.set_ylabel('Galaxy stellar mass $M/M_\odot$', size = 24)
	#Legend
	ax.legend(loc=3)
	#Grid
	ax.grid(True, alpha = 0.2)


	fig.savefig(out)







if __name__=='__main__':
	main()
