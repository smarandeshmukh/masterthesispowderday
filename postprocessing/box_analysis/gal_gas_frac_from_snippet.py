import numpy as np
import os
import sys
import h5py


def calc_gas_frac(f):
	
	gas = f['PartType0']
	stars = f['PartType4']
	h = f['Header'].attrs['HubbleParam']

	gas_mass = gas['Masses'][:] * 1.0e10 / h
	star_mass = stars['Masses'][:] * 1.0e10 / h

	gas_mass_tot = np.sum(gas_mass)
	star_mass_tot = np.sum(star_mass)
	return gas_mass_tot / (gas_mass_tot + star_mass_tot)

def get_haloid(input_file):

	hdf5file = os.path.basename(input_file)
	try:
		ind = hdf5file.index('gal')
		return hdf5file[ind + 3: ind + 3 + 4]
	except:
		return 'galaxy'

if __name__=='__main__':
	
	#User input
	try:
		hdf5file = sys.argv[1]
		out = sys.argv[2]
	except IndexError:
		print 'This calculates the gas fraction of a snippet galaxy. frac = gas / (stars + gas)'
		print 'Syntax: [script name] [hdf5 file] [output]'
		raise IndexError('Check the syntax')

	assert os.path.exists(hdf5file), 'HDF5 file does not exist: %s' % hdf5file
	
	try:
		f = h5py.File(hdf5file)
	except IOError:
		raise IOError('Could not open hdf5 file: %s' % hdf5file)

	gas_frac = calc_gas_frac(f)

	#Try and get haloid from input file name
	haloid = get_haloid(hdf5file)


	#Write to file
	if not os.path.exists(out):
		o = open(out, 'w')
		o.write('#Galaxy gas fractions in frac = gas / (stars + gas) \n#Haloid(0) frac(1)\n')
		o.close()
	o = open(out, 'a')
	o.write(str(haloid) + ' ' + str(gas_frac) + '\n')
