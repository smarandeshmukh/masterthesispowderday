#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/box_analysis/sfr_hist_calc.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/04072016/snippets'
output_file='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/04072016/sfr_hist_100Myr_100Myr.txt'

bins=$[1]
tstart=$[0]
tend=$[100]

start=$[0]
target=$[162]

full_list=($inp_root/*.hdf5)
sub_list=${full_list[@]:$start:$target}

rm -f $output_file

for i in $sub_list
do 
	echo $i
	python $exe $i $output_file $tstart $tend $bins
done
