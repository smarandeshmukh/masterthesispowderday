#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/box_analysis/gal_gas_frac_to_mass_from_snippet.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/04072016/snippets'
output_file='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/04072016/sim_gas_frac_to_star_mass.txt'

start=$[0]
target=$[162]

full_list=($inp_root/*.hdf5)
sub_list=${full_list[@]:$start:$target}

rm -f $output_file

for i in $sub_list
do 
	#echo $i
	python $exe $i $output_file
done
