import numpy as np
import os
import sys
import h5py
from astropy.cosmology import LambdaCDM
from astropy import cosmology
import astropy.units as u

quiet = True

def sfr(f, t1, t2):

	#Extract cosmology
	h = f['Header'].attrs['HubbleParam']
	Om0 = f['Header'].attrs['Omega0']
	Ode0 = f['Header'].attrs['OmegaLambda']
	z = f['Header'].attrs['Redshift']
	a0 = f['Header'].attrs['Time']
	cosmo = LambdaCDM(H0=100.0*h, Om0=Om0, Ode0=Ode0)

	#Lookback time at sim snapshot
	time0 = cosmo.lookback_time(z).value

	#Get the look back times at t1 and t2. (In Gyr)
	time1 = (time0 + t1/1000) * u.Gyr
	time2 = (time0 + t2/1000) * u.Gyr
	
	#Get the scale factors at t1 and t2
	z1 = cosmology.z_at_value( cosmo.lookback_time, time1)
	z2 = cosmology.z_at_value( cosmo.lookback_time, time2)
	a1 = 1 / ( 1 + z1 )
	a2 = 1 / ( 1 + z2 )

	#Now access the hdf5 file
	stars = f['PartType4']
	ages = stars['StellarFormationTime'][:]

	#Get indices of all particles for which a2 < a < a1
	good = np.where( np.logical_and( a2 < ages, ages < a1) )[0]
	
	#Access the masses of those particles 
	mass = stars['Masses'][:]
	good_mass = mass[ good ]

	#Sum and convert 1e10*Msun/h to Msun
	mass_tot = np.sum(good_mass)
	mass_tot = mass_tot * 1.0e10 / h

	#Get delta t and convert from Myr -> yr
	del_t = t2 - t1
	del_t = del_t * 1.0e6

	sfr = mass_tot / del_t

	if not quiet:
		print 'Time slot:', t1, t2
		print 'Number of particles in that slot:', len(good)
		print 'Mass of those particles:', mass_tot
		print 'SFR =', sfr, 'Msun/yr'
	
	return sfr


if __name__=='__main__':

	#Check user input
	try:
		hdf5file = sys.argv[1]
		out = sys.argv[2]
		t1 = float(sys.argv[3])
		t2 = float(sys.argv[4])
		bins = int(sys.argv[5])
	except IndexError:
		print "This script will calculate the sfr of a galaxy using a charactersitic age of t1 < age < t2 (normalized with no of bins)"
		print "t1 and t2 have to be given in Myr."
		print 'Syntax: [script name] [hdf5 file path] [output file] [t1] t2]'
		raise IndexError('Check your input arguments')
	except ValueError:
		raise ValueError('Are your ages floats? Is your bin number an integer?')
	assert t1 < t2, 't1 has to be less than t2'
	assert os.path.exists(hdf5file), 'hdf5 file does not exist'
	try:
		f = h5py.File(hdf5file, 'r')
	except IOError:
		raise IOError('Could not open hdf5 file')
	
	a2_array = np.zeros( bins )
	sfr_array = np.zeros( bins )

	for b in range(bins):
		a1 =  t1 + b * (t2 - t1) / bins
		a2 =  t1 + (b + 1) * (t2 - t1) / bins
		#Now do the work
		s = sfr(f, a1, a2)
		a2_array[b] = a2
		sfr_array[b] = s
		if not quiet:
			print a1, '-', a2, s

	#Try and parse haloid from hdf5 file
	try:
		basename = os.path.basename(hdf5file)
		ind = basename.index('gal')
		haloid = basename[ind + 3: ind + 3 +4]
	except:
		haloid = 'galaxy'

	#Print to file
	if not os.path.exists(out):
		o = open(out, 'w')
		#Write header
		header = "#Sfr rates in Msun/yr in the following bins: (Not cumulative)\n#"
		o.write(header)
		#Write right edge of all bins
		for x in a2_array:
			o.write(str(x) + ' ')
		o.write('\n')
		o.close()

	o = open(out, 'a')
	#Haloid
	o.write(haloid + ' ')
	#SFR
	for y in sfr_array:
		o.write(str(y) + ' ')
	o.write('\n')
