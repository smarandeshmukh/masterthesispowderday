import numpy as np
import os
import sys
import h5py
import pygadgetreader

quiet = False

M_sun_in_g = 1.989e33
kpc_in_cm = 3.086e21
km_s_in_cm_s = 1e5
gamma = 5.0/3.0
k_b_cgs = 1.381e-16
mu = 1.0
proton_mass = 1.673e-24


if __name__ == '__main__':

	#User input
	try:
		hdf5name = sys.argv[1]
		output = sys.argv[2]
	except IndexError:
		print 'This calculates the cold gas fraction to an output file.'
		print 'Syntax: [script name] [hdf5 file name] [output] (optional ->) [gas temp in K]'
		raise IndexError('Check Syntax')

	assert os.path.exists(hdf5name), 'Could not access hdf5 file: %s' % hdf5name

	#Check if user has set T
	try:
		T_cold = float(sys.argv[3])
		if not quiet:
			print 'Gas temperature T_cold set to: %d' % T_cold
	except:
		T_cold = 100
		if not quiet:
			print 'Using default value of T_cold: %d' % T_cold

	assert T_cold > 0, 'T_cold has to be a positive number'

	#Access the hdf5 file
	f = h5py.File(hdf5name, 'r')
	gas = f['PartType0']
	sp_e = gas['InternalEnergy'][:]
	rho = gas['Density'][:]
	m = gas['Masses'][:]
	hsml = gas['SmoothingLength'][:]

	#Get the h value
	h = f['Header'].attrs['HubbleParam']

	
	test = [100, 10000, 103345, 37803, 74305]

	d = rho[:] * (1e10 * 2e33 / h) / (3.086e21/h)**3

	n = d / (1.67e-24)

	p_Pa = sp_e[:] * ( ( 1e5 )**2 ) * d * (gamma - 1) / 10

	Tp = (p_Pa / (n * k_b_cgs) )

	#The correction:
	gammap = 2.0
	T2star = 2500 * (hsml / 180) ** (2./3.)
	n2star = 3.2 * (hsml / 180) ** (-4./3.)

	T_corr = T2star * (n / n2star) ** (gammap - 1)

	T_final = Tp - T_corr

	print 'Cold T definition:', T_cold

	cold_ids = np.where( T_final < T_cold )[0]
	
	cold_gas_mass = m[cold_ids]

	cold_gas_mass_tot = np.sum(cold_gas_mass)
	gas_mass_tot = np.sum(m)

	print 'Total mass:', gas_mass_tot
	print 'Cold gas mass:', cold_gas_mass_tot

	#Access the stars
	stars = f['PartType4']
	m_stars = stars['Masses'][:]
	m_star_tot = np.sum(m_stars)

	print 'Stellar Mass:', m_star_tot

	fraction = cold_gas_mass_tot/ (cold_gas_mass_tot + m_star_tot)
	print 'Cold Fraction to Baryon fraction:', fraction

	#Access the galaxy id
	hdf5file = os.path.basename(hdf5name)
	try:
		ind = hdf5file.index('gal')
		haloid =  hdf5file[ind + 3: ind + 3 + 4]
	except:
		haloid =  'galaxy'

	#Write to file:
	if not os.path.exists(output):
		o = open(output, 'w')
		o.write('#Galaxy cold gas fraction, T < %d K, f = cold gas / (cold gas + star) \n#Haloid(0) frac(1)\n' % (T_cold) )
		o.close()
	o = open(output, 'a')
	o.write(str(haloid) + ' ' + str(fraction) + '\n')
