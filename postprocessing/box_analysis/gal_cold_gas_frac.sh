#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/box_analysis/get_cold_gas_frac_from_snippet.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/25072016/snippets'
output_file='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/25072016/sim_cold_gas_frac_1e4.txt'

start=$[0]
target=$[162]

full_list=($inp_root/*.hdf5)
sub_list=${full_list[@]:$start:$target}

rm -f $output_file

for i in $sub_list
do 
	#echo $i
	python $exe $i $output_file 1e4
done
