import numpy as np
import os
import sys
import h5py


def calc_met(f):
	
	gas = f['PartType0']
	metal_frac = gas['Metallicity'][:]
	h = f['Header'].attrs['HubbleParam']

	gas_mass = gas['Masses'][:] * 1.0e10 / h
	metal_mass = metal_frac * gas_mass

	metal_mass_tot = np.sum(metal_mass)
	gas_mass_tot = np.sum(gas_mass)
	return metal_mass_tot/gas_mass_tot / 0.02

def get_haloid(input_file):

	hdf5file = os.path.basename(input_file)
	try:
		ind = hdf5file.index('gal')
		return hdf5file[ind + 3: ind + 3 + 4]
	except:
		return 'galaxy'

if __name__=='__main__':
	
	#User input
	try:
		hdf5file = sys.argv[1]
		out = sys.argv[2]
	except IndexError:
		print 'This calculates the metallicity of a snippet galaxy from the gas only.'
		print 'Syntax: [script name] [hdf5 file] [output]'
		raise IndexError('Check the syntax')

	assert os.path.exists(hdf5file), 'HDF5 file does not exist: %s' % hdf5file
	
	try:
		f = h5py.File(hdf5file)
	except IOError:
		raise IOError('Could not open hdf5 file: %s' % hdf5file)

	met = calc_met(f)

	#Try and get haloid from input file name
	haloid = get_haloid(hdf5file)


	#Write to file
	if not os.path.exists(out):
		o = open(out, 'w')
		o.write('#Galaxy metallicities in Z/Z0 \n#Haloid(0) Z(1)\n')
		o.close()
	o = open(out, 'a')
	o.write(str(haloid) + ' ' + str(met) + '\n')
