import h5py
import numpy as np
import sys
import os


def m_calc(f):
	
	stars = f['PartType4']
	starmass = stars['Masses'][:]
	m = np.sum(starmass) * 1.0e10
	
	return m

def get_haloid(input_file):

	hdf5file = os.path.basename(input_file)
	try:
		ind = hdf5file.index('gal')
		return hdf5file[ind + 3: ind + 3 + 4]
	except:
		return 'galaxy'


if __name__ == '__main__':

	#Get user input
	try:
		inp = sys.argv[1]
		output = sys.argv[2]
	except IndexError:
		print "This script calculates the galaxy stellar masses from hdf5 snippets containing ONLY the galaxy. The mass is given in units of Msun/h, or the default units of the gadget hdf5 file."
		print "Syntax: [script name] [hdf5 snippet] [output file]"
		raise IndexError("You need to input a valid hdf5 file and output")
	
	#Check existence of hdf5
	assert os.path.exists(inp), "Hdf5 file does not exist: %s" % inp

	#Check if input is an hdf5 file
	try:
		f = h5py.File(inp)
	except IOError:
		raise IOError( "Could not detect hdf5 file." )

	#Get stellar mass
	mass = m_calc(f)

	#Try and get haloid from input file name
	haloid = get_haloid(inp)

	#Write to file
	if not os.path.exists(output):
		o = open(output, 'w')
		o.write('#Galaxy stellar masses in Msun/h \n#Haloid(0) Mass(1)\n')
		o.close()
	o = open(output, 'a')
	o.write(str(haloid) + ' ' + str(mass) + '\n')
