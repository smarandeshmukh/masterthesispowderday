import numpy as np
import sys
import os



if __name__=='__main__':
	
	#User input
	try:
		cat = sys.argv[1]
	except IndexError:
		print 'Calculates the distance between the galaxies.'
		print 'Syntax: [script name] [catalog]'
		raise IndexError('Check Syntax')

	#Load catalog
	d = np.loadtxt(cat)[:]

	cutoff = 1e6
	
	#Select parent halos only
	host_halo = d[:,1]
	m = d[:,3]
	mstar = d[:,64]


	print 'Number of halos:', host_halo.shape
	parents = np.where( host_halo == -1)[0]
	massive = np.where( mstar > cutoff)[0]
	print 'Number of host halos:', parents.shape
	print 'Number of massive galaxies (M> %d)' % cutoff, massive.shape
	massive_hosts =  np.intersect1d( massive, parents )
	print 'Number of massive host halos:', massive_hosts.shape

	d = d[massive_hosts]
	
	#Load coordinates
	x = d[:,5]
	y = d[:,6]
	z = d[:,7]

	m = d[:,3]
	h = d[:,0]

	sep_arr = np.array([0,0,0,0,0,0,0])

	print x[1]

	for i in range(len(d)):
		if i % 10 == 0:
			print i
		for j in range(i+1, len(d)):
			sep = (x[i] - x[j])**2 + (y[i] - y[j])**2 + (z[i] - z[j])**2
			sep = np.sqrt(sep)
			line = np.array([sep, i, j, mstar[i], mstar[j], h[i], h[j] ])
			sep_arr = np.vstack( (sep_arr, line) )
	
	print sep_arr.shape
	sep_arr = np.delete(sep_arr, 0, axis = 0)

	sorted_sep =  sep_arr[ np.argsort(sep_arr[:,0]) ]
	print sorted_sep[:10]
	
	np.savetxt('separations.dat', sorted_sep)
