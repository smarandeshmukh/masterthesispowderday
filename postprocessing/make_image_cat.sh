#!/bin/bash

echo 'Hello'

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/make_image3.py'
root_fol='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/04072016'
out_name='/images_z0'


a=($root_fol/*/*rtout.image*)

echo 'Executable path:' $exe
echo 'Root folder of all image folders' $root_fol

echo 'Number of images to make:' ${#a[@]}

current=$[1]

for image in ${a[@]}
do
	output_fol=${image%/*}$out_name
	echo 'Processing file no. ' $current ' of ' ${#a[@]}
	python $exe $image $output_fol quiet
	current=$[$current + 1]
done
