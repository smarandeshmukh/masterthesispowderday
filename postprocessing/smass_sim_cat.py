import numpy as np
import h5py
import os
import sys

def main():
	
	def m_in_gal_sphere(position, radius):
		#Get particle indices within a sphere of r_gal
		coords = f['PartType4']['Coordinates'][:]
		pids = np.where( np.sum((coords - position)**2, 1) < radius**2 )[0]
		#Return the masses of these particles
		h = f['Header'].attrs['HubbleParam']
		mass = f['PartType4']['Masses'][:]
		return np.sum(mass[pids]) * 10**10 / h
	
	def m_in_gal_cube(position, radius):
		#Get particle indices within a cube of edge l=2*radius
		coords = f['PartType4']['Coordinates'][:]
		xids = np.where( np.abs(coords[:,0] - position[0]) < radius)[0]
		yids = np.where( np.abs(coords[:,1] - position[1]) < radius)[0]
		zids = np.where( np.abs(coords[:,2] - position[2]) < radius)[0]
		from functools import reduce
		pids = reduce( np.intersect1d, (xids,yids,zids) )
#		print len(pids)
		#Return the masses of these particles
		h = f['Header'].attrs['HubbleParam']
		mass = f['PartType4']['Masses'][:]
		return np.sum(mass[pids]) * 10**10 / h

	#Get user input
	try:
		hdf5name = sys.argv[1]
		cat = sys.argv[2]
		out = sys.argv[3]
	except:
		print 'Wrong input!'
		print 'Syntax: [script.py] [hdf5 file] [catalogue file] [output file]'
		raise Exception('Input Error')
	
	#Check existence of crucial files
	if not os.path.exists(hdf5name):
		raise Exception('hdf5 file does not exist')
	if not os.path.exists(cat):
		raise Exception('catalogue file does not exist')
	
	#Open hdf5 file
	f = h5py.File(hdf5name)
	
	#Access catalogue and define col names
	c = np.loadtxt(cat)
	haloid_col = 0
	pos_col = np.array([2,3,4])
	gal_r_col = 8

	#Initialize final array
	answer = np.array([])

	for row in range(len(c))[:]:
		#print c[row, haloid_col], c[row, pos_col[0]], c[row, pos_col[1]], c[row, pos_col[2]], c[row, gal_r_col]
		haloid = int(c[row, haloid_col])
		center = np.array([c[row, pos_col[0]], c[row, pos_col[1]], c[row, pos_col[2]]])
		rad = c[row, gal_r_col]
#		print haloid, center, rad
#		print haloid, rad, m_in_gal_sphere(center, rad), m_in_gal_cube(center,rad)
		line = np.array([haloid,m_in_gal_sphere(center, rad), m_in_gal_cube(center,rad)])
		if row == 0:
			answer = line[None,:]
		else:
#			print answer.shape
			answer = np.append(answer, line[None,:], axis = 0 )

	print answer
	header = 'haloid(0) m_sphere(1) m_cube(2) in m_star'
	np.savetxt(out, answer, header=header)

if __name__=='__main__':
	main()
