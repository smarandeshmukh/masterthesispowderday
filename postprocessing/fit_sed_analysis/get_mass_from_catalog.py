import numpy as np
import os
import sys


if __name__ == '__main__':

	#User input
	try:
		cat_file = sys.argv[1]
		output = sys.argv[2]
	except IndexError:
		print 'This pulls the stellar masses from a catalog file and writes to a new file.'
		print 'Syntax [script name] [sed fit catalog] [output]'
		raise IndexError('Check Syntax')

	#Check that the file exists
	assert os.path.exists(cat_file), 'Could not access the catalog file %s' % cat_file

	#Access the data
	d = np.loadtxt(cat_file)
	print d.shape
