import numpy as np
import os
import sys





if __name__=='__main__':

	#User input
	try:
		inp_root_folder = sys.argv[1]
		basename = sys.argv[2]
		x_filters = sys.argv[3:5]
		y_filters = sys.argv[5:7]
		inclination = int(sys.argv[7])
		output = sys.argv[8]
	except IndexError:
		print 'This script assemble the colors from a magnitude file.'
		print 'Syntax [inp root folder] [mag file basename] [x_filter 1] [x_filter 2] [y_filter 1] [y_filter 2] [inclination] [output]'
		print 'Input root folder contains all the galaxy subfolders'
		print 'mag file basename is the name of the mag file.'
		print 'x_axis = x_filter 1 - x_filter 2.'
		print 'y_axis = y_filter 1 - y_filter 2.'
		print 'inclination = inclnation id'
		raise IndexError('Check Syntax')

	#Get the filter ids
	x_filter_1 = int( x_filters[0] )
	x_filter_2 = int( x_filters[1] )
	y_filter_1 = int( y_filters[0] )
	y_filter_2 = int( y_filters[1] )

	#Print user inputs
	print 'Inp root folder:', inp_root_folder
	print 'Basename:', basename
	print 'x_filters:', x_filter_1, x_filter_2
	print 'y_filters:', y_filter_1, y_filter_2
	print 'inclination id:', inclination
	print 'output:', output

	#Assert that stuff exists
	assert os.path.exists(inp_root_folder), 'Could not find input root folder'

	#Traverse through root directory to find mag files
	mag_file_locations = []
	for root, dirs, files in os.walk(inp_root_folder):
		for file_names in files:
			if file_names == basename:
				mag_file_locations.append( os.path.join(root, file_names) )


	print 'Number of mag files:', len(mag_file_locations)

	#Convert the inclination id to the correct col number
	inc_col = inclination*2 + 1

	#Open the final output file
	o = open(output, 'w')

	mag_file_locations = np.sort( mag_file_locations )

	#Now start accessing the files
	for i in range( len(mag_file_locations[:]) ):
		filename = mag_file_locations[i]
		haloid = filename.split('/')[-2]
		d = np.loadtxt(filename, usecols=(inc_col, inc_col+1) )
		if i == 0:
			d_filter_names = np.loadtxt(filename, usecols=(0,), dtype=str )
			print 'x filter names:', d_filter_names[x_filter_1], '-', d_filter_names[x_filter_2]
			print 'y filter names:', d_filter_names[y_filter_1], '-', d_filter_names[y_filter_2]
			o.write('#Colors for the galaxies for inclination %d \n' % inclination)
			o.write('#Using filter files\n')
			o.write('#X-axis = %s - %s \n' % (d_filter_names[x_filter_1], d_filter_names[x_filter_2]) )
			o.write('#Y-axis = %s - %s \n' % (d_filter_names[y_filter_1], d_filter_names[y_filter_2]) )
			o.write('#haloid(0) x_color(1) x_color_err(2) y_color(3) y_color_err(4) \n')

		#Calculate the errors
		x_color = d[x_filter_1, 0] - d[x_filter_2, 0]
		x_color_err = d[x_filter_1, 1] + d[x_filter_2, 1]
		y_color = d[y_filter_1, 0] - d[y_filter_2, 0]
		y_color_err = d[y_filter_1, 1] + d[y_filter_2, 1]

		o.write('%s %f %f %f %f \n' %(haloid, x_color, x_color_err, y_color, y_color_err) )
		

