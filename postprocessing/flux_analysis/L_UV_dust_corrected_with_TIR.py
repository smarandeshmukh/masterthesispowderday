import numpy as np
import os
import sys

if __name__=='__main__':

	#User Input
	try:
		UV_filename = sys.argv[1]
		TIR_filename = sys.argv[2]
		output = sys.argv[3]
	except IndexError:
		print 'This applies the dust correction to the L_UV: L_UV_corr = L_UV + 0.46*L_TIR'
		print 'Syntax: [UV lums] [TIR lums] [output]'
		print 'flux files are expected to have 3 cols: (1) gal id (2) flux(erg/s) (3) uncertainty (erg/s)'
		raise IndexError('Check Syntax')

	#Check that files exist
	assert os.path.exists(UV_filename), 'Could not find UV file.'
	assert os.path.exists(TIR_filename), 'Could not find TIR file.'

	#Read the data
	uv = np.loadtxt(UV_filename)
	tir = np.loadtxt(TIR_filename)

	uv_flux = uv[:,1]
	uv_unc = uv[:,2]
	tir_flux = tir[:,1]
	tir_unc = tir[:,2]

	assert np.array_equal(uv[:,0], tir[:,0])

	#Write to file
	header1 = '#The corrected the UV luminosities using the TIR: L_UV: L_UV_corr = L_UV + 0.46*L_TIR\n'
	header2 = '#Halo id(0) Flux [erg/s] (1) sig_f [erg/s] (2)\n'

	o = open(output, 'w')
	o.write(header1)
	o.write(header2)
	for i in range(len(uv[:,0])):
		o.write(str(int(uv[i,0])) + ' ' + str(uv_flux[i] + 0.46*tir_flux[i]) + ' ' )
		o.write(str(uv_unc[i] + 0.46*tir_unc[i]) + '\n')
