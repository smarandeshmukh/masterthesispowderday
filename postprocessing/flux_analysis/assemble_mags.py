import numpy as np
import os
import sys

quiet = False

def get_files(root, name):

	file_list = []

	for root, dirs, files in os.walk(root):
		for file in files:
			if file == name:
				file_list.append( os.path.join(root, file) )

	return file_list

if __name__=='__main__':

	#User input
	try:
		root_fol = sys.argv[1]
		base_name = sys.argv[2]
		out = sys.argv[3]
	except IndexError:
		print 'This script assembles all the magnitudes into 1 file.'
		print 'Syntax [script name] [root folder] [base mag bame] [output] (optional -> ) [filter id] [inclination id]'
		raise IndexError('Check Syntax')

	assert os.path.exists(root_fol) == True, 'could no find root folder: %s' % root_fol

	#Check if user want to use particular filer/inclination
	try:
		filt_id = int(sys.argv[4])
		#incl_id = int(sys.argv[4])
	except (IndexError, ValueError):
		if not quiet:
			print 'No filter ids detected. Using defaults.'
		filt_id = 0

	try:
		incl_id = int(sys.argv[5])
	except (IndexError, ValueError):
		if not quiet:
			print 'No inclinations detected. Using defaults.'
		incl_id = 0

	if not quiet:
		print 'Root folder:', root_fol
		print 'Magnitude file name:', base_name
		print 'Filter and inclination id:', filt_id, incl_id

	#Get the file list of mag files
	magfiles = get_files(root_fol, base_name)

	print magfiles[:2], magfiles[0].split('/')[-2]

	#Get the filter name
	filter_name = np.loadtxt(magfiles[0], dtype=str)[filt_id, 0]

	#Initialise file
	o = open(out, 'w')
	o.write('#Magnitudes plotted using the following filter: %s \n' % filter_name )
	o.write('#Magnitudes plotted for inclination: ' + str(incl_id) + '\n')

	#Now extract the magnitudes
	for i in magfiles[:]:
		d_string = np.loadtxt(i, dtype=str)
		
		#Try and get the haloid
		try:
			haloid = i.split('/')[-2]
		except:
			haloid = 'galaxy'

		#Access the magnitudes
		d = d_string[:, 1:].astype(float)
		
		#Get the mag at filter_id and inclination
		mag_final = d[filt_id, incl_id]

		#Write to file
		o.write(haloid + ' ' + str(mag_final) + '\n')
