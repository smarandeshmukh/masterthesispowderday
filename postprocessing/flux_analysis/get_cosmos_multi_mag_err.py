import numpy as np
import os
import sys

start_id = 12
end_id = 44

bin_left = 11.0
bin_right = 35.0

if __name__=='__main__':

	#User input
	try:
		catalog_file = sys.argv[1]
		output = sys.argv[2]
		ir_catalog_file = sys.argv[3]
	except IndexError:
		print 'This scripts calculates the photometric errors in the different bands.'
		print 'Syntax: [script] [catalog file] [output] [IR_scosmos_file]'
		raise IndexError('Check Syntax')

	assert os.path.exists(catalog_file)

	#Load the data. First get the relevant cols to load. Only mag columns here
	col_ids = np.arange(start_id, end_id)
	d = np.loadtxt(catalog_file, comments='|', usecols = col_ids)

	#Get the column for the star bool
	star_bool = np.loadtxt(catalog_file, comments='|', usecols = (57,), dtype = int )

	#Load the first line to get the band names
	line = open(catalog_file, 'r').readline()
	line = np.asarray( [x.strip() for x in line.split('|')][1:], dtype = str )
	filter_ids = np.arange(start_id, end_id, 2)
	filter_names = line[filter_ids]

	binedges = np.arange(bin_left, bin_right, 0.5)
	print binedges

	
	#Open the output file and write header
	o = open(output, 'w')
	o.write('#This file contains the mag errors pulled from the Cosmos survey\n')
	o.write('#Subaru=(u,B,V,g,r,i,z); CTIO/KPNO=(K_mag); CFHT=(i_cfht_mag); SDSS=(ugriz); HST=F814w; Subaru=(NB816) \n')
	o.write('#Mag_bin(0) Mag_err_in_filter(1....)\n#Mag bin(0) ')
	for i in range(len(filter_names) ):
		o.write( '%10s'  %(filter_names[i]) + ' ')
	o.write('\n')

	#Iterate through the different mag bins
	for j in range( len(binedges) - 1)[:]:
		bin_bright = binedges[j]
		bin_faint = binedges[j+1]

		#print 'Processing bin:', bin_bright, '-', bin_faint

		#Write the bincenter
		o.write( '%12s' %(str( (bin_bright+bin_faint)/2 ) + ' ') )

		#print bin_bright,

		#Iterate throught the different filters
		for i in range( len(filter_names) )[:]:
			#Access the mag col and err col
			mag_col_index = 2*i
			mag_col = d[:, mag_col_index]
			mag_err_col = d[:, mag_col_index + 1]
			
			#See what objects fall inside this bin
			ids_in_bin = np.where( np.logical_and( mag_col >= bin_bright, mag_col < bin_faint) )[0]

			#Check if bin is not empty
			if len(ids_in_bin) > 0:
				
				#Get the flag is object is star or not
				is_star = star_bool[ids_in_bin]
				is_gal_ids = np.where( is_star != 1 )[0]
				
				#Get the errors for all the galaxies in this bin
				gal_mag_err = mag_err_col[ids_in_bin][is_gal_ids]
				
				#Calculate the mean error
				err_in_bin = np.mean( gal_mag_err )
				
				#Write to file if there are galaxies
				if len(is_gal_ids) > 0:
					o.write( '%10s' %(str( '%.4f' % err_in_bin)) + ' ' )
				else:
					o.write( '%10s'  %(str(-1)) + ' ')
				
				"""
				print filter_names[i], bin_bright
				print 'Number of objects in bin:', len(ids_in_bin)
				print 'Number of galaxies:', len(is_gal_ids)
				print 'Errors:', gal_mag_err
				print 'Mean error in bin:', err_in_bin
				print 
				"""
				
			else:
				o.write( '%10s'  %(str(-1)) + ' ')
		o.write('\n')
	o.close()

	#Load IR data
	d2 = np.loadtxt(ir_catalog_file, usecols = (15,16,24,25,33,34,42,43), skiprows=267 )
	
	#IR filter names
	ir_filter_names = ['3.6um', '4.5um', '5.8um', '8.0um']

	#initiliaze final array
	ir_final = np.zeros( (len(binedges) - 1, len(ir_filter_names) ) )

	#Start iteration
	for j in range( len(binedges) - 1)[:]:
		
		bin_bright = binedges[j]
		bin_faint = binedges[j+1]
		
		#Iterate through filters
		for i in range( len(ir_filter_names) )[:]:
			ir_mag_col_id = 2*i
			ir_mag_err_col_id = ir_mag_col_id + 1

			ir_flux = d2[:,ir_mag_col_id]
			ir_flux_err = d2[:, ir_mag_err_col_id]

			#Get the non zero fluxes first
			flux_non0_ids = np.where( ir_flux > 0 )[0]
			flux_err_non_ids = np.where( ir_flux_err > 0 )[0]
			non0_ids = np.intersect1d( flux_non0_ids, flux_err_non_ids )

			#greater_than_half_ids = np.where( ir_flux_err/ir_flux < 3)[0]
			#non0_ids = np.intersect1d( non0_ids, greater_than_half_ids)

			ir_mag = -2.5*np.log10( ir_flux[non0_ids] * 1e-6 / 3631 )
			#ir_mag_err = np.abs(-2.5 / np.log(10) * ir_flux_err[non0_ids] / ir_flux[non0_ids])
			ir_mag_err = np.abs( -2.5 * np.log10( (ir_flux[non0_ids] + ir_flux_err[non0_ids]) / ir_flux[non0_ids] ) )

			bool1 = np.logical_and( ir_mag >= bin_bright, ir_mag < bin_faint)

			ids_in_bin = np.where( bool1 )[0]

			if len(ids_in_bin) > 0:

				gal_mag_err = ir_mag_err[ids_in_bin]
				err_in_bin = np.mean( gal_mag_err )

				print bin_bright, err_in_bin
				#print ir_flux[ids_in_bin]
				#print ir_flux_err[ids_in_bin]
			else:
				err_in_bin = -1
			ir_final[j][i] = err_in_bin
			
	e1 = np.loadtxt(output)
	print e1.shape, ir_final.shape
	e2 = np.concatenate( (e1, ir_final), axis = 1)

	o = open(output, 'w')
	o.write('#This file contains the mag errors pulled from the Cosmos survey\n')
	o.write('#Subaru=(u,B,V,g,r,i,z); CTIO/KPNO=(K_mag); CFHT=(i_cfht_mag); SDSS=(ugriz); HST=F814w; Subaru=(NB816); IRAC IR filter=(3.6,4.5,5.8,8.0) \n')
	o.write('#Mag_bin(0) Mag_err_in_filter(1....)\n#Magbin(0) ')
	for i in range(len(filter_names) ):
		o.write( '%10s'  %(filter_names[i]) + ' ')
	for i in range(len(ir_filter_names)):
		o.write( '%10s' %(ir_filter_names[i]) + ' ')
	o.write('\n')
	
	print e2.shape


	for row in range( len(e2) ):
		for col in range( len(e2[0]) ):
			val = e2[row][col]
			if val.is_integer():
				o.write( '%10s ' %(str(val)) )
			else:
				val_str = str( '%4f' % val )
				o.write( '%10s' % val_str)
		o.write('\n')
