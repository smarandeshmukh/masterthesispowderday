import numpy as np
import os
import sys
from astropy.cosmology import LambdaCDM

Om0 = 0.308
Ode0 = 0.692
H0 = 67.8

#Array telling us the order the filter names are written wrt to the cosmos_err_file
cosmos_filter_order = np.array( [1,2,9,10,11,12,13,14,16,17,18,19] )

#Array describing the filter ids for FAST
FAST_filter_order = np.array( [78,79,73,74,75,76,77,16,18,19,20,21] )

if __name__=='__main__':

	#User input
	try:
		mag_err_file = sys.argv[1]
		inp_root_fol = sys.argv[2]
		mag_basename = sys.argv[3]
		output = sys.argv[4]
		z = float(sys.argv[5])
		inclination = int( sys.argv[6] )
	except IndexError:
		print 'This compiles all the magnitudes and the correspondin errors into 1 file.'
		print 'Syntax: [script name] [mag_err_file] [inp_root_fol] [mag_basename] [output] [redshift] [inclination]'
		raise IndexError('Check Syntax')

	#Check that stuff exists
	assert os.path.exists(mag_err_file), 'Mag_err file does not exist'
	assert os.path.exists(inp_root_fol), 'inp_root_fol does not exist'
	assert len(cosmos_filter_order) == len(FAST_filter_order), 'Check the lengths of "cosmos_filter_order" and "FAST_filter_order". They dont seem to be the same'

	#Initialize location array
	mag_file_locations = []

	#Walk through all the folders
	for root, directory, filename in os.walk(inp_root_fol):
		for f in filename:
			if f == mag_basename:
				mag_file_locations.append( os.path.join(root, f) )

	#Sort the locations
	mag_file_locations = np.sort( mag_file_locations )

	#Get the distance modulus from the cosmology in parsecs
	cosmo = LambdaCDM(H0 = H0, Om0 = Om0, Ode0 = Ode0)
	distance = cosmo.luminosity_distance(z).value * 1e6
	modulus = 5 * np.log10( distance ) - 5

	#Get the right column ids for the inclinations
	i_col = 2*inclination
	i_err = i_col + 1

	#Load the cosmos error file and the available magnitude bins
	e = np.loadtxt(mag_err_file)
	print 'Cosmos error file shape:', e.shape
	cosmos_selected_filters = np.array( open( mag_err_file ).readlines()[3].split()[1:] )[cosmos_filter_order]
	mag_bincenters = e[:,0]

	#Keep track of then number of times the errors fall outside a bin
	bad_errors = 0
	
	#Now start to iterate throught the files
	for i in range( len(mag_file_locations) )[:]:
		
		file_name = mag_file_locations[i]

		#Parse the haloid
		try:
			haloid = int(  file_name.split('/')[-2] )
		except:
			haloid = i

		#Access the magnitudes
		d1 = np.loadtxt( file_name, dtype = str )
		d = d1[:, 1:].astype( float )

		mags = d[:, i_col]
		mag_err = d[:, i_err]

		#Compile the mag and erros into 1 flattenned array
		line_array = np.zeros( 2*len(mags) + 1 )
		for j in range( len(mags) ):
			#First compute the apparent magnitude
			app_mag = mags[j] + modulus
			#Convert to jansky
			flux_Jy = 3631 * 10**( -app_mag / 2.5 )
			line_array[j*2 + 1] = flux_Jy

			#For the errors find which error is greater: sim or obs. Pick the largest one
			sim_mag_err = mag_err[j]
			magbin_id = np.argmin( np.abs(app_mag - mag_bincenters) )
			#Sometimes that observational bin has no errors. Then pick the error from the bin closts to that
			non_negative_err_ids = np.where( e[:, cosmos_filter_order[j]] != -1 )[0]
			closest_id = non_negative_err_ids[ np.argmin( np.abs( magbin_id - non_negative_err_ids) ) ]
			if magbin_id != closest_id:
				bad_errors += 1
			print app_mag, mag_bincenters[magbin_id], sim_mag_err, e[closest_id][cosmos_filter_order[j]], magbin_id, closest_id
			obs_error = e[closest_id][cosmos_filter_order[j]]
			total_error = np.maximum( sim_mag_err, obs_error )
			del_flux = flux_Jy * total_error * np.log(10) / 2.5
			line_array[j*2 + 2] = del_flux
		line_array[0] = haloid
		
		#Save the filter names
		if  i == 0:
			filter_names = d1[:,0]
			final = line_array
		else:
			final = np.vstack( (final, line_array) )


	print 'Filters used from mag files:\n', filter_names
	print 'Filters used from the cosmos err file:\n', cosmos_selected_filters
	print 'Number of times errors did not fall into a mag bin:', bad_errors

	#Save the output file
	o = open(output, 'w')
	#Print header
	header_str = '# id '
	for i in range( len(cosmos_filter_order) ):
		header_str += 'F%d E%d ' %(FAST_filter_order[i], FAST_filter_order[i])
	header_str += 'z_spec'
	o.write(header_str)
	for row in range( len(final) ):
		for col in range( len(final[row]) ):
			if col == 0:
				o.write( '\n%5s ' %( str( int(final[row][col]) ) ) )
			else:
				o.write( str( final[row][col] ) + ' '  )
		o.write('%.3f' %(z) )
