import numpy as np
import os
import sys


def get_SFR(lum):
	
	#General equation: SFR = a * lum^b
	a = 5.58e-36
	b = 0.826
	SFR = 5.58e-36 * ( lum )**0.826
	return SFR

def get_unc(lum, sigma):
	
	#General equation sig_SFR = a*b*L^(b-1) * sig_l
	a = 5.58e-36
	b = 0.826
	sig_unc = a * b * ( lum**(b-1) ) * sigma
	return sig_unc

def printing(haloid, SFR, sig, output_file):
	
	f = open(output_file, 'w')
	f.write('#SFR rates calculated from 24mu emission in Msun/yr \n#Haloid(0) SFR(1) uncertainty(2)\n')
	for i in range( len(haloid) ):
		f.write( "%04d" % haloid[i] + ' ' + str(SFR[i]) + ' ' + str(sig[i]) + '\n' )


if __name__=='__main__':
	
	#Print help here
	try:
		flux_file_name = sys.argv[1]
		output_file = sys.argv[2]
	except:
		print "This program convert 24 micron emission to SFR rate based on the calibration reported in Murphy et. al, 2011"
		print "Syntax: [script name] [input flux file] [output file]"
		print 'Input file has to have >2 columns: (1) galaxy id and (2) flux in erg/s (3) uncertainty'
		raise Exception('Input files not detected')

	#Load data
	try:
		d = np.loadtxt(flux_file_name)
	except:
		raise Exception('Could not load input into numpy')
	assert d.shape[1] >= 2, 'Input data has n_cols != 2'

	#Calculate the SFR
	SFR_array = get_SFR(d[:,1])
	#Calculate uncertainty if there is third column
	SFR_unc_array = 0
	if d.shape[1] >= 3:
		SFR_unc_array = get_unc(d[:,1], d[:,2])
	#Print to file
	printing( d[:,0], SFR_array, SFR_unc_array, output_file )
