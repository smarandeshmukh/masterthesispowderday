import numpy as np
import sys
import os

def main():

	try:
		root_fol = sys.argv[1]
		mag_basename = sys.argv[2]
		filters_ids = sys.argv[3]
	except:
		raise Exception


	#Get mag files
	mag_f_list = []
	for root, sub, files in os.walk(root_fol):
		for item in files:
			if item == mag_basename:
				mag_f_list.append( os.path.join(root, item) )

	#Get filter ids
	f_ids = np.array( filters_ids.split(','), dtype=int)


	for f in mag_f_list[:3]:
		print f
		d = np.loadtxt(f, dtype=str)
		print d[f_ids, 0]




if __name__=='__main__':
	main()
