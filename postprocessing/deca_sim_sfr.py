import numpy as np
import h5py
import os
import sys
from astropy.cosmology import LambdaCDM, z_at_value
from astropy import units as u
from functools import reduce

def main(inp='', agestart=10, agefinish=20, center=None, R=None,quiet=True):

	#Print user inputs
	if not quiet:
		print 'Input file:', inp
		print 'Agestart (Myr)', agestart
		print 'Agefinish (Myr)', agefinish
		print 'Center', center
		print 'Cube edge length = 2 *', R


	#Verify hdf5 file existence
	if not os.path.exists(inp):
		print "Input file does not exist:", inp
		raise Exception("Input file does not exist.")

	#Open hdf5 file
	f = h5py.File(inp, 'r')

	#Access relevant fields from header
	h = f['Header']
	H0 = h.attrs['HubbleParam'] * 100
	Om0 = h.attrs['Omega0']
	Ode0 = h.attrs['OmegaLambda']
	z = h.attrs['Redshift']

	#Zoom into specified region
	coords = f['PartType4']['Coordinates'][:]
	coord_ids_x = np.where( np.abs(coords[:,0] - center[0]) <= R)[0]
	coord_ids_y = np.where( np.abs(coords[:,1] - center[1]) <= R)[0]
	coord_ids_z = np.where( np.abs(coords[:,2] - center[2]) <= R)[0]
	coord_ids = reduce(np.intersect1d, (coord_ids_x,coord_ids_y,coord_ids_z))
	if not quiet:
		print 'Number of stars in zoomed in region =',  len(coord_ids)
	ages = f['PartType4']['StellarFormationTime'][:]

	#Create Cosmology
	cosmo = LambdaCDM(H0=H0, Om0=Om0, Ode0=Ode0)

	#Calculate corresponding scale factor
	t0 = cosmo.lookback_time(z)
	if not quiet:
		print "Currently lookback time at redshift", z, "is", t0
	#Convert agelimit to scale factor
	t_target_s = (t0.value + agestart/1000) * u.Gyr
	t_target_f = (t0.value + agefinish/1000) * u.Gyr
	z_agelimit_s = z_at_value(cosmo.lookback_time, t_target_s)
	z_agelimit_f = z_at_value(cosmo.lookback_time, t_target_f)
	a_min = 1/(1+z_agelimit_f)
	a_max = 1/(1+z_agelimit_s)
	if not quiet:
		print "Agelimit detected."
		print 'Calculating scale factor for a lookback time of:', t_target_s, t_target_f
		print 'Redshift at agelimit', z_agelimit_s, z_agelimit_f
		print 'Scalefactor at agelimit = limiting scale factor:', a_max, a_min

	#Get indexes of all particles for which a_max > a > a_min
	ind_ages_min = np.where(ages >= a_min)[0]
	ind_ages_max = np.where(ages >= a_max)[0]
	ind_ages_min_max = np.setdiff1d(ind_ages_min, ind_ages_max)
	#Combine these indices with the ones inide zoom in region
	if center != None:
		ind = reduce( np.intersect1d, (ind_ages_min_max, coord_ids) )
	else:
		ind = ind_ages_min_max
	if not quiet:
		print 'Number of star particles in input file', len(ages)
		print 'Number of star particles in agelimited list and "zoom" limited =', len(ind)

	#Get total mass of selected particles
	mass = f['PartType4']['Masses'][:]
	mass_sel = mass[ind]
	mass_tot = np.sum(mass_sel) * 1e10 / (H0/100) * u.Msun
	if not quiet:
		print 'Total stellar mass formed within agelimits:', mass_tot

	#Get delta_t = t_target - t0
	del_t = t_target_f - t_target_s
	sfr = mass_tot / del_t.to(u.yr)
	if not quiet:
		print 'Stellar mass formed in =', del_t
		print 'Then the SFR is', sfr

	return sfr.value

	f.close()

if __name__== "__main__":
	main()
