#Hi, here is the script that will convert your hyperion output to nice .fits files

#This script will take 3 parameters:
#1) The raw Hyperion output usually this is something like .rtout.image
#2) the directory in which you will put your images
#3)	redshift. This is needed to put your source at some distance which is got by 
#	assuming Planck2013 cosmology and a redshift (luminosity distance). In case
#	you want to put a distance directly, set z = 0 and change the default value to
#	whatever you want

import numpy as np
from hyperion.model import ModelOutput
from hyperion.util.constants import pc, lsun
from astropy.io import fits
from astropy.cosmology import Planck13
from astropy import units as u
import sys
import os
import shutil

model_name = '/vol/aibn1058/data1/sdeshmukh/RT_files/outputs/sampletests/gdgt_682/gdgt_682_n64.rtout.image'
output_dir = '/vol/aibn1058/data1/sdeshmukh/RT_files/outputs/fits_files_temp'
redshift = 0.0
default = 300 #pc

#Terminal input
if len(sys.argv) > 1:
	model_name = sys.argv[1]
	output_dir = sys.argv[2]
	redshift = float(sys.argv[3])

#Create directory structure
if os.path.exists(output_dir):
	print "Removing existing directory and creating again"
	shutil.rmtree(output_dir)
os.makedirs(output_dir)


#Load Image
print "Loading image file....."
m = ModelOutput(model_name)

#Get distance, i.e. at what distance the object has to be viewed
if redshift == 0:
	distance_to_source = default*pc
else:
	distance_to_source = Planck13.luminosity_distance(redshift).cgs.value
image = m.get_image(distance=distance_to_source, units='Jy')

#Print parameters used
print "Your raw image file:", model_name
print "Your output directory:", output_dir
print "Redshift and distance (default distance if z=0): z=", redshift, "d=",distance_to_source, "cms"
print "Number of viewing angles your image has:", image.val.shape[0]
print "Number of wavelengths your image has:", image.val.shape[3]
print "Your image can be viweved in the following wavelengths (rest frame) (microns):", image.wav

#Write to fits file. I will add custom fields in the header so that analyzing the fits should be easier later
print "Writing to fits files....."



for inclination in range(image.val.shape[0]):
	sub_dir_name=output_dir+"/view_"+str(inclination)
	os.makedirs(sub_dir_name)
	for wave_id in range(image.val.shape[3]):
		fits_name=sub_dir_name + "/" + str(image.wav[wave_id]*(1+redshift)) + ".fits"
		h = fits.Header()

		h['datafile'] = (model_name, 'name of data file')

		h['distance']=(distance_to_source, 'distance to object in cms')
		h['z'] = redshift

		h['wav_0'] = (image.wav[wave_id], '(mu) in rest frame')
		h['wav'] = (image.wav[wave_id]*(1+redshift), '(mu) in obs frame = image')
		h['wav_u'] = ('micron', 'Units for wavelength')

		h['f_0'] = (3*10**14 / image.wav[wave_id], '(Hz) in rest frame')
		h['f'] = (3*10**14 / ((1+redshift)*image.wav[wave_id]), '(Hz) in obs frame = image')
		h['f_u'] = ('Hz', 'Units for frequency')

		h['flux_u'] = (image.units, 'Units for the flux')

		h['xmin'] = (image.x_min,'cms')
		h['xmax'] = (image.x_max,'cms')
		h['ymin'] = (image.y_min,'cms')
		h['ymax'] = (image.y_max,'cms')
		h['xy_u'] = ('cm', 'Units of the spatial extent')

		h['ang'] = (image.pix_area_sr, 'strad, angular extent of image')
		h['ang_u'] = ('sr', 'Units of the angular extent')

		fits.writeto( fits_name, image.val[inclination,:,:,wave_id],header=h,clobber=True)
