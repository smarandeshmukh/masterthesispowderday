import numpy as np
import sys
import os
from hyperion.model import ModelOutput
import h5py
from astropy.cosmology import LambdaCDM
from astropy import units
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.integrate import simps
import cosmolopy
from astropy import constants as const

def main():
	try:
		sed_file = sys.argv[1]
		filt_trans_file = sys.argv[2]
		hdf5file = sys.argv[3]
		inclination = int(sys.argv[4])
		out = sys.argv[5]
		q = sys.argv[6]
	except:
		print 'Syntax:'
		print '[script name] [sed file] [filter file] [hdf5 file] [inclination id]  [output] [quiet=True/False]'
		raise Exception('Syntax Error')
	
	#Check whether to run in quiet mode or not
	if q == 'True':
		quiet = True
	else:
		quiet = False


	#Print user input
	if not quiet:
		print 'sed file:', sed_file
		print 'filter file:', filt_trans_file
		print 'hdf5file:', hdf5file
		print 'inclination id:', inclination
		print 'output file:', out

	#Extract cosmology from hdf5 file to get luminosity distance
	f = h5py.File(hdf5file)
	z = f['Header'].attrs['Redshift']
	H0 = f['Header'].attrs['HubbleParam']
	Om0 = f['Header'].attrs['Omega0']
	Ode0 = f['Header'].attrs['OmegaLambda']
	cosmo = LambdaCDM(H0=H0, Om0=Om0, Ode0=Ode0)
	d = ( cosmo.luminosity_distance(z) ).to(units.cm).value
	f.close()

	#Constant
	c = (const.c.to('micron/s')).value
	
	#Access sed
	model = ModelOutput(sed_file)
	sed = model.get_sed(distance = d, inclination='all', aperture = -1, units='ergs/cm^2/s')
	w = sed.wav
	nu = sed.nu
	flux = sed.val

	#Set resolution
	a = np.arange(np.amin(w), np.amax(w), 0.0001)

	#Interpolate sed to get better resolution
	sed_intp = interp1d(w, flux[inclination,:], kind='linear', bounds_error=False, fill_value=0)

	#Get the monochromatic flux here and mag
	mono = 0.15
	mono_flux_dens = sed_intp(mono) / (c/mono)
	mono_flux_dens_Jy = mono_flux_dens * 10**23
	mono_m = -2.5*np.log10(mono_flux_dens_Jy/3631)

	#Access filter
	filt = np.loadtxt(filt_trans_file)
	#DeRedshift the filter and change units from angstrom to microns
	filt_wav = filt[:,0] / ( 10000*(1+z) )
	filt_trans = filt[:,1]
	#Define interpolation to accurately get filter profile
	filt_intp = interp1d(filt_wav, filt_trans, kind='linear', bounds_error=False, fill_value=0)

	#Convert sed fluxes to flux density f_lambda
	flux_dens = sed_intp(a) / a

	#filter transmission array interpolation
	filt_t = filt_intp(a)

	#Compute filter flux in this block
	#frequency array as defined by interpolation
	nu_a = c / a
	#Compute normalization factors
	norm_nu = np.abs(simps( filt_t, c / a ))
	norm_lambda = simps(filt_t, a)
	#Divide total energy by norm
	filt_flux = simps(flux_dens*filt_t, a)
	filt_flux_dens_nu = filt_flux / norm_nu
	filt_flux_dens_lambda = filt_flux / norm_lambda
	
	#Convert to Jy
	filt_flux_dens_nu_Jy = filt_flux_dens_nu * 10**23
	filt_flux_dens_lambda_Jy = filt_flux_dens_lambda * 10**23

	#Convert to AB apparent magnitude
	m = -2.5*np.log10(filt_flux_dens_nu_Jy/3631)
	#Compute distance in units of 10pc
	d_10_pc = ( (d*units.cm).to(units.pc) / 10 ).value
	M = m - 5*np.log10( d_10_pc )
	mono_M = mono_m - 5*np.log10( d_10_pc )

	#print calculations	
	if not quiet:
		print 'flux spectral density f_nu (erg/s/cm2/Hz):', filt_flux_dens_nu
		print 'Apparent magnitude:', m
		print 'Distance in 10*pc:', d_10_pc
		print 'Absolute magnitude:', M
		print 'Monochromatic flux density:', mono_flux_dens
		print 'Monochromatic Abs magnitude:', mono_M

	#Parse sed file for halo id
	halo_id_str = ( (sed_file.split('/')[-1]).split('.')[0] )[3:]

	#Also calculate mean wavelength of filter in microns
	lambda_mean = simps(a*filt_t, a) / simps(filt_t, a)

	#Write to file
	if not os.path.exists(out):
		outputfile = open(out, 'w')
		outputfile.write(halo_id_str + ' ' + str(M) + ' ' + str(m) + ' ' + str(filt_flux_dens_nu) + ' ' )
		outputfile.write(str(mono) + ' ' + str(mono_M) + ' ' + str(lambda_mean) + '\n')
		outputfile.close()

	else:
		outputfile = open(out, 'a')
		outputfile.write(halo_id_str + ' ' + str(M) + ' ' + str(m) + ' ' + str(filt_flux_dens_nu) + ' ' )
		outputfile.write(str(mono) + ' ' + str(mono_M) + ' ' + str(lambda_mean) + '\n')
		outputfile.close()
	
	"""
	#Plotting routine for filter curves
	b = sed_intp(a) / a
	#plt.loglog(w, flux[inclination,:], '+')
	plt.loglog(a, filt_t)
	plt.loglog(3*10**14/a, filt_t)
	plt.show()
	"""

if __name__=='__main__':
	main()
