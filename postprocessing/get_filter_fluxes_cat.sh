#!/bin/bash

echo 'hi'

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/get_filter_fluxes_v3.py'
inp_fol='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/30052016'
filter_fol='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/'

file_list=($inp_fol/*/*rtout.sed)

for file in ${file_list[@]}
do
	sed_fol=`dirname $file`/mag_not_z_corrected.txt
	echo $sed_fol
	python $exe $file $filter_fol $sed_fol
done
