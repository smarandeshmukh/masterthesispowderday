#!/bin/bash

echo 'Hi'

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/get_sfr_cat2.py'
inp='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/03052016'
out='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/03052016/sfr_sed2.txt'

#Remove any old file under that name
rm -f $out

basename="${out%.*}"

rm -f $basename*.txt

file_list=($inp/*/*rtout.sed)

#echo ${file_list[@]}

echo '#haloid(0) number of inclinations(1) inclination_num(2) TIR(3) 24mu(4) 70mu(5) FUV with 25mu(6) FUV with TIR(7)'>>$out

for file in ${file_list[@]}
do
#	echo $file
	python $exe $file $out
done
