import numpy as np
from hyperion.model import ModelOutput
import os
import sys
from scipy.interpolate import interp1d
from scipy.integrate import simps
import matplotlib.pyplot as plt
import cosmolopy

def main():
	
	quiet = True

	try:
		inp = sys.argv[1]
		fil_fol = sys.argv[2]
		output = sys.argv[3]
	except:
		print 'Not enough inputs'
		print 'Syntax:', '[script name] [input sed file] [filter root folder] [output file]'
		raise Exception()

	#Print user input here
	if not quiet:
		print 'Input sed:', inp
		print 'Filter transmission file:', fil_fol

	#Access sed file
	m = ModelOutput(inp)
	sed = m.get_sed(aperture=-1, distance=None, inclination='all', units='ergs/s')
	w = sed.wav
	nu = sed.nu
	flux = sed.val

	#Erase and open output file
	o = open(output, 'w')
	o.close()
	o = open(output, 'a')
	#Write header
	o.write('#Filter name #Faceon #45deg #Edge #sim_x #sim_y #sim_z \n')
	o.write('#Magnitudes are from the AB system \n')

	for filter_name in os.listdir(fil_fol):
		full_path = os.path.join(fil_fol, filter_name)

		#Access filter transmission file
		t = np.loadtxt(full_path)
		#Change units to microns
		t[:,0] = t[:,0] / 10**4

		#interpolate the filter transmission
		filt_intp = interp1d(t[:,0], t[:,1], kind='linear', bounds_error=False, fill_value=0)

		#Calculate the mean wavelength
		lambda_mean = np.trapz( t[:,1]*t[:,0], t[:,0] ) / np.trapz(t[:,1], t[:,0])
		#Convert to angstrom
		lambda_mean = lambda_mean * 10000

		if not quiet:
			print full_path, 'mean lambda:', lambda_mean

		mags_string = ''

		for inclination in range(len(flux)):

			if not quiet:
				print '\t', inclination

			#interpolate sed
			sed_intp = interp1d(w, flux[inclination, :], kind='linear', bounds_error=False, fill_value=0)
			
			#Set resolution
			wavelength_coverage = np.arange(np.amin(w), np.amax(w), 0.001)
			
			#Calculate flux going through filter
			fil_flux = simps(sed_intp(wavelength_coverage) * filt_intp(wavelength_coverage), wavelength_coverage)

			#In case you want to print some diagnostic stuff
			"""
			#Plot filter data points and interpolation
			fig = plt.figure(figsize = (16,9))
			ax = fig.add_subplot(1,1,1)
			x_new = np.arange(0.99*np.amin(f[:,0]),1.01*np.amax(f[:,0]), 0.0001)
			ax.plot(f[:,0], f[:,1], 'kx', markersize=10)
			ax.plot(x_new, func1(x_new))
			fig.savefig('pls_delete.png')
			"""
			
			"""
			#Plot sed and the transmitted flux
			fig2 = plt.figure(figsize = (16,9))
			ax2 = fig2.add_subplot(1,1,1)
			ax2.loglog(w, flux[0, :])
			fil_flux = np.array([ flux[0, i] * func1(w[i]) for i in range(len(w))  ])
			ax2.loglog(w, fil_flux )
			fig2.savefig('pls_delete2.png')
			"""

		#	print np.average(f[:,0]), np.average(f[:,0], weights=f[:,1])

			#Divide total flux in band by mean lambda to reconvert to flux density. (Weird....)
			flux_in_band_dens_nu = fil_flux / np.abs( simps(t[:,1], (3*10**14)/t[:,0]) )
			mag = -2.5*np.log10( ( flux_in_band_dens_nu*10**23 / ( 4*3.14*(3.09*10**19)**2 ) ) / 3631 )

			mags_string = mags_string + str(mag) + ' '

		#Write to file
		o.write(filter_name + ' ' + mags_string + '\n')

	#Close output file
	o.close()

if __name__=='__main__':
	main()
