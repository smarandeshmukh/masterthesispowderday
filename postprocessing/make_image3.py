import numpy as np
import os
import sys
from hyperion.model import ModelOutput
import shutil
from astropy.io import fits
from astropy.cosmology import LambdaCDM
import astropy.units as u


def main():

	quiet=False

	def writeheader(image, i, w):
		h = fits.Header()
		h['datafile'] = (inp, 'name of data file')
		h['distance'] = (distance, 'cms')
		h['redshift'] = (redshift, 'z')
		h['xmin'] = (image.x_min,'cms')
		h['xmax'] = (image.x_max,'cms')
		h['ymin'] = (image.y_min,'cms')
		h['ymax'] = (image.y_max,'cms')
		h['ang'] = (image.pix_area_sr, 'strad, angular extent of image')
		h['lat_min'] = (image.lat_min,'degrees')
		h['lat_max'] = (image.lat_max,'degrees')
		h['lon_min'] = (image.lon_min,'degrees')
		h['lon_max'] = (image.lon_max,'degrees')

		
		return h

	#Check for user inputs
	try:
		inp = sys.argv[1]
		out_fol = sys.argv[2]
	except:
		print 'Not enough arguments!'
		print 'Syntax: [script name] [input file] [output folder] [extra arguments....]'
		print 'Extra arguments syntax: [option] [value]'
		print '\t options=redshift,funits'
		print '\t values=redshift->float!!!, funits=(ergs/s, ergs/cm^2/s, ergs/cm^2/s/Hz, Jy, mJy, MJy/sr)'
		print 'If redshift is provided you need to supply cosmology. Full example:'
		print 'python make_image3.py blah.rtout.image /blah/images2 redshift 0.5 H0 69 Om0 0.3 Ode0 0.7 funits MJy/sr'
		print 'Note: distance will be angular diameter distance'

		raise Exception('Not enough arguments!')
	#Set default parameters
	funits = 'ergs/s'
	redshift = 0
	distance = None
	
	#Check for additional parameters
	if len(sys.argv) == 3:
		if not quiet:
			print "Using default parameters. Fluxes will be in erg/s."
	else:
		args = sys.argv[3:]
		if 'redshift' in args:
			redshift = float(args[args.index('redshift') + 1 ])
			H0 = float(args[args.index('H0') + 1 ])
			Om0 = float(args[args.index('Om0') + 1 ])
			Ode0 = float(args[args.index('Ode0') + 1 ])
			cosmo = LambdaCDM(H0 = H0, Om0=Om0, Ode0=Ode0)
			distance =  cosmo.angular_diameter_distance(redshift).to(u.cm).value
		if 'funits' in args:
			funits = args[args.index('funits') + 1 ]
		if 'quiet' in args:
			quiet=True

	#Access model and data
	m = ModelOutput(inp)
	im = m.get_image(inclination='all', units = funits, distance = distance)
	wav = im.wav
	f = im.val
#	print f.shape, len(f), funits, im.units, im.lat_min, im.lat_max
	
	#Delete all contents in the output folder if it exists
	if os.path.exists(out_fol):
		shutil.rmtree(out_fol)
		if not quiet:
			print "Deleted exising output folder"

	#Create image folders
	counter = 0
	for inclination in f:
		fol_name = 'view' + str(counter)
		counter+=1
		path = os.path.join(out_fol, fol_name)
		os.makedirs(path)
		if not quiet:
			print "Inclination", counter
		for wave_id in range(inclination.shape[2]):
			data = inclination[:,:,wave_id]
			if not quiet:
				print "\tWavelength no.:", wave_id, "=", wav[wave_id] * (1 + redshift)
			wavelength = wav[wave_id] * (1 + redshift)
			fits_name = os.path.join(path, 'lambda_' + str(wavelength) + '.fits')
			fits.writeto(fits_name, data, header=writeheader(im, inclination, wave_id), clobber = True, checksum = True)
	
	


if __name__=='__main__':
	main()
