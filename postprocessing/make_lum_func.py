import numpy as np
import matplotlib.pyplot as plt
import sys

def main():

	try:
		inp = sys.argv[1]
		out = sys.argv[2]
	except:
		print "Correct Syntax: [script name] [input file] [output file]"
		raise Exception('Wrong Syntax')
	
	#Parsa et 2015 Schechter func
	def Schechter(M, M_0, phi_0, alpha):
		
		return 0.4*np.log(10) * phi_0 * 10**( -0.4*(M-M_0)*(alpha+1)  ) * np.exp( -10**(-0.4*(M-M_0)) )
	
	
	#Read data
	d = np.loadtxt(inp)
	haloid = d[:,0]
	mag_face_on_f = d[:,1]
	mag_face_mono = d[:,5]
	real_w = str( int(d[0,-1] *10000) )
	mono_w = str( int(d[0,4] *10000) )

#	print mag_face_on_f
	print real_w

	#Make histogram, bincenters and binwidth
	n, bin_edges = np.histogram(mag_face_on_f, bins = 14)
	bincenters = ( bin_edges[:-1] + bin_edges[1:] ) / 2
	n_mono, bin_edges_mono = np.histogram(mag_face_mono, bins = 14)
	bincenters_mono = ( bin_edges_mono[:-1] + bin_edges_mono[1:] ) / 2

	#normalization
	h = 0.678
	volume_wo_h = (12 / h )**3
	binwidth = bincenters[1] - bincenters[0]
	binwidth_mono = bincenters_mono[1] - bincenters_mono[0]
	norm = volume_wo_h*binwidth
	norm_mono = volume_wo_h*binwidth_mono
	n_norm = n / norm
	n_norm_mono = n_mono / norm_mono

	#Errorbars linear
	err_count = np.sqrt(n)
	err = err_count / norm
	err_count_mono = np.sqrt(n_mono)
	err_mono = err_count_mono / norm_mono

	#Errorbars log
	err_mono_log_up = np.abs( np.log10(1 + err_mono / n_norm_mono) )
	err_mono_log_down = np.abs( np.log10(1 - err_mono / n_norm_mono) )

	print n_norm, n_norm_mono
	print bincenters, bincenters_mono
	
	a = np.arange(np.amin(bincenters) - 0.5, np.amax(bincenters) + 0.5, 0.1)


	#Plotting
	fig = plt.figure(figsize = (16,9) )
	ax = fig.add_subplot(1,1,1)

#	ax.set_yscale("log")

	#Our stuff
#	ax.errorbar(bincenters, n_norm, yerr = err, label='STD_DYN_3, V_filter, z = 3.109, $M_{' + str(real_w) + '}$', alpha=0.2)
#	ax.errorbar(bincenters_mono, n_norm_mono, yerr = err_mono, label='STD_DYN_3, monochromatic, z = 3.109, $M_{' + str(mono_w)  + '}$')

	#Log plots
	ax.errorbar(bincenters_mono, np.log10(n_norm_mono), yerr=(err_mono_log_down,err_mono_log_up) ,label='STD_DYN_3, z = 3.109, $\lambda = ' + str(mono_w)  + '$')

	"""
	#Literature fits
	ax.plot(a,Schechter(a, -20.20, 0.00532, -1.31), '--', label='Parsa et al. 2015 z = 2.8, $\lambda=1500$')
	ax.plot(a,Schechter(a, -21.07, 0.00140, -1.60), '--', label='Arnouts et al. 2005 z = 3.0, $\lambda=1500$')
	ax.plot(a,Schechter(a, -20.45, 0.00410, -1.36), '--', label='Weisz et al. 2014 z = 3.0, $\lambda=1500$')
	ax.plot(a,Schechter(a, -20.90, 0.00167, -1.43), '--', label='Sawicki & Thomson 2006 z = 3.0, $\lambda=1700$')
	ax.plot(a,Schechter(a, -20.94, 0.00179, -1.65), '--', label='van der Burg, Hildebrant & Erben 2010 z = 3.0, $\lambda=1600$')
	ax.plot(a,Schechter(a, -20.97, 0.00171, -1.73), '--', label='Reddy & Steidel 2009 z = 3.05, $\lambda=1700$')
	"""

	#Literature log plots

	ax.plot(a,np.log10(Schechter(a, -20.20, 0.00532, -1.31)), 'r--', label='Parsa et al. 2015 z = 2.8, $\lambda=1500$')
	ax.plot(a,np.log10(Schechter(a, -21.07, 0.00140, -1.60)), 'g--', label='Arnouts et al. 2005 z = 3.0, $\lambda=1500$')
#	ax.plot(a,np.log10(Schechter(a, -20.45, 0.00410, -1.36)), 'c--', label='Weisz et al. 2014 z = 3.0, $\lambda=1500$')
#	ax.plot(a,np.log10(Schechter(a, -20.90, 0.00167, -1.43)), 'm--', label='Sawicki & Thomson 2006 z = 3.0, $\lambda=1700$')
#	ax.plot(a,np.log10(Schechter(a, -20.94, 0.00179, -1.65)), 'y--', label='van der Burg, Hildebrant & Erben 2010 z = 3.0, $\lambda=1600$')
	ax.plot(a,np.log10(Schechter(a, -20.97, 0.00171, -1.73)), 'k--', label='Reddy & Steidel 2009 z = 3.05, $\lambda=1700$')


	#Literature data points
	Reddy_data_x = np.array([-22.77, -22.27, -21.77, -21.27, -20.77, -20.27, -19.77, -19.27, -18.77])
	Reddy_data_y = np.array([3,30,85,240,686,1530,2934,4296,5536.])
	Reddy_data_err = np.array([1,13,32,104,249,273,333,432,601.])
	ax.errorbar(Reddy_data_x, np.log10(Reddy_data_y*1e-6), yerr = Reddy_data_err/Reddy_data_y, color='k', linestyle='None', marker='o' )

	A_d = np.loadtxt('Arnouts2005.csv', dtype=float)
	A_up = np.loadtxt('Arnouts2005_err_up.csv')
	A_down = np.loadtxt('Arnouts2005_err_down.csv')
	ax.errorbar(A_d[:,0], A_d[:,1], yerr=(A_d[:,1] - A_down[:,1], A_up[:,1] - A_d[:,1]),  color='g', linestyle='None', marker='o')

	Parsa_data_x = np.array([-22.5,-22,-21.5,-21,-20.5,-20,-19.5,-19,-18.5,-18,-17.5,-17,-16.5,-16,-15.5])
	Parsa_data_y = np.array([3,23,117,462,1462,2511,3830,4387,7382,8353,12432,12238,12821,15599,14625])
	Parsa_data_err = np.array([1,4,8,16,107,140,173,185,838,892,1088,1079,1105,1216,971.])
	ax.errorbar(Parsa_data_x, np.log10(Parsa_data_y*1e-6), yerr = Parsa_data_err/Parsa_data_y, color='r', linestyle='None', marker='o')

	#Limits
	ax.set_xlim( (-24, -17.5) )

	#Labels
	ax.set_xlabel('M', size = 24)
	ax.set_ylabel('$\Phi(\mathrm{N/mag/Mpc}^3)$', size = 24)
	#Grid and legend
	ax.grid(True, alpha = 0.4)
	ax.legend(loc=4)

	fig.savefig(out)

	"""
	#Access bin edges
	x_1 = hist[1]

	#Calculate binsize and normalize to binsize and density
	binsize = x_1[1] - x_1[0]
	y = hist[0] / (12.0/0.678)**3 * (1/ binsize)
	#linear error
	y_error_lin = np.sqrt(hist[0]) / (12.0/0.678)**3 * (1 / binsize)
	y_err_log_up = np.abs( np.log10(1 + y_error_lin / y) )
	y_err_log_down = np.abs( np.log10(1 - y_error_lin / y) )

	x = np.array([ (x_1[i] + x_1[i+1])/2 for i in range( len(x_1) - 1) ])
	plt.errorbar( x,np.log10(y), yerr = [y_err_log_up, y_err_log_down] )
	plt.xlabel('Magnitude M$_{AB}$ at ' + real_w + ' Angstrom', size = 24)
	plt.ylabel('log Number Density N/Mpc$^3$ mag$^{-1}$', size = 24)
#	plt.ylim(np.amin(y), 1.1*np.amax(y))
	plt.grid(True)
	plt.show()

	print y, x
	"""


if __name__=='__main__':
	main()
