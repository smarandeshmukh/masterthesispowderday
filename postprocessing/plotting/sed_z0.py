import numpy as np
import os
import sys
from hyperion.model import ModelOutput
import matplotlib.pyplot as plt

#Does your sed file have uncertainties?
uncertainty_option = False



if __name__=='__main__':

	quiet = False

	#Get main user input
	try:
		sed_file_name = sys.argv[1]
		out = sys.argv[2]
	except IndexError:
		print 'This script plots the sed from a hyperion output file.'
		print 'Syntax: [script name] [hyperion model output file] [output file] [other args]'
		print '[other args] can take the followning values:'
		print '[inclination value] = [-1, 0,1,2....] which inclination to plot the sed at (-1 means all inclinations)'
		print '[showlabels] This will overlay some labels showing the important emission lines'
		raise IndexError('Check the syntax')

	#Get the optional arguments
	inclinations_int = -1 #Default
	show_labels_opt = False
	try:
		inclinations_int = int(sys.argv[3])
		if inclinations_int == -1:
			inclinations_opt = 'all'
		else:
			inclinations_opt = inclinations_int
	except:
		inclinations_opt = 'all'
	
	try:
		show_labels_string = sys.argv[4]
		if show_labels_string == 'showlabels':
			show_labels_opt = True
	except:
		show_labels_opt = False
	
	#Access the model
	try:
		m = ModelOutput(sed_file_name)
	except IOError:
		raise IOError('Could not access the hyperion sed file.')

	#Access the sed
	sed = m.get_sed(inclination=inclinations_opt, distance = None, aperture=-1, uncertainties=uncertainty_option)

	#Get wavelengths and fluxes and uncertainties
	wav = sed.wav
	nu = sed.nu
	units = sed.units
	flux = sed.val

	# *Effing* Hyperion does not know spelling
	if units == 'ergs/s':
		units = 'erg/s'
	#Change to 2d array if it isnt
	if flux.ndim == 1:
		flux = flux[None, :]

	#If uncertainty is specified
	if uncertainty_option:
		unc = sed.unc
		if unc.ndim == 1:
			unc = unc[None, :]
		#Calculate the log errors
		err_minus = np.zeros( (len(unc), len(unc[0]) ) )
		err_plus = np.zeros( (len(unc), len(unc[0]) ) )
		for i in range(len(unc)):
			with np.errstate(invalid='ignore'):
				err_minus[i] = np.abs( np.log10(1 - unc[i] / flux[i]) )
				err_plus[i] = np.abs( np.log10(1 + unc[i] / flux[i]) )

	#Change everything to log
	logw = np.log10(wav)
	lognu = np.log10(nu)
	with np.errstate(divide='ignore'):
		logflux = np.log10(flux)
	
	#Initialize plot
	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot(1,1,1)

	#Plot
	#for i in range(len(flux)):
		#Temp for special galaxy
	for i in [0,4]:
		ax.plot(logw, logflux[i])

	#Limits
	ax.set_xlim(-1.2, 3)
	ax.set_ylim(40, 46)

	fig.savefig(out)

	"""
	print flux.shape, unc.shape, flux.ndim, unc.ndim
	print err_minus.shape
	print err_minus[:, 20:30]
	"""

	
