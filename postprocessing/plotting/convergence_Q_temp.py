import numpy as np
import os
import sys
import matplotlib.pyplot as plt

percentile = 98.0
Del_tol = 1.02
Q_tol = 2





if __name__=='__main__':
	
	#User input
	try:
		out1 = sys.argv[1]
		out2 = sys.argv[2]
		Q_files = sys.argv[3:]
	except IndexError:
		print 'This will print the covnergence plots given convergence files'
		print 'Syntax: [script name] [output file Q] [output file Delta] [list of Q files]'
		raise IndexError('Check Syntax')

	for filename in Q_files:
		assert os.path.exists(filename), 'File not found: %s' % filename

	print 'Output files:', out1, out2
	print 'Number of Q_files:', len(Q_files)
	print 'Q_files:', Q_files

	#Intialize plots
	fig1 = plt.figure( figsize = (16,9) )
	ax1 = fig1.add_subplot(1,1,1)

	fig2 = plt.figure( figsize = (16,9) )
	ax2 = fig2.add_subplot(1,1,1)

	#colors
	colors = ['b', 'r']

	#linewdiths
	lw = [7, 10]

	#labels
	lab1 = ['$\mathrm{Number\ of\ photons} = 3\cdot 10^8 $', '$\mathrm{Number\ of\ photons} = 1\cdot 10^7 $']

	#Access data and plot
	for i in range( len(Q_files) ):
		#Load into numpy
		d = np.loadtxt(Q_files[i])

		#Print the 95th percentile
		print d[:, int(percentile/100*len( d[0] ) )]

		#Plot the final Q values
		ax1.plot( 100 * np.arange(1., len( d[-1] ) ) / len(d[-1]),   d[-1][1:], color = colors[i] , linewidth = lw[i], alpha = 0.8, label = lab1[i] )
		

		j_list = []
		Delta_list = []
		#Access the Rs for each iteration
		for j in range( len(d) )[1:]:
			#Plot the Q ratios
			Q1 = d[j][ percentile / 100 * len(d[j]) ] 
			Q2 = d[j-1][ percentile / 100 * len(d[j]) ]
			Delta = np.maximum( Q1/Q2, Q2/Q1 ) 
			j_list.append(j)
			Delta_list.append(Delta)
		
		#And plot
		j_arr = np.array(j_list)
		Delta_arr = np.array( Delta_list )
		ax2.plot(j_arr, Delta_arr, color = colors[i], marker = 'o', label = lab1[i], linewidth = 4, markersize = 12)
	
	#Focus to the action scene
	ax1.set_xlim( 95, 100)
	ax1.set_ylim( 0, 20 )
	ax2.set_xlim( 0, 10 )
	ax2.set_ylim( 0.95, 1.1)

	#Ticks
	ax1.tick_params( labelsize = 18 )
	ax2.tick_params( labelsize = 18 )

	#Legend
	ax1.legend(loc = 2, fontsize = 24)
	ax2.legend(loc = 2, fontsize = 24)

	#Grid
	ax1.grid(True, alpha = 0.3)
	ax2.grid(True, alpha = 0.3)

	#Axis labels
	ax1.set_xlabel('$\mathrm{Percentile\ } p (\mathrm{\%})$', size = 26)
	ax1.set_ylabel('$R^{i}$', size = 26)
	ax2.set_xlabel('$\mathrm{Iteration\ No.}$', size = 26)
	ax2.set_ylabel('$\Delta^i$', size = 26)

	#Grey the good zone in Delta plot
	x2 = np.arange(0, np.amax( ax2.get_xlim() ) + 5 )
	ax2.fill_between( x = x2, y1 = np.ones(len(x2)) * Del_tol, y2 = np.ones(len(x2)) * 1, color = 'k', alpha = 0.2 )

	#Mark the tolerance levels in Q plot and the percentile
	x1 = np.arange(0.0, 101)
	ax1.fill_between( x = x1, y1 = np.ones(len(x1)) * Q_tol, color = 'k', alpha = 0.2 )
	ax1.plot(np.ones(2)*percentile, [0, np.amax(ax1.get_ylim() )], color = 'k', linestyle = 'dashed', linewidth = 3 )


	#Save figure
	fig1.savefig(out1, bbox_inches = 'tight')
	fig2.savefig(out2, bbox_inches = 'tight')
