import numpy as np
import sys
import os
import matplotlib.pyplot as plt
import h5py

inp = sys.argv[1]
out = sys.argv[2]

try:
	print 'Using params from hdf5 file'
	f = h5py.File( sys.argv[3] )
	l = f['Header'].attrs['BoxSize'] / 1000.0
	h = f['Header'].attrs['HubbleParam']
except:
	print 'Using default params'
	l = 12.0
	h = 0.678

print l,h

d = np.loadtxt(inp)


masses = d[:,1]

#Plot masses without h
logm = np.log10( masses/h )
#Plot masses with h
#logm = np.log10( masses )

#Histogram
n, binedges = np.histogram(logm)
bincenters = ( binedges[:-1] + binedges[1:] ) / 2
binwidth = binedges[1] - binedges[0]

#Norm
norm = l**3 / h**3 * binwidth

logn = np.log10(n/norm)

#Poisson errors
p_err = np.sqrt(n)

#Convert poisson errors to log errors
p_err = np.array( [  np.abs( np.log10(1 - p_err / n) ), np.abs( np.log10(1 + p_err/n) ) ] )

#Cosmic variance
c_err = 0
c_err = 0.2

#Initiliaze figure
fig = plt.figure(figsize = (16,9) )
ax = fig.add_subplot(1,1,1)

#Labels and ticks
plt.tick_params( labelsize = 16 )
ax.set_xlabel( '$\mathrm{Galaxy\ Stellar\ Mass}$ $ \mathrm{log}( M / M_\odot ) $', size = 24 )
ax.set_ylabel( '$\mathrm{log}(\phi / \mathrm{Mpc^3} / \mathrm{dex}) $ ', size = 24 )

#Limits
xlim = np.amin(bincenters) - 0.5
ylim = np.amax(bincenters) + 0.5
ax.set_xlim( xlim, ylim )

#Plot our data
ax.errorbar(bincenters, logn, yerr = p_err + c_err, label = 'This work', linewidth = 4, capsize = 8, capthick = 2)

#Plot Tomczak data
x_tom = np.array( [9.500, 9.752, 10.005, 10.243, 10.509, 10.748, 11.000, 11.252, 11.505, 11.757] )
y_tom = np.array( [-2.656, -2.767, -3.019, -3.214, -3.353, -3.730, -4.009, -4.149, -4.721, -5.363] )
err_minus_tom = np.array( [0.1,0.1,0.1,0.1,0.126,0.168,0.210,0.1,1.046, 0.418] )
err_plus_tom = np.array( [0.1,0.1,0.1,0.1,0.1,0.125,0.1,0.182,0.293,0.447] )
ax.errorbar( x_tom, y_tom, yerr=(err_minus_tom, err_plus_tom), label='Tomczak et. al, 2014', linewidth = 4, capsize = 8, capthick = 2 )

#Plot Tomczak fit
def Tomczak2014(x):
	M_0 = 11.35
	alpha = -1.74
	phi_0 = 10**(-4.36)
	return np.log(10)*phi_0*10**( (x - M_0)*(1+alpha) )*np.exp(-10**(x-M_0) )

x = np.arange(xlim, ylim, 0.01)
ax.fill_between(x, y1=np.log10( Tomczak2014(x)) + 0.2, y2 = np.log10( Tomczak2014(x)) - 0.8, color = 'k', alpha = 0.1 )
ax.plot(x, np.log10(Tomczak2014(x)), color = 'k', linestyle = 'dotted', linewidth = 2)

#Grid
ax.grid(True, alpha = 0.3)

#Legend
ax.legend(loc = 3, numpoints=1, fontsize = 20)


fig.savefig(out)

print n, p_err
print logn, p_err
