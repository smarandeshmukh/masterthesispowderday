import numpy as np
import os
import sys
import matplotlib.pyplot as plt



if __name__=='__main__':
	
	h = 0.678

	#inputs
	try:
		gas_frac_file = sys.argv[1]
		mass_file = sys.argv[2]
		out = sys.argv[3]
	except IndexError:
		print 'This plots the baryonic gas fraction against the galaxy stellar mass'
		print 'Syntax: [gas fraction] [mass file] [output]'
		raise IndexError('Check Syntax')

	#Load data
	gfrac = np.loadtxt(gas_frac_file)
	mass = np.loadtxt(mass_file)

	assert len(gfrac[:,1]) == len(mass[:,1]), 'Mass and gas_frac columns have different shape'

	#Initialize figure
	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot( 1,1,1 )

	#Ticks
	plt.tick_params( labelsize = 16 )

	#Plot
	ax.scatter( np.log10( mass[:,1] / h), gfrac[:,1], label = 'This work', alpha = 0.8, s = 40 )

	
	#Plot Popping
	Popping_data_raw = np.array([
		6.955529906986214, 0.8932038834951459,
		7.271029940932852, 0.8737864077669906,
		7.60750899585851, 0.854368932038835,
		8.05030891438658, 0.8009708737864081,
		8.492905153099326, 0.7524271844660195,
		8.93611243125806, 0.6893203883495147,
		9.380134428678115, 0.6067961165048545,
		9.781994704324806, 0.529126213592233,
		10.205241360581166, 0.44174757281553445,
		10.585918935433497, 0.36893203883495174,
		10.839907665150385, 0.31553398058252435,
		11.051530993278568, 0.27184466019417464])
	Popping_data_reshaped =  Popping_data_raw.reshape( (len(Popping_data_raw) / 2, 2) )
	Popping_x = Popping_data_reshaped[:,0]
	Popping_y = Popping_data_reshaped[:,1]
	ax.plot( Popping_x, Popping_y, linewidth = 5, alpha = 0.7, color='g', label = 'Popping, Somerville & Trager (2014), z = 3' )

	#Plot Popping, Caputi, etc...
	pop_cap_raw = np.array([
		8.74810, 0.98543,
		8.98106, 0.96613,
		9.19263, 0.93266,
		9.38312, 0.90869,
		9.62675, 0.89410,
		9.83869, 0.88904,
		10.03985, 0.86978,
		10.19861, 0.85060,
		10.34670, 0.82670,
		10.51556, 0.76962,
		10.57872, 0.73639,
		10.66221, 0.63685,
		10.73516, 0.54206,
		10.79769, 0.46149,
		10.84982, 0.39513,
		10.89121, 0.31933,
		10.95381, 0.24349,
		11.00556, 0.14873,
		11.17473, 0.11533,
		11.24875, 0.10101,
		11.47161, 0.11960,
		11.69309, 0.03404,
		11.92780, 0.14728])
	pop_cap_raw_reshaped = pop_cap_raw.reshape( (len(pop_cap_raw) / 2, 2) )
	pop_cap_x = pop_cap_raw_reshaped[:,0]
	pop_cap_y = pop_cap_raw_reshaped[:,1]
	ax.plot( pop_cap_x, pop_cap_y, linewidth = 5, alpha = 0.7, color='r', label = 'Popping et al. (2015), z=1.75-2' )




	#Limits
	ax.set_xlim( 7.7, 10.5)
	
	#Labels
	ax.set_xlabel(' $\mathrm{Galaxy\ Stellar\ Mass\ log}(\mathrm{M} / \mathrm{M}_\odot)$ ', size = 24) 
	ax.set_ylabel(' $\mathrm{Gas\ Fraction\ } f_\mathrm{gas} = M_\mathrm{gas} / M_\mathrm{gas + stars}  $ ', size = 24)

	#Legend
	ax.legend(loc = 3, fontsize = 20, scatterpoints = 1)

	#Grid
	ax.grid(True, alpha = 0.3)

	fig.savefig(out)
