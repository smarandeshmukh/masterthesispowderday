import numpy as np
import os
import sys
import matplotlib.pyplot as plt



if __name__=='__main__':
	
	h = 0.678

	#inputs
	try:
		met_file = sys.argv[1]
		mass_file = sys.argv[2]
		out = sys.argv[3]
	except IndexError:
		print 'This plots the Mass metallicity relation in the log scale with z in solar units.'
		print 'Syntax: [met file] [mass file] [output]'
		raise IndexError('Check Syntax')

	#Load data
	met = np.loadtxt(met_file)
	mass = np.loadtxt(mass_file)

	assert len(met[:,1]) == len(mass[:,1]), 'Mass and metal columns have different shape'

	#Initialize figure
	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot( 1,1,1 )

	#Ticks
	plt.tick_params( labelsize = 16 )

	#Plot
	ax.scatter( np.log10( mass[:,1] / h), np.log10(met[:,1]), label = 'This work', alpha = 0.8, s = 40 )

	#Plot Mannucci
	mannucci_data_raw = np.array([8.518518518518519, 0.06373057341948983,
	8.725925925925926, 0.08692214496115101,
	8.918518518518518, 0.11164160898985877,
	9.155555555555555, 0.1538000659665394,
	9.385185185185186, 0.20153376859417363,
	9.68888888888889, 0.2804310428135241,
	9.992592592592592, 0.37866997376558875,
	10.185185185185185, 0.44892512582186056,
	10.385185185185186, 0.5269132630526508,
	10.644444444444444, 0.6309573444801934,
	10.896296296296295, 0.733191449060232,
	11.088888888888889, 0.8103929461614218])
	mannucci_data_reshaped =  mannucci_data_raw.reshape( (len(mannucci_data_raw) / 2, 2) )
	mannucci_x = mannucci_data_reshaped[:,0]
	mannucci_y = mannucci_data_reshaped[:,1]
	ax.plot( mannucci_x, np.log10(mannucci_y), linewidth = 5, alpha = 0.7, color='g', label = 'Mannucci et al. (2009)' )

	#Limits
	ax.set_xlim( 7.7, 10.5)
	
	#Labels
	ax.set_xlabel(' $\mathrm{Galaxy\ Stellar\ Mass\ log}(\mathrm{M} / \mathrm{M}_\odot)$ ', size = 24) 
	ax.set_ylabel(' $\mathrm{Metallicity\ log}(\mathrm{Z} / \mathrm{Z}_\odot)$ ', size = 24)

	#Legend
	ax.legend(loc = 2, fontsize = 20, scatterpoints = 1)

	#Grid
	ax.grid(True, alpha = 0.3)

	fig.savefig(out)
