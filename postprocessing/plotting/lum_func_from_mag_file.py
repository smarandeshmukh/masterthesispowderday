import numpy as np
import os
import sys
import matplotlib.pyplot as plt




if __name__=='__main__':

	#Check user input
	try:
		magfile = sys.argv[1]
		output = sys.argv[2]
	except IndexError:
		print 'This computes the luminosity function given a magnitude file.'
		print 'Syntax: [script name] [magfile] [output] (optional ->) [h]'
		raise IndexError('Check Syntax')

	#Get h
	try:
		h = float(sys.argv[3])
	except:
		h = 0.678

	#Get edge length
	try:
		l = float(sys.argv[4])
	except:
		l = 12.0 / h

	#Load data
	d = np.loadtxt( magfile )
	mags = d[:,1]

	#Histogram
	n, binedges = np.histogram(mags, bins = 10, range=(np.amin(mags), -15))
	bincenters = ( binedges[1:] + binedges[:-1] ) / 2
	binwidth = bincenters[1] - bincenters[0]

	print bincenters, n, np.amin(mags), np.amax(mags)

	#Binlimits
	xmin = np.amin(bincenters) - 0.5
	xmax = np.amax(bincenters) + 0.5

	#Get the normalization
	norm = ( l / h)**3 * binwidth

	#Linear poisson error
	err = np.sqrt(n)

	#initialize plot
	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot(1,1,1) 

	#Ticks
	plt.tick_params(labelsize = 16)

	#Log errors
	lerr_minus = np.abs( np.log10( 1 - err / n ) )
	lerr_plus = np.abs( np.log10( 1 + err / n ) )

	#Cosmic Variance
	c_err = 0.2

	#Limits
	ax.set_xlim(xmin, xmax)

	#Grid
	ax.grid(True, alpha = 0.3)

	#Labels
	ax.set_xlabel(' $\mathrm{Magnitude\ M}_{%s} $' % str('UV'), size = 24 )
	ax.set_ylabel( '$\mathrm{log}(\phi / \mathrm{Mpc^3} / \mathrm{dex}) $ ', size = 24 )

	#Plot in log
	ax.errorbar( bincenters, np.log10( n/norm), yerr = (lerr_minus + c_err,lerr_plus + c_err), label = 'This work, $z = 3$', linewidth = 4, capsize = 8, capthick = 2 )

	#Parsa data raw linear
	Parsa_data_x = np.array([-22.5,-22,-21.5,-21,-20.5,-20,-19.5,-19,-18.5,-18,-17.5,-17,-16.5,-16,-15.5])
	Parsa_data_y = np.array([3,23,117,462,1462,2511,3830,4387,7382,8353,12432,12238,12821.,15599,14625])
	Parsa_data_err = np.array([1,4,8,16,107,140,173,185,838,892,1088,1079,1105,1216,971.])
	#Convert to log
	logParsa_data_y = np.log10(Parsa_data_y * 1e-6) 
	err_Parsa_minus = np.abs( np.log10(  1 - Parsa_data_err / Parsa_data_y) )
	err_Parsa_plus = np.log10( 1 + Parsa_data_err / Parsa_data_y )
	#Plot
	ax.errorbar(Parsa_data_x, logParsa_data_y, yerr = (err_Parsa_minus, err_Parsa_plus),  label = 'Parsa et al. 2015, $z = 2.8$', linewidth = 4, capsize = 6, capthick = 2)
	#Define Schechter
	def logSchechter(M, M_0, phi_0, alpha):
		return np.log10( 0.4*np.log(10) * phi_0 * 10**( -0.4*(M-M_0)*(alpha+1)  ) * np.exp( -10**(-0.4*(M-M_0)) ) )
	a = np.arange(xmin, xmax, 0.1)
	ax.fill_between(a, y1=logSchechter(a, -20.20-0.07, 0.00532 + 0.0006, -1.31+0.04), y2=logSchechter(a, -20.20+0.07, 0.00532 - 0.0006, -1.31-0.04), color = 'k', alpha = 0.1 )

	#Legend
	ax.legend(loc = 3, numpoints=1, fontsize = 20)

	fig.savefig(output)
