import numpy as np
import matplotlib.pyplot as plt
import sys

if __name__=='__main__':

	x_label = 'g - $\mathcal{R}$'
	y_label = 'u - g'

	#User input
	try:
		color_file = sys.argv[1]
		output = sys.argv[2]
	except IndexError:
		print 'Plots the color color diagram [input] [output]'
		raise IndexError('Check Syntax')

	d = np.loadtxt(color_file)
	
	#Load x,y colors
	x_col = d[:,1]
	y_col = d[:,3]

	#Initialise figure
	fig = plt.figure(figsize=(16,9))
	ax = fig.add_subplot(1,1,1)

	ax.scatter(x_col, y_col, s = 40, alpha = 0.8)


	#Tick size
	plt.tick_params( labelsize = 20)

	#Axes
	ax.set_xlabel(x_label,size = 24)
	ax.set_ylabel(y_label, size = 24)

	#Limits
	ax.set_xlim( (-1,4) )
	ax.set_ylim( (-1,4) )

	#Grid
	ax.grid(True, alpha = 0.2)


	fig.savefig(output)
