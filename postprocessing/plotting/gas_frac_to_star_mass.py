import numpy as np
import os
import sys
import matplotlib.pyplot as plt



if __name__=='__main__':
	
	h = 0.678

	#inputs
	try:
		gas_frac_file = sys.argv[1]
		mass_file = sys.argv[2]
		out = sys.argv[3]
	except IndexError:
		print 'This plots the  gas fraction (f_gas = gas / stars) against the galaxy stellar mass'
		print 'Syntax: [gas fraction] [mass file] [output]'
		raise IndexError('Check Syntax')

	#Load data
	gfrac = np.loadtxt(gas_frac_file)
	mass = np.loadtxt(mass_file)

	assert len(gfrac[:,1]) == len(mass[:,1]), 'Mass and gas_frac columns have different shape'

	#Initialize figure
	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot( 1,1,1 )

	#Ticks
	plt.tick_params( labelsize = 16 )

	#Plot
	ax.scatter( np.log10( mass[:,1] / h), np.log10(gfrac[:,1]), label = 'This work', alpha = 0.8, s = 40 )

	"""
	#Plot Mannucci
	Popping_data_raw = np.array([
	6.955529906986214, 0.8932038834951459,
	7.271029940932852, 0.8737864077669906,
	7.60750899585851, 0.854368932038835,
	8.05030891438658, 0.8009708737864081,
	8.492905153099326, 0.7524271844660195,
	8.93611243125806, 0.6893203883495147,
	9.380134428678115, 0.6067961165048545,
	9.781994704324806, 0.529126213592233,
	10.205241360581166, 0.44174757281553445,
	10.585918935433497, 0.36893203883495174,
	10.839907665150385, 0.31553398058252435,
	11.051530993278568, 0.27184466019417464])
	Popping_data_reshaped =  Popping_data_raw.reshape( (len(Popping_data_raw) / 2, 2) )
	Popping_x = Popping_data_reshaped[:,0]
	Popping_y = Popping_data_reshaped[:,1]
	ax.plot( Popping_x, Popping_y, linewidth = 5, alpha = 0.7, color='g', label = 'Mannucci et al. (2009)' )
	"""
	#Limits
	ax.set_xlim( 7.7, 10.5)
	
	#Labels
	ax.set_xlabel(' $\mathrm{Galaxy\ Stellar\ Mass\ log}(\mathrm{M} / \mathrm{M}_\odot)$ ', size = 24) 
	ax.set_ylabel(' $\mathrm{Gas\ Fraction\ log} f_\mathrm{gas} = \mathrm{log} M_\mathrm{gas} / M_\mathrm{stars}  $ ', size = 24)

	#Legend
	ax.legend(loc = 3, fontsize = 20, scatterpoints = 1)

	#Grid
	ax.grid(True, alpha = 0.3)

	fig.savefig(out)
