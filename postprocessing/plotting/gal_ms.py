import numpy as np
import sys
import os
import matplotlib.pyplot as plt

h = 0.678


if __name__=='__main__':

	#User input
	try:
		real_sfr_file = sys.argv[1]
		obs_sfr_file = sys.argv[2]
		mass_sfr_file = sys.argv[3]
		output = sys.argv[4]
	except IndexError:
		print 'This prints the galaxy main sequence. It plots the true one and the observed one.'
		print 'Syntax: [script name] [real sfr file] [obs sfr file] [mass file] [output]'
		raise IndexError('Check Syntax')

	err_msg = 'The following input file does not exist: '
	assert os.path.exists(real_sfr_file), err_msg + real_sfr_file
	assert os.path.exists(obs_sfr_file), err_msg + obs_sfr_file
	assert os.path.exists(mass_sfr_file), err_msg + mass_sfr_file

	#Access full data
	rsfr = np.loadtxt(real_sfr_file)
	osfr = np.loadtxt(obs_sfr_file)
	mass = np.loadtxt(mass_sfr_file)

	#Access specific columns
	real = rsfr[:,1]
	obs = osfr[:,1]
	m = mass[:,1]

	#Access errors if it exists
	try:
		error_is = True
		e_obs = osfr[:,2]
		#Convert to log errors here
		err_minus = np.abs( np.log10( 1 - e_obs/obs) )
		err_plus = np.abs( np.log10( 1 + e_obs/obs) )
		err_mean = ( err_minus + err_plus ) / 2
		err_norm = ( err_minus + err_plus ) / ( 2 * np.mean(  err_mean ))
	except:
		error_is = False
		e_obs = np.zeros(len(obs))

	#Initialize plots here
	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot( 1,1,1 )


	#We do not plot errors since they are too small to be seen
	#ax.errorbar( np.log10(m[i]), np.log10(obs[i]), yerr =  err_mean[i])

	#Plot scatter OBS without errors
	ax.scatter( np.log10( m[:len(obs)] / h), np.log10(obs), alpha = 1, s = 40, color = 'r', label='Inferred SFR' )

	#Plot scatter REAL without errors
	ax.scatter( np.log10( m[:len(real)] / h), np.log10(real), alpha = 0.7, s = 40, color = 'k', label='Intrinsic SFR'  )

	#Tick
	plt.tick_params( labelsize = 16 )

	#Labels
	ax.set_xlabel(' $\mathrm{Galaxy\ Stellar\ Mass\ log} \mathrm{(M/M_\odot)} $ ', size = 24)
	ax.set_ylabel(' $\mathrm{ log(SFR/M_\odot yr^{-1})}$ ', size = 24)

	#Grid
	ax.grid(True, alpha = 0.3)

	#Legend
	ax.legend(loc = 2, fontsize = 24, scatterpoints=1)

	fig.savefig(output)
