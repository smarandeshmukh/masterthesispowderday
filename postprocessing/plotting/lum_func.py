import numpy as np
import os
import sys
import matplotlib.pyplot as plt




if __name__=='__main__':
	
	quiet = False

	#User input
	try:
		inp = sys.argv[1]
		out = sys.argv[2]
	except IndexError:
		raise IndexError('Check Syntax')

	#load data
	data = np.loadtxt(inp)
	d = data[:,1]
	try:
		e = data[:,2]
	except: 
		e = np.zeros(len(d))
	
	#Extract wavelength from header or input
	try:
		w = float(sys.argv[3])
		if not quiet:
			print 'wavelength taken from user argument'
	except:
		l1 = open(inp).readline()
		w = float(l1.split(':')[1].split()[0])
		if not quiet:
			print 'wavelength taken from header'

	#Volume data
	l = 12.0 #Mpc/h
	h = 0.678

	#Change microns to angstrom
	w = 10000*w

	#Now start converting the luminosity to Magnitude system AB
	dis = 4*np.pi*(10*3.086e18)**2

	#Get frequency of the light
	freq = 3*10**18 / w

	#Norm factor for AB system
	J = 3631 #Jy

	#Finally convert to magnitudes
	d1 = -2.5*np.log10( (d / (dis * freq)) * 1e23 / J  )
	print d1

	#Histogram
	n, binedges = np.histogram(d1)
	bincenters = ( binedges[:-1] + binedges[1:] ) / 2
	binwidth = binedges[1] - binedges[0]
	print n, bincenters

	#Norm
	norm = (l/h)**3 * binwidth

	#Log
	logn = np.log10( n / norm )

	#Poisson error
	p_err = np.sqrt(n)

	#Cosmic variance in log
	c_err = 0.2

	#Error in the flux
	if np.any(e):
		f_err = np.abs(-2.5 / d * e)
	else:
		f_err = 0
	
	#Total error in log
	t_err = np.array( [ np.abs( np.log10( 1 - p_err / n) ), np.log10( 1 + p_err / n) ] )

	#Initialise histogram
	fig = plt.figure(figsize = (16,9) )
	ax = fig.add_subplot(1,1,1)

	#Labels and ticks
	plt.tick_params( labelsize = 16 )
	ax.set_xlabel(' $\mathrm{Magnitude\ M}_{%s} $' % str(int(w)), size = 24 )
	ax.set_ylabel( '$\mathrm{log}(\phi / \mathrm{Mpc^3} / \mathrm{dex}) $ ', size = 24 )

	#Limits
	xmin = np.amin(bincenters) - 0.5
	ymin = np.amax(bincenters) + 0.5
	ax.set_xlim( xmin, ymin )

	#Grid
	ax.grid(True, alpha = 0.3)

	#Plot
	ax.errorbar(bincenters, logn, yerr = t_err + c_err, label = 'This work, $z = 3$', linewidth = 4, capsize = 8, capthick = 2)

	#Parsa data raw linear
	Parsa_data_x = np.array([-22.5,-22,-21.5,-21,-20.5,-20,-19.5,-19,-18.5,-18,-17.5,-17,-16.5,-16,-15.5])
	Parsa_data_y = np.array([3,23,117,462,1462,2511,3830,4387,7382,8353,12432,12238,12821.,15599,14625])
	Parsa_data_err = np.array([1,4,8,16,107,140,173,185,838,892,1088,1079,1105,1216,971.])
	#Convert to log
	logParsa_data_y = np.log10(Parsa_data_y * 1e-6) 
	err_Parsa_minus = np.abs( np.log10(  1 - Parsa_data_err / Parsa_data_y) )
	err_Parsa_plus = np.log10( 1 + Parsa_data_err / Parsa_data_y )
	#Plot
	ax.errorbar(Parsa_data_x, logParsa_data_y, yerr = (err_Parsa_minus, err_Parsa_plus),  label = 'Parsa et al. 2015, $z = 2.8$', linewidth = 4, capsize = 6, capthick = 2)
	#Define Schechter
	def logSchechter(M, M_0, phi_0, alpha):
		return np.log10( 0.4*np.log(10) * phi_0 * 10**( -0.4*(M-M_0)*(alpha+1)  ) * np.exp( -10**(-0.4*(M-M_0)) ) )
	a = np.arange(xmin, ymin, 0.1)
	ax.fill_between(a, y1=logSchechter(a, -20.20-0.07, 0.00532 + 0.0006, -1.31+0.04), y2=logSchechter(a, -20.20+0.07, 0.00532 - 0.0006, -1.31-0.04), color = 'k', alpha = 0.1 )

	#Legend
	ax.legend(loc = 3, numpoints=1, fontsize = 20)
	


	fig.savefig(out)
