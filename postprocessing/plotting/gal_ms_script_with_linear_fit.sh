#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/plotting/gal_ms_with_linear_fit.py'
real='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/04072016/sfr_hist_100Myr_100Myr.txt'
obs='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/31082016/SFR_24mu.txt'
masses='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/04072016/sim_gal_mass_full.txt'
output='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/31082016/gal_ms_24_0.png'

python $exe $real $obs $masses $output
