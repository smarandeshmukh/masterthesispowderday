import numpy as np
import os
import sys
import matplotlib.pyplot as plt


def lum_to_mag(lum, w):
	
	distance = 3.086e19
	freq = 3*10**14 / w
	J = 3631.0

	mag = -2.5 * np.log10( lum / (4 * np.pi * distance**2) * (1 / freq) * (1e23 / J) )

	return mag


if __name__=='__main__':

	#Input
	try:
		esc_frac_file = sys.argv[1]
		lum_file = sys.argv[2]
		output = sys.argv[3]
	except IndexError:
		print 'This plots the escape fraction against the magnitude.'
		print 'Syntax: [script name] [escape fraction file] [luminosity file] [output file]'
		raise IndexError('Check Syntax')

	assert os.path.exists(esc_frac_file), 'File does not exist: %s' % esc_frac_file
	assert os.path.exists(lum_file), 'File does not exist: %s' % lum_file

	#Get data
	esc_full = np.loadtxt(esc_frac_file)
	lum_full = np.loadtxt(lum_file)

	#Access columns
	e = esc_full[:,1]
	l = lum_full[:,1]

	#Access header to get wavelegnth
	line1 = open(lum_file, 'r').readlines()[0]
	try:
		wavelength = float( line1.split(': ')[1].split(' ')[0] )
	except:
		wavelength = 0.15
		print 'Could not get wavelength from file. Using (microns): ', wavelegnth

	#Convert luminosities to magnitudes
	m = lum_to_mag( l, wavelength )

	assert len(m) == len(e), 'Lum array and esc array are not the same shape'

	#Plotting

	#Initialize figure
	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot(1,1,1)

	#Plot
	ax.scatter(m, e, s = 40, alpha = 0.8 )

	#Ticks
	plt.tick_params(labelsize = 16)

	#Labels
	ax.set_xlabel(' $\mathrm{Magnitude\ M}_{%s}$ ' % (str(int(wavelength*10000))), size = 24 )
	ax.set_ylabel(' $\mathrm{Escape\ Fraction\ } f_{\mathrm{esc}}$ ', size = 24)

	#Grid
	ax.grid(True, alpha = 0.3)

	fig.savefig(output)
