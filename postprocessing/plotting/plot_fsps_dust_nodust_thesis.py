import numpy as np
import fsps
import matplotlib.pyplot as plt

lsun = 3.846e23

met = 0.02*0.21
ages = np.array( [0.001, 0.01, 0.1, 1., 10.] )


#Initiliaze temp model to get available metallicities
sp_temp = fsps.StellarPopulation()
met_available = np.array(sp_temp.zlegend)
imet = np.argmin( np.abs(met_available - met) )
print 'User metallicity = %f ; Closest metallicity = %f' % (met, met_available[imet] )

#Now make the model
sp = fsps.StellarPopulation(zmet = imet + 1, add_neb_emission = True)

#Initialize the figure
fig = plt.figure( figsize = (16,9) )
ax = fig.add_subplot(1,1,1)

for i in range( len(ages) ):

	#Get the spectrum
	wave, flux_dens = sp.get_spectrum(zmet = imet + 1, tage = ages[i], peraa = True)
	
	ax.plot( np.log10(wave/10000.0), np.log10(flux_dens*wave * lsun), label = 'Age = 1e%d Myr' % (np.log10(ages[i] * 1000)), linewidth = 2 )

#Legend
ax.legend( loc = 1, fontsize = 20 )

#Limits
ax.set_ylim(20.5,27.5)
ax.set_xlim(np.log10(0.07), np.log10(1100.0))

#Grid
ax.grid(True, alpha = 0.2)

#Labels
ax.set_xlabel('$\mathrm{Wavelength\ log} (\lambda) [\mu\mathrm{m}] $', fontsize = 24)
ax.set_ylabel('$\mathrm{Flux\ log} (\lambda f_\lambda) [\mathrm{erg/s}] $', fontsize = 24 )

#Ticks
ax.tick_params(labelsize = 20)

fig.savefig('/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/19082016/0000/FSPS_demo.pdf', bbox_inches='tight')

