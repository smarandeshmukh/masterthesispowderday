import numpy as np
import os
import sys
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


if __name__=='__main__':
	
	#User input
	try:
		dust_t_file = sys.argv[1]
		out = sys.argv[2]
	except IndexError:
		print 'This will print the distribution of your dust temperatures.'
		print 'Syntax: [script] [dust_temp] [output]'
		raise IndexError('Check Syntax')

	assert os.path.exists(dust_t_file), 'could not find dust file: %s' % dust_t_file

	#Load data
	d0 = np.loadtxt(dust_t_file)

	#Sort it
	d0 = np.sort(d0)

	#Trim all node cells
	d = d0[ d0>0.1 ]

	#Histogram
	n, binedges = np.histogram(d[ d <100], bins = 20)
	bincenters = ( binedges[1:] + binedges[:-1] ) / 2
	
	#Initialise plot
	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot(1,1,1)

	#Plot
	a = np.arange(0, len(d))
	ax.plot(bincenters, n)

	#Define fitting function
	def mb(x, A, sig):
		return A * x**2 * np.exp( - x**2/ (2*sig**2) )

	#Fit to data
	p,cov	 =  curve_fit(mb, bincenters, n, p0 = [220000, 60])
	ax.plot( bincenters, mb(bincenters, p[0], p[1]) )

	print 'fitting:', p, mb(bincenters, p[0], p[1])


	#Print
	print np.median(d0), np.mean(d0)
	print np.median(d), np.mean(d), np.std(d)

	print d.shape, d[d < 100].shape

	print np.median(d[d < 100]), np.mean(d[d < 100]), np.std( d[ d < 100] )

	#Save
	fig.savefig(out)

