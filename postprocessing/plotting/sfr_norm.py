import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy.optimize import curve_fit

def gaussian(x, a, x0, sigma):
	return a*np.exp(-(x-x0)**2/(2*sigma**2))

data = sys.argv[1]
out = sys.argv[2]

#Load y data
d = np.loadtxt(data)[:, 1:]

#Load x data from header
line = open(data).readlines()[2][1:]
xdata = np.fromstring(line, dtype=float, sep=' ')

#Get the bincenters
mid_array = np.array([])
sig_array = np.array([])
binc = xdata
for bins in range( len(binc) )[:]:
	binvalues =  d[:, bins]
	n, bin_edges = np.histogram(binvalues)
	bincenters = ( bin_edges[:-1]  + bin_edges[1:] ) / 2
	popt, pcov = curve_fit(gaussian, bincenters, n)
	#plt.plot(bincenters, n)
	#plt.show()
	mid_array = np.append(mid_array, popt[1])
	sig_array = np.append(sig_array, np.abs(popt[2]))

"""
print d.shape, len(d)
print xdata.shape

print mid_array
print sig_array
"""
a = np.zeros((1,2))

#Put the entire data in one array
for i in range(len(xdata)):
	
	for sfr_year in range(len(d)):
		point = np.array( [xdata[i], d[sfr_year, i]])
		point = point[None, :]
		a = np.append( a, point, axis = 0)
a = np.delete(a, 0, 0)

#Initialize plot
fig = plt.figure( figsize = (16,9) )
ax = fig.add_subplot(1,1,1)

#Plotting

#The data
#ax.scatter( a[:,0], a[:,1], alpha = 0.05, s = 10, color = 'k')

#The gaussian fits
ax.errorbar(xdata, mid_array, yerr = sig_array, linewidth = 3, capsize = 5, capthick = 3, color='b')

#Limits
ax.set_xlim(0.7*np.amin(xdata), 1.1*np.amax(xdata))
#Ticks
plt.tick_params( labelsize = 22 )

#Labels
ax.set_xlabel('Time $t$ [Myr]', size = 26)
ax.set_ylabel('SFR [ SFR($t$) / $\overline{ \mathrm{SFR}}_{200} $ ]', size = 26)

#Grid
ax.grid(True, alpha = 0.2)

fig.savefig(out)
