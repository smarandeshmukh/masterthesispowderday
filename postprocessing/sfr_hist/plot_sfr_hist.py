import numpy as np
import matplotlib.pyplot as plt
import sys


def main():
	
	try:
		inp = sys.argv[1]
		out = sys.argv[2]
	except:
		raise Exception

	d = np.loadtxt(inp)
	print d.shape

	head = (open(inp)).readline()

	ages = np.array( ((head.split('[')[-1]).split(']')[0]).split(), dtype=float )

	haloids = d[:,0]
	data = d[:,1:]

	fig = plt.figure( figsize = (16,9) )
	ax = fig.add_subplot(1,1,1)

	for i in range(6):
		if i==4:
			continue
		ax.plot( ages[1:], data[i] )

	fig.savefig(out)

if __name__=='__main__':
	main()
