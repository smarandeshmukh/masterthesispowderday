import numpy as np
import h5py
import os
import sys
from astropy.cosmology import LambdaCDM, z_at_value
from astropy import units as u
from functools import reduce

def main(inp='', agelimit=None, center=None, R=None,quiet=True):

	#For terminal operation get/check user input 
	if __name__=='__main__':
		print "Running from terminal"
		try:
			inp = sys.argv[1]
			quiet = False
		except:
			print "Wrong input"
			print "Syntax: [script name] [hdf5 file] [extra arguments]"
			print "Extras syntax: [option] [value]"
			print "Options \t agelimit (Myr) \n\t\t center x y z \n\t\t 'R (=Cube-edge-half-length)' R \n\t\t quiet True/False"
			raise Exception("Wrong input")
		if len(sys.argv) > 2:
			print "User has extra arguments."
			args = sys.argv[2:]
			if 'agelimit' in args:
				agelimit = float(args[ args.index('agelimit') + 1 ])
			if 'center' in args:
				center = np.array([0,0,0])
				center[0] = float(args[ args.index('center') + 1 ])
				center[1] = float(args[ args.index('center') + 2 ])
				center[2] = float(args[ args.index('center') + 3 ])
			if 'R' in args:
				R = float(args[ args.index('R') + 1 ])
	
	#Print user inputs
	if not quiet:
		print 'Input file:', inp
		print 'Agelimit (Myr)', agelimit
		print 'Center', center
		print 'Cube edge length = 2 *', R


	#Verify hdf5 file existence
	if not os.path.exists(inp):
		print "Input file does not exist:", inp
		raise Exception("Input file does not exist.")

	#Open hdf5 file
	f = h5py.File(inp, 'r')

	#Access relevant fields from header
	h = f['Header']
	H0 = h.attrs['HubbleParam'] * 100
	Om0 = h.attrs['Omega0']
	Ode0 = h.attrs['OmegaLambda']
	z = h.attrs['Redshift']

	#Zoom in to region if center is specified
	if center==None:
		if not quiet:
			print 'Including all particles from the box'
		ages = f['PartType4']['StellarFormationTime'][:]
	else:
		coords = f['PartType4']['Coordinates'][:]
		coord_ids_x = np.where( np.abs(coords[:,0] - center[0]) <= R)[0]
		coord_ids_y = np.where( np.abs(coords[:,1] - center[1]) <= R)[0]
		coord_ids_z = np.where( np.abs(coords[:,2] - center[2]) <= R)[0]
		coord_ids = reduce(np.intersect1d, (coord_ids_x,coord_ids_y,coord_ids_z))
		if not quiet:
			print 'Number of stars in zoomed in region =',  len(coord_ids)
		ages = f['PartType4']['StellarFormationTime'][:]

	#Create Cosmology
	cosmo = LambdaCDM(H0=H0, Om0=Om0, Ode0=Ode0)

	#If agelimit is set calculate corresponding scale factor
	t0 = cosmo.lookback_time(z)
	if not quiet:
		print "Currently lookback time at redshift", z, "is", t0
	#Convert agelimit to scale factor
	if agelimit:
		t_target = (t0.value + agelimit/1000) * u.Gyr
		z_agelimit = z_at_value(cosmo.lookback_time, t_target)
		a_min = 1/(1+z_agelimit)
		if not quiet:
			print "Agelimit detected."
			print 'Calculating scale factor for a lookback time of:', t_target
			print 'Redshift at agelimit', z_agelimit
			print 'Scalefactor at agelimit = limiting scale factor:', a_min
	#Else include all star particles and set oldest star as limiting age and convert to scale factor
	else:
		if not quiet:
			print 'No agelimit detected.'
			print 'Calculating smallest scale factor = earliest star particle:'
		if center==None:
			if not quiet:
				print 'Calculating a_min inside box.'
			a_min = np.amin(ages)
		else:
			if not quiet:
				print 'Calculating a_min inside zoom-in region.'
			a_min = np.amin(ages[coord_ids])
		
		z_agelimit = 1/a_min - 1
		t_target = cosmo.lookback_time(z_agelimit)
		if not quiet:
			print '\t Limiting scale factor', a_min
			print 'Age at limiting scale factor', t_target

	#Get indexes of all particles for which a > a_min
	ind_ages = np.where(ages >= a_min)[0]
	#Combine these indices with the ones inide zoom in region
	if center != None:
		ind = reduce( np.intersect1d, (ind_ages, coord_ids) )
	else:
		ind = ind_ages
	if not quiet:
		print 'Number of star particles in input file', len(ages)
		print 'Number of star particles in agelimited list and "zoom" limited =', len(ind)

	#Get total mass of selected particles
	mass = f['PartType4']['Masses'][:]
	mass_sel = mass[ind]
	mass_tot = np.sum(mass_sel) * 1e10 / (H0/100) * u.Msun
	if not quiet:
		print 'Total stellar mass formed within agelimits:', mass_tot

	#Get delta_t = t_target - t0
	del_t = t_target - t0
	sfr = mass_tot / del_t.to(u.yr)
	if not quiet:
		print 'Stellar mass formed in =', del_t
		print 'Then the SFR is', sfr

	return sfr.value

	f.close()

if __name__== "__main__":
	main()
