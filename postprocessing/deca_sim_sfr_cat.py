import numpy as np
import sys
import os
import deca_sim_sfr

def main():

#	print get_sim_sfr.main(inp ='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/inputfiles/hugemod.hdf5', agelimit = 10.0, center = np.array([2.981140323680000165e+03, 6.486511637259999588e+03, 3.821594477950000055e+03]), R=2*17.15, quiet=False)
	
	agelimit_array = np.arange(0.0, 210.0, 10.0)

	#Get user input
	try:
		hdf5name = sys.argv[1]
		cat = sys.argv[2]
		out = sys.argv[3]
		quiet = sys.argv[4]
	except:
		print 'Syntax: [script name] [hdf5 file] [catalogue file] [output file] [quiet=True/False]'
		raise Exception('Not enough inputs')

	#Check whether to run in quiet mode
	if quiet=='False':
		quiet = False
	else:
		quiet = True

	#Verify file existence
	if not os.path.exists(hdf5name):
		raise Exception('hdf5 file does not exist')
	if not os.path.exists(cat):
		raise Exception('Catalogue file does not exist')
	if os.path.exists(out):
		if not quiet:
			print 'Removing existing output file'
			os.remove(out)

	#Print user input
	if not quiet:
		print 'Hdf5 file: ', hdf5name
		print 'Catalogue file: ', cat
		print 'Output file: ', out

	#Define columns
	haloid_col = 0
	x_col = 2
	y_col = 3
	z_col = 4
	r_gal_col = 8

	#Load catalogue and data
	d = np.loadtxt(cat)
	haloid = d[:,haloid_col]
	x = d[:,x_col]
	y = d[:,y_col]
	z = d[:,z_col]
	r_gal = d[:,r_gal_col]

	#Open output file and write header
	f = open(out, 'w')
	f.close()
	f = open(out, 'a')
	f.write('#haloid(0) Delta_t (Myr)' + str(agelimit_array) + '\n')

	#Iterate through each halo
	for i in range(len(haloid))[:]:
		line = str(int(haloid[i]))
		if not quiet:
			print 'Processing halo number:', int(haloid[i])
		#Iterate through each delta_t
		for index in range(len(agelimit_array) - 1):
			line = line + ' ' + str(deca_sim_sfr.main(inp = hdf5name, agestart = agelimit_array[index], agefinish = agelimit_array[index+1], center = np.array([ x[i], y[i], z[i] ]), R = r_gal[i], quiet = True))

		f.write(line + '\n')

	f.close()

if __name__ == '__main__':
	main()
