import numpy as np
from hyperion.model import ModelOutput
import sys
import os
from astropy.cosmology import LambdaCDM
import astropy.units as u
from astropy import constants as const
import matplotlib.pyplot as plt


def main():
	
	quiet = False
	
	try:
		inp = sys.argv[1]
		out1 = sys.argv[2]
		out2 = sys.argv[3]
		quiet_bool = sys.argv[4]
	except:
		print 'Not enough arguments!'
		print 'Fomat: [script name] [input] [nu_L_nu name] [L_nu] [True/False -> whether to run in quiet mode]'
		raise Exception('Not enough arguments!')

	#Check whether to run in quiet mode
	if quiet_bool == 'True':
		quiet = True

	#Set default parameters
	funits = 'ergs/s'
	redshift = 0
	distance = None
		
	#Print user inputs
	if not quiet:
		print 'Input file:', inp
		print 'nu_L_nu file name:', out1
		print 'L_nu file name:', out2

	#Access input file
	m = ModelOutput(inp)
	sed = m.get_sed(distance = distance, inclination ='all', aperture = -1, units = funits)
	#Get wavelengths, frequencies and fluxes
	wav = sed.wav*(1 + redshift)
	nu = sed.nu/(1 + redshift)
	flux = sed.val

	#Initialize plot
	fig_nu_L = plt.figure(dpi = 1200, figsize = (16,9))
	fig_L = plt.figure(dpi = 1200, figsize = (16,9))

	ax_nu_L = fig_nu_L.add_subplot(1,1,1)
	ax_L = fig_L.add_subplot(1,1,1)

	#Access different inclinations
	for inclination in range(flux.shape[0])[:]:
		#Plot in function of wavelength
		nu_L_nu = flux[inclination,:]

		ax_nu_L.set_xlim(np.amin(wav), np.amax(wav))
		ax_nu_L.set_ylim(10e39, np.amax(flux) )
		ax_nu_L.set_xlabel('wavelength $\lambda$ ($\mu$m)', size = 24)
		ax_nu_L.set_ylabel('flux $\\lambda L_\\lambda$ ' + '(' + str('erg/s') + ')', size = 24)
		ax_nu_L.loglog(wav, nu_L_nu, marker = "None" )
		ax_nu_L.tick_params(labelsize = 20)

		L_nu = nu_L_nu/wav

		ax_L.set_xlim(np.amin(wav), np.amax(wav))
		#ax_L.set_ylim(10e39, np.amax(flux) )
		ax_L.set_xlabel('wavelength $\lambda$ (microns)', size = 24)
		ax_L.set_ylabel('flux $ L_\\lambda$ ' + '(' + str('erg/s') + '/$\mu$' + ')', size = 24)
		ax_L.loglog(wav, L_nu, marker = "None" )
		ax_L.tick_params(labelsize = 20)


#	ax_nu_L.legend(loc=2)
	ax_nu_L.set_title('Galaxy SED in rest frame', size=24)
#	ax_L.legend(loc=2)
	ax_L.set_title('Galaxy SED in rest frame', size=24)

#	plt.show()
	fig_nu_L.savefig(out1)
	fig_L.savefig(out2)



if __name__=="__main__":
	main()
