#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/get_flux.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/30052016'
output='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/30052016/flux_cat_1700.txt'
wavelength=0.17

file_list=($inp_root/*/*rtout.sed)

echo 'Number of files to process:' ${#file_list[@]}

rm -f $output

echo '#haloid(0) wavelength(1) M_AB_at_inclination (2......)'>>$output
echo '#Computed for closest wavelength to' $wavelength 'microns'>>$output

for file in ${file_list[@]}
do
	python $exe $file $wavelength $output quiet
done

