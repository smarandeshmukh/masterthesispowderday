#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/get_lum_cat.py'
inp='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/30052016'
out='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/30052016/lum.txt'

#Remove any old file under that name
rm -f $out

file_list=($inp/*/*rtout.sed)

echo ${file_list[@]}

echo '#haloid(0) Num_inclinations(1) Lum_tot for inclinations(2...)'>>$out

for file in ${file_list[@]}
do
#	echo $file
	python $exe $file $out
	done
