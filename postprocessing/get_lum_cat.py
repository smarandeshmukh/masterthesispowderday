import numpy as np
from hyperion.model import ModelOutput
import sys
import os

def main():
	
	quiet = False
	inclination = 3

	#Get input
	try:
		inp = sys.argv[1]
		out = sys.argv[2]
	except:
		print 'Wrong Syntax'
		print 'Correct Syntax: [script name] [input file] [output file]'
		raise Exception('Wrong Syntax')
	#Verify file existence
	if not os.path.exists(inp):
		raise Exception("Input file does not exist")

	#Print user input
	if not quiet:
		print "Input file:", inp
		print "Output file:", out

	#Access sed file
	m = ModelOutput(inp)
	sed = m.get_sed(inclination='all', aperture=-1, distance=None)
	wav = sed.wav
	nu_l_nu = sed.val
	freq = sed.nu
	flux = nu_l_nu / freq
	num_inclinations = len(flux)

	#Calculate total lum with trapezoid rule
	lum = np.trapz(flux,freq)
	
	#Write string with haloid and l_tot
	haloid = inp.rsplit('/')[-2]
	lum_string = ''.join(str(e) + ' ' for e in lum)
	line = str(haloid) + ' ' + str(num_inclinations) + ' ' + str(lum_string) + '\n'

	#Append to file
	f = open(out, 'a')
	f.write(line)
	f.close()

	



if __name__=='__main__':
	main()
