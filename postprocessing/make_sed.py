import numpy as np
import sys
import os
from hyperion.model import ModelOutput

def main():
	
	#Verify user inputs
	try:
		inp = sys.argv[1]
		output = sys.argv[2]
		redshift = sys.argv[3]
	except IndexError:
		print "Not enough inputs!"
		print "Syntax: [script name] [input name] [output name] [redshift]"
		print "To use default redshift set redshift to -1"
		sys.exit()

	try:
		redshift = float(redshift)
	except ValueError:
		raise ValueError("Could not convert your redshift to float!")

	if not os.path.exists(inp):
		raise Exception("Your input file does not exist")
	
	#Print user inputs
	print "Your input file:", inp
	print "Your output:", output
	print "Your redshift:", redshift


	#Access the model
	m = ModelOutput(inp)
	sed = m.get_sed(inclination='all', aperture=-1)
	wav = sed.wav
	flux = sed.val
	print len(wav), len(flux), flux.shape, sed.units

	#Define limits of the plot
	wav_min = np.amin(wav)
	wav_max = np.amax(wav)
	flux_min = np.amin(flux)
	flux_max = np.amax(flux)
	print wav_min,wav_max,flux_min,flux_max

	print "hi again"


if __name__=="__main__":
	main()
