import numpy as np
import sys
import os
from get_sfr2 import get_sfr as sfr
from hyperion.model import ModelOutput


def main():
	
	quiet = True

	try:
		inp = sys.argv[1]
		out = sys.argv[2]
	except:
		print 'Input error'
		raise Exception('Input Error')

	if len(sys.argv) > 3:
		#Parse for additional arguments
		extras = sys.argv[3:]
		if 'quiet' in extras:
			quiet = True

	#Print user input
	print 'Input folder:', inp

	#Check if input path is good
	if not os.path.exists(inp):
		raise Exception('Input folder does not exist')

	#Parse output master name
	master_out = os.path.dirname(out) + '/' + (out.rsplit('/')[-1]).rsplit('.')[0]
	
	#Check if output exists. Create if not.
	if not os.path.exists(master_out + 'TIR.txt'):
		if not quiet:
			print "Output does not exist. Creating output file."
		o1 = open(master_out + 'TIR.txt', 'w')
		o2 = open(master_out + '24mu.txt', 'w')
		o3 = open(master_out + '70mu.txt', 'w')
		o4 = open(master_out + 'FUV_25mu.txt', 'w')
		o5 = open(master_out + 'FUV_TIR.txt', 'w')
		o1.close()
		o2.close()
		o3.close()
		o4.close()
		o5.close()
	else:
		if not quiet:
			print 'Output exists. Will append to file.'

	#Parse for halo id. Will get this from name of folder
	haloid = int(inp.rsplit('/')[-2])

	#Start writing the line
	line = str(haloid)

	#Open the model
	m = ModelOutput(inp)
	sed = m.get_sed(distance = None, inclination='all', aperture=-1)
	wav = sed.wav
	nu = sed.nu
	flux = sed.val

	#Calculate the sfr for each inclination
	num_i = flux.shape[0]

	#Write number of inclinations
	line = line + ' ' + str(num_i)

	sfr_string_TIR = ' '
	sfr_string_24mu = ' '
	sfr_string_70mu = ' '
	sfr_string_FUV_25mu = ' '
	sfr_string_FUV_TIR = ' '

	for i in range(num_i):
		line = line + ' ' + str(i)
		flux_i = flux[i,:]
		sfr_TIR = sfr(wav, nu, flux_i, None, 'TIR', d_method = None)
		sfr_24mu = sfr(wav, nu, flux_i, None, '24mu', d_method = None)
		sfr_70mu = sfr(wav, nu, flux_i, None, '70mu', d_method = None)
		sfr_FUV_25mu = sfr(wav, nu, flux_i, None, 'FUV', d_method = '25mu')
		sfr_FUV_TIR = sfr(wav, nu, flux_i, None, 'FUV', d_method = 'TIR')

		sfr_string_TIR = sfr_string_TIR + str(sfr_TIR) + ' '
		sfr_string_24mu = sfr_string_24mu + str(sfr_24mu) + ' '
		sfr_string_70mu = sfr_string_70mu + str(sfr_70mu) + ' '
		sfr_string_FUV_25mu = sfr_string_FUV_25mu + str(sfr_FUV_25mu) + ' '
		sfr_string_FUV_TIR = sfr_string_FUV_TIR + str(sfr_FUV_TIR) + ' '
	
	o1 = open(master_out + 'TIR.txt', 'a')
	o2 = open(master_out + '24mu.txt', 'a')
	o3 = open(master_out + '70mu.txt', 'a')
	o4 = open(master_out + 'FUV_25mu.txt', 'a')
	o5 = open(master_out + 'FUV_TIR.txt', 'a')

	o1.write(str(haloid) + ' ' + str(sfr_string_TIR)  + '\n')
	o2.write(str(haloid) + ' ' + str(sfr_string_24mu)  + '\n')
	o3.write(str(haloid) + ' ' + str(sfr_string_70mu) + '\n')
	o4.write(str(haloid) + ' ' + str(sfr_string_FUV_25mu)  + '\n')
	o5.write(str(haloid) + ' ' + str(sfr_string_FUV_TIR)  + '\n')


	o1.close()
	o2.close()
	o3.close()
	o4.close()
	o5.close()

if __name__=='__main__':
	main()
