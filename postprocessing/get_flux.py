import numpy as np
import cosmolopy
import sys
from hyperion.model import ModelOutput

def main():

	quiet = False

	try:
		inp = sys.argv[1]
		wavelength = float(sys.argv[2])
		out = sys.argv[3]
	except:
		print 'Wrong Syntax'
		print 'Correct syntax: [script name] [input sed file from hyperion] [rest wavelength (microns) for mag calc using AB system] [output file] [extra arguments: "quiet"]'
		raise Exception('Wrong inputs syntax')

	#Parse for more input options
	if len(sys.argv) > 4:
		if 'quiet' in sys.argv[4:]:
			quiet = True

	#Print input
	if not quiet:
		print 'input file:', inp
		print 'Rest wavelength (micron):', wavelength
		print 'output:', out
			
	#Access sed file
	m = ModelOutput(inp)
	sed = m.get_sed(distance = None, inclination = 'all', aperture = -1, units='ergs/s')
	w = sed.wav
	nu = sed.nu
	flux = sed.val
	new_flux = np.copy(flux)

	#Get haloid from sed file name and append to line to write
	haloid = ((inp.rsplit('/')[-1]).split('.')[0])[-4:]
	line = str(haloid) + ' '

	#Get closest wavelength to user wavelength
	iwav = np.argmin(np.abs(w - wavelength))
	if not quiet:
		print 'Calculating Magnitudes at lambda (microns) = ', w[iwav]

	line += str(w[iwav]) + ' '

	#Get flux at that wavelength
	if not quiet:
		print flux[:,iwav] #This is in units of erg/s

	#Change flux to erg/s/ang to conform to cosmolopy module
	for i in range(len(flux)):
		new_flux[i] = new_flux[i] / (w*10000)

	#Change wavelength to angstrom to conform with cosmolopy module
	wavelength = wavelength*10000

	for i in range(len(flux)):
		if not quiet:
			print i, flux[i,iwav], 'erg/s'
		line += str(cosmolopy.magnitudes.magnitude_AB(None, new_flux[i, iwav], wavelength)[1]) + ' '
	
	if not quiet:
		print "haloid and magnitudes at different inclinations:\n", line

	o = open(out, 'a')
	o.write(line + '\n')
	o.close()


if __name__=='__main__':
	main()
