import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib import cm

def main():

	print 'hi'

	try:
		halo_cat = sys.argv[1]
		mag_cat = sys.argv[2]
		out = sys.argv[3]
	except:
		print 'Syntax: [script name] [halo catalog] [mag catalog] [output file]'
		raise Exception('Syntax error')

	#Access data
	halos = np.loadtxt(halo_cat)
	magnitudes = np.loadtxt(mag_cat)
	#haloids from both datasets
	haloids_all = np.array(halos[:,0], dtype=int)
	haloids_mags = np.array(magnitudes[:,0], dtype=int)
	#hosthalos
	sub = halos[:,1]
	#halomass. Units are Msun/h
	h_mass = halos[:,3]
	#mags
	m = magnitudes[:,5]

	#Get halo masses indexed by the mag catalog
	h_halo_mass_gal = h_mass[haloids_mags]
	#Get host halo indexes 
	sub_gal = sub[haloids_mags]
	#Divide in to parents and children
	h_parent_mass = h_halo_mass_gal[ np.where(sub_gal == -1.)[0] ]
	h_child_mass = h_halo_mass_gal[ np.where(sub_gal != -1.)[0] ]
	h_parent_mag = m[ np.where(sub_gal == -1.)[0] ]
	h_child_mag = m[ np.where(sub_gal != -1.)[0] ]


	#Plotting
	fig = plt.figure( figsize=(16,9) )
	ax = fig.add_subplot(1,1,1)

	#Scatter plot
	ax.scatter(h_parent_mag, np.log10(h_parent_mass), c='b', label='Parent halos')
	ax.scatter(h_child_mag, np.log10(h_child_mass), facecolor='None', edgecolor='r', label='Sub halos')
	
	#Labels
	ax.set_ylabel('Halo Mass $\mathrm{log} (M_h) [\mathrm{M_{\odot}}h^{-1}] $', size = 24)
	ax.set_xlabel('UV Luminosity $\mathrm{M_{1500}}$', size = 24)
	#Legend
	ax.legend(loc=1)


	
	fig.savefig(out)




if __name__=='__main__':
	main()
