import numpy as np
import sys
import os
from hyperion.model import ModelOutput
import h5py
from astropy.cosmology import LambdaCDM
from astropy import units
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.integrate import simps
import cosmolopy
from astropy import constants as const

def main():
	try:
		sed_file = sys.argv[1]
		filt_trans_file = sys.argv[2]
		hdf5file = sys.argv[3]
		inclination = int(sys.argv[4])
		out = sys.argv[5]
		q = sys.argv[6]
	except:
		print 'Syntax:'
		print '[script name] [sed file] [filter file] [hdf5 file] [inclination id]  [output] [quiet=True/False]'
		raise Exception('Syntax Error')
	
	#Check whether to run in quiet mode or not
	if q == 'True':
		quiet = True
	else:
		quiet = False


	#Print user input
	if not quiet:
		print 'sed file:', sed_file
		print 'filter file:', filt_trans_file
		print 'hdf5file:', hdf5file
		print 'inclination id:', inclination
		print 'output file:', out

	#Extract cosmology from hdf5 file to get luminosity distance
	f = h5py.File(hdf5file)
	z = f['Header'].attrs['Redshift']
	H0 = f['Header'].attrs['HubbleParam']
	Om0 = f['Header'].attrs['Omega0']
	Ode0 = f['Header'].attrs['OmegaLambda']
	cosmo = LambdaCDM(H0=H0, Om0=Om0, Ode0=Ode0)
	d = ( cosmo.luminosity_distance(z) ).to(units.cm).value
	f.close()

	#Constant
	c = (const.c.to('micron/s')).value
	
	#Access sed
	model = ModelOutput(sed_file)
	sed = model.get_sed(distance = d, inclination='all', aperture = -1, units='Jy')
	w = sed.wav
	nu = sed.nu
	flux = sed.val

	#Plot sed
	fig = plt.figure( figsize=(16,9) )
	ax = fig.add_subplot(1,1,1)
	ax.set_xscale("log")
	ax.set_yscale("log")
	ax.set_ylabel(sed.units)
	ax.set_xlabel('$\mu$m')
	ax.plot(w, flux[inclination], '+-')
	fig.savefig('delete.png')

	#Parse haloid
	haloid_str = sed_file.split('/')[-2]

	#Get mono wavelength
	mono = 0.15

	#interpolate sed
	sed_intp = interp1d(w, flux[inclination], kind='linear', fill_value = 0, bounds_error=False)

	#Get magnitude
	m_app_mono = -2.5*np,log10(sed_intp(mono) / 3631)

	print sed_intp(mono)


	"""
	#Plotting routine for filter curves
	b = sed_intp(a) / a
	#plt.loglog(w, flux[inclination,:], '+')
	plt.loglog(a, filt_t)
	plt.loglog(3*10**14/a, filt_t)
	plt.show()
	"""

if __name__=='__main__':
	main()
