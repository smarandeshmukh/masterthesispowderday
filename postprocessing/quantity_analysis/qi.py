import numpy as np
import os
import sys
from hyperion.model import ModelOutput
import h5py


#User input
try:
	modelname = sys.argv[1]
	out = sys.argv[2]
except IndexError:
	print 'This script will print your sorted Q values to your output file for each iteration (for temperatures).'
	print 'Syntax: [script name] [sed file] [output]'
	raise IndexError('Check Syntax')

#Access a model
m = ModelOutput(modelname)

#Current directory
cwd = os.getcwd()

#initialize final array
elist = []

for i in range(30):
	
	print 'In iteration:', i
	#Extract the stuff if possible
	try:
		octree = m.get_quantities(iteration = i)
	except:
		break

	hdf5name = os.path.join(cwd, 'temp%s.hdf5' % (str(i)) )


	if os.path.exists(hdf5name):
		os.remove(hdf5name)

	#Initialize temporary hdf5 file
	f = h5py.File(hdf5name, 'w')

	#Write to file
	octree.write(f)

	#Access the group
	quan = f['Quantities']
	e = quan['temperature'][:]

	#Reshape the dust array just in case
	if e.ndim == 1:
		e = e[None, :]

	#Get the energies of the last dust type
	d0 = e[-1]

	#Save the array
	elist.append(d0)

	#Delete temporary hdf5 files
	f.close()
	os.remove(hdf5name)

print 'Number of iterations:', i
print 'Intial shape of energies:', d0.shape
#Transform to numpy
e =  np.asarray(elist)

#Calculate the Rs
rlist = []
for i in range(1, len(e)):
	rtemp = np.maximum( e[i] / e[i-1], e[i-1] / e[i] )
	rlist.append(rtemp)

r = np.asarray( rlist )

s = r

print 'Total number of cells:', len(d0)
print 'Number of cells where we were unable to calculate the Q (iteration 0): (probably due to node cells):',  len( np.where( np.isnan(r[0]) )[0] )
print 'Number of cells where Q was calculated (iteration 0):', len( np.where( np.isfinite(r[0]) )[0] )

for i in range( len(r) ):
	s[i][ np.where( np.invert(np.isfinite(r[i])) )[0] ] = 0
	s[i] = np.sort(s[i])
	s[i][ np.where( s[i] == 0 )[0] ] = 1

print 'Completed processing sed file:', modelname
print 'Saving to:', out
np.savetxt(out, s)

