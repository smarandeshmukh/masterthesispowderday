import numpy as np
import os
import sys
from hyperion.model import ModelOutput
import h5py


#User input
try:
	modelname = sys.argv[1]
	out = sys.argv[2]
except IndexError:
	print 'This script calculate the dust temperature in all cells'
	print 'Syntax: [script name] [sed file] [output]'
	raise IndexError('Check Syntax')

#Access a model
m = ModelOutput(modelname)

#Current directory
cwd = os.getcwd()

#initialize final array
elist = []

#Extract the stuff
octree = m.get_quantities()

hdf5name = os.path.join(cwd, 'temp%s.hdf5' % (str('last')) )


if os.path.exists(hdf5name):
	os.remove(hdf5name)

#Initialize temporary hdf5 file
f = h5py.File(hdf5name, 'w')

#Write to file
octree.write(f)

#Access the group
quan = f['Quantities']
e = quan['temperature'][:]

#Reshape the dust array just in case
if e.ndim == 1:
	e = e[None, :]

#Get the temperatures of the last dust type
d0 = e[-1]

#Delete temporary hdf5 files
f.close()
os.remove(hdf5name)

print 'Intial shape of energies:', d0.shape
print 'Completed processing sed file:', modelname
print 'Saving to:', out
np.savetxt(out, d0)

