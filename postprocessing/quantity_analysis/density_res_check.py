import h5py
import os
import sys
from hyperion.model import ModelOutput
import numpy as np
import matplotlib.pyplot as plt


def write_to_file(m, name):
	
	octree = m.get_quantities()

	#If object is not an octree issue warning
	if type(octree).__name__ != 'OctreeGrid':
		print 'WARNING!: The model is not an octree!!!!!'

	#Create the hdf5 file
	f = h5py.File(name, 'w')
	octree.write(f)
	f.close()

def conv_ref_structure(f):
	
	ref_array = np.asarray( f['Geometry']['cells'][:10], dtype=int )
	print ref_array
	answer = np.array([])
	for i in range( len(ref_array) ):
		print type( ref_array[i] ), ref_array[i]
		if ref_array[i] == 1:
			answer = np.append(answer, -1)
		else:
			answer = np.append(answer, 11)
	

	return answer

if __name__=='__main__':
	
	#User input
	try:
		inp = sys.argv[1]
		out = sys.argv[2]
	except IndexError:
		print 'This conducts a density diagnostic for a given file.'
		print 'Syntax: [script name] [input SED file] [output]'
		raise IndexError('Check Syntax')

	assert os.path.exists(inp), 'Could not find the sed file'

	#Access the model
	try:
		m = ModelOutput(inp)
	except:
		raise Exception('Could not access the model file.')

	#Write quantites to file. Clear the temporary hdf5file if it exists
	hdf5name = 'temp.hdf5'
	if os.path.exists(hdf5name):
		os.remove(hdf5name)
	write_to_file(m, hdf5name)

	#Access the file
	f = h5py.File(hdf5name, 'r')
	
	#Get the refinement structure so that we can read the dust masses correctly
	#vol_arr = conv_ref_structure(f)

	#Get the density array of dust type 3
	dens = f['Quantities']['density'][2, :]
	
	#Sort it 
	dens = np.sort(dens)
	
	#Remove zeros
	dens = np.trim_zeros(dens)[:10000]

	n, binedges = np.histogram(dens, bins = 50)
	bincenters = (binedges[1:] + binedges[:-1]) / 2

	plt.plot(bincenters, n)
	plt.show()
