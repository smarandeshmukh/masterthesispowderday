import numpy as np
import matplotlib.pyplot as plt
import sys

def main():
	
	try:
		sed_sfr = sys.argv[1]
		sim_sfr = sys.argv[2]
		mass = sys.argv[3]
		out = sys.argv[4]
	except:
		print 'Wrong Syntax'
		print 'Correct Syntax: [script name] [sed inferred sfr_basename] [sim_sfr] [masses] [output]'
		raise Exception('Wrong Syntax')

	sfr_types = ['TIR','24mu','70mu','FUV_25mu','FUV_TIR']
	rel_del_t = [100, 100, 100, 10, 10]
	col_num_sim_sfr = [3,3,3,0,0]

	#Load into numpy arrays
	r = np.loadtxt(sim_sfr)
	m = np.loadtxt(mass)
	sed_sfr_dict = {}
	for i in range(len(sfr_types)):
		sed_sfr_dict[sfr_types[i]] = np.loadtxt(sed_sfr + sfr_types[i] + '.txt')
	
	#Load sim sfr and mass
	sim_sfr = r[:,1:]
	masses = m[:,2]

	#Column to plot 1=face 2=45deg 3=edge 4,5,6 sim axis
	plot_col = 1

	#sim sfr timescale 0=10 1=40 2=70 3=100
	

	for i in range(len(sfr_types)):
		fig1 = plt.figure(figsize = (16,9))
		ax1 = fig1.add_subplot(1,1,1)
		ax1.loglog(masses, sim_sfr[:,col_num_sim_sfr[i]], 'ko', label = 'Simulation SFR for timescale $\Delta$t(Myr) = ' + str(rel_del_t[i]))
		ax1.loglog(masses, (sed_sfr_dict[sfr_types[i]])[:,plot_col], 'ro', label = str(sfr_types[i]) + ' SFR  estimate from SED')
		ax1.set_xlabel('Galaxy Stellar Mass (M/M$_\odot$)', size = 24)
		ax1.set_ylabel('SFR (M$_\odot$/yr)', size = 24)
		ax1.set_xlim([1e8, 2e10])
		ax1.legend(loc=2)
		ax1.grid(True)
		fig1.savefig(out + '_' + sfr_types[i] + str(rel_del_t[i]) + '.pdf')

if __name__=='__main__':
	main()
