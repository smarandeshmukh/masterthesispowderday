import numpy as np
import sys
import os
import matplotlib.pyplot as plt


def main():
	
	def Tomczak2014(x):

		
		M_0 = 11.35
		alpha = -1.74
		phi_0 = 10**(-4.36)

		return np.log(10)*phi_0*10**( (x - M_0)*(1+alpha) )*np.exp(-10**(x-M_0) )

	def Fontana2007(x):

		M_0 = 11.16
		M_1 = 0.17
		M_2 = -0.07
		alpha_0 = -1.18
		alpha_1 = -0.082
		phi_0 = 0.0035
		phi_1 = -2.20
		z = 3.11

		phi_z = phi_0 * (1 + z)**phi_1
		alpha_z = alpha_0 + alpha_1*z
		M_z = M_0 + M_1*z + M_2*(z**2)
		
		
		return phi_z * np.log(10) * ( 10.0**(x-M_z) )**(1 + alpha_z) * np.exp( -10.0**(x-M_z) )

	def errTomczak(x):

		M_0 = 11.35
		dM_0 = 0.33
		alpha = -1.74
		dalpha = 0.12
		phi_0 = 10**(-4.36)
		dphi_0 = phi_0 * 0.29

		tom_val_at_x = Tomczak2014(x)

		return tom_val_at_x * np.sqrt(   (dphi_0 / phi_0)**2 + (dalpha**2)*( np.log(10)*(x-M_0) )**2 + (dM_0**2)*( ( np.log(10)*(1+alpha) )**2 + ( np.log(10)*np.exp( (x-M_0)*np.log(10)) )**2 )  )
	
	try:
		inp_cat = sys.argv[1]
		out = sys.argv[2]
	except:
		raise Exception

	#Print user inputs here
	if not os.path.exists(inp_cat):
		raise Exception('Catalog does not exist')

	d = np.loadtxt(inp_cat)

	h = 0.678

	print d.shape
	haloids = d[:,0]
	masses_lin = d[:,1]
	#with h
	#masses = np.log10(masses_lin * h**2)
	#without h
	masses = np.log10(masses_lin)

#	print masses
	#Plot histogram and take only non-zero values
	hist2,bins=np.histogram(masses, bins=10)
	hist1 = hist2[np.where(hist2 > 0)[0]]
	centers = (bins[:-1] + bins[1:]) / 2
	centers = centers[np.where(hist2 > 0)[0]]
	binwidth = bins[1] - bins[0]

#	print masses, centers, binwidth

	#Normalization factor
		#with h
	#vol = (12.0)**3
		#without h
	vol = (12.0/h)**3
	norm = vol*binwidth
	hist = hist1 / norm

	#Error bars
	y_errors_lin = np.sqrt(hist1) / norm
	y_errors_log = y_errors_lin / hist

	#Theory
	a = np.arange(7,12,0.1)
	b = Fontana2007(a)
		#Mortlock et al.
	a2 = np.array([9.6,9.9,10.1,10.4,10.6,10.9,11.1,11.4,11.6,11.9,12.1])
	b2 = np.array([-2.45,-2.56,-2.62,-2.88,-2.98,-3.14,-3.96,-3.78,-3.96,-3.96,-3.96])
	e2 = np.array([0.12,0.13,0.13,0.14,0.14,0.16,0.20,0.17,0.19,0.19,0.22])
		#Tomczak
	Tom = np.loadtxt('Tomczak2014.csv')
	tom_x = Tom[:,0]
	tom_y = Tom[:,1]	

	#Initialise Figure
	fig = plt.figure(figsize = (16,9) )
	ax = fig.add_subplot(1,1,1)
	

	ax.set_xscale("log")
#	ax.set_yscale("log")

	#with h
	#ax.set_xlim( (10**7, 10**11) )
	#without h
	ax.set_xlim( (10**8, 10**11) )
#	ax.set_ylim( (-6,-1) )

	#with h labels
	#ax.set_xlabel('Galaxy Stellar Mass M$h_{70}^2$/M$_{\odot}$', size=24)
	#ax.set_ylabel('log $\phi$ [ $\mathrm{h}^3_{70}$  $\mathrm{Mpc}^{-3}$ / log(M/M$_\odot$) ]', size=24)

	#without h labels
	ax.set_xlabel('Galaxy Stellar Mass M/M$_{\odot}$', size=24)
	ax.set_ylabel('log $\phi$ [  $\mathrm{Mpc}^{-3}$ / log(M/M$_\odot$) ]', size=24)

	#h already taken care of in data
	ax.errorbar(10**centers, np.log10(hist), yerr = (0.2+np.log10(1+y_errors_log), -np.log10(1 - y_errors_log)+0.2), label='STD_DYN_3', markersize = 8, marker = 'D')

	print 'vals:', 10**centers, np.log10(hist), y_errors_log
	
	#Plotting Fontana and Mortlock with h
	#ax.plot( (10**a)*0.7**2, b, label='Fontana et al. 2007')
	#ax.errorbar(10.0**a2, 10.0**b2, yerr=np.abs( e2*(10**b2) ), label='Mortlock et al. 2011'  )

	#Plot Tomczak without h
	tom_err_up = [0,0,0,0.18,0,0.13,0.,0.15,0.28,0.32]
	tom_err_down = [0,0,0,0.1,0.13,0.16,0.14,0.13,1.18,0.89]
	ax.errorbar( 10**tom_x, tom_y, yerr=(tom_err_down, tom_err_up), marker='o', label='Tomczak et al. 2014', markersize=8, linewidth=2)

	#Plot Tomczak fit
	a4 = np.arange(7.0,12.0, 0.1)
	b4 = Tomczak2014(a4)
	#The errorbar cheat array for Tomczak
	a5 = np.arange(np.amin(tom_x), np.amax(tom_x), 0.01)
	b5 = Tomczak2014(a5)
	#The errorbar cheat for the extrapolation
	a6 = np.arange(np.amin(a4), np.amin(tom_x), 0.01)
	b6 = Tomczak2014(a6)

	print a4, b4,  '\n\t', errTomczak(a4)
	errtom = errTomczak(a5)
	errtom_cheat = errTomczak(a6)
	ax.plot(10**a4,np.log10(b4), 'r-', label ='Tomczak best fit' )
#	ax.plot(10**a4, np.log10(b4 + errtom), 'g-.' )
#	ax.plot(10**a4, np.log10(b4 - errtom), 'g-.' )
	
	#Fill Tomczak data points with cheat
	ax.fill_between(10**a5, np.log10(b5), np.log10(b5 + errtom), facecolor='green', alpha=0.4, linewidth=0)
	ax.fill_between(10**a5, np.log10(b5), np.log10(b5) - np.log10( b5[0] / (b5[0]-errtom[0]) ) , facecolor='green', alpha=0.4, linewidth=0)

	#Fill the extrapolation
	ax.fill_between(10**a6, np.log10(b6), np.log10(b6 + errtom_cheat), facecolor='green', alpha=0.4, linewidth=0)
	ax.fill_between(10**a6, np.log10(b6), np.log10(b6) - np.log10( b5[0] / (b5[0]-errtom[0])  ), facecolor='green', alpha=0.4, linewidth=0)

	#tick size
	ax.tick_params(labelsize = 20 )

	#Plot Eagles data
#	eagles_1504 = np.loadtxt('Eagles_2.csv')
#	eagles_752 = np.loadtxt('Eagles_1.csv')
#	ax.plot(10**eagles_1504[:,0],  eagles_1504[:,1], 'k', linestyle = 'dotted', label='Eagles model L100N1504, M. Furlong et al. 2015')
#	ax.plot(10**eagles_752[:,0], eagles_752[:,1],  'k--', label ='Eagles model L025N752, M. Furlong et al. 2015' )



	ax.legend(loc=3)
	
	ax.grid(True, alpha=0.2)

	fig.savefig(out)

	print centers, hist2

	#Log error bars
#	y_errors = np.sqrt(hist1) / norm

	"""
	#Theory
	a = np.arange(7, 12, 0.1)
	b = Fontana2007(a)
	#Plot Mortlock
	a2 = np.array([9.6,9.9,10.1,10.4,10.6,10.9,11.1,11.4,11.6,11.9,12.1])
	b2 = np.array([-2.45,-2.56,-2.62,-2.88,-2.98,-3.14,-3.96,-3.78,-3.96,-3.96,-3.96])
	e2 = np.array([0.12,0.13,0.13,0.14,0.14,0.16,0.20,0.17,0.19,0.19,0.22])
	print a2.shape, b2.shape, e2.shape

#	print a,b
	print len(np.where(masses > 5e8)[0])

	#Intialize figure
	fig = plt.figure(figsize = (16,9) )
	ax = fig.add_subplot(1,1,1)

	ax.errorbar(np.log10(centers * 0.7**2), np.log10(hist), yerr = np.abs(y_err_log), label='z = 3.10')
	ax.plot(a + np.log10(0.7**2),np.log10(b), label='Fontana et al. 2006')
	ax.errorbar(a2, b2, yerr = e2, label='Mortlock et al. 2011')
	ax.set_xlabel('Galaxy Stellar Mass log(M$\cdot$ h$^2$/M$_\odot$)', size = 24)
	ax.set_ylabel('Number count log(h$^3$/ (Mpc$^3$*log(M/M$_*$)) ) ', size = 24)
	ax.set_xlim( (7,12) )
	ax.grid(True)
	ax.legend(loc=3)
#	ax.set_ylim( (-6,-1)  )

#	print np.log10(centers)
	print hist2, bins, y_err_log
	print np.log10(binwidth)
	print np.where(hist1 > 0)[0]
	
	fig.savefig(out)
	"""

if __name__=='__main__':
	main()
