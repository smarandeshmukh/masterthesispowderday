import numpy as np
import os
import sys
import matplotlib.pyplot as plt





if __name__=='__main__':

	#User input
	try:
		obs_file = sys.argv[1]
		prod_file = sys.argv[2]
		output = sys.argv[3]
	except IndexError:
		print 'This computes the escape fraction.'
		print 'You already need to have computed the produced photons as well as the observed photons.'
		print 'Syntax: [script name] [observed photons file] [produced photons file] [output] [optional...]'
		print '[optional] = integer specifying the inclination index'
		raise IndexError('Check Syntax')
	#Optional arguments
	try:
		inclination = int(sys.argv[4])
	except:
		inclination = 0

	assert inclination >= 0, 'Inclination has to non-negative'

	o = np.loadtxt(obs_file)[:, 1 + inclination]
	p = np.loadtxt(prod_file)[: ,1]

	assert len(o) == len(p), 'The observed array and the produced array have different shapes.'

	#Get the haloids
	haloids = np.loadtxt(obs_file)[:,0]

	#Open file
	f = open(output, 'w')
	f.write('#Escape fractions for inclination %s. \n#Haloid(0) f_esc(1)\n' % (str(inclination)) )

	#Print to file
	for i in range( len(o) ):
		f.write( str( '%04d' % haloids[i] ) + ' ' + str( o[i]/ p[i] ) + '\n')

