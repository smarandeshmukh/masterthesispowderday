#!/bin/bash

echo 'hi'

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/esc_frac/calc_ion_photons_production.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/04072016'
output='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/04072016/ion_photons_prod_100.txt'

start=$[0]
target=$[126]

full_list=($inp_root/*)
sub_list=${full_list[@]:$start:$target}

rm -f $output

for i in $sub_list
do 
	filename=$i/*rtin.sed
	echo $filename
	python $exe $filename $output
done
