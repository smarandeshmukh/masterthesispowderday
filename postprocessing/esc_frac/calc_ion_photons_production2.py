import sys
import os
import numpy as np
from hyperion.model import ModelOutput
import matplotlib.pyplot as plt


m_name = sys.argv[1]

print m_name

m = ModelOutput(m_name)

sed1 = m.get_sed(inclination = 0, aperture = -1)
sed2 = m.get_sed(inclination = 0, aperture = -1, component = 'source_emit')
sed3 = m.get_sed(inclination = 0, aperture = -1, component = 'dust_emit')
sed4 = m.get_sed(inclination = 0, aperture = -1, component = 'source_scat')
sed5 = m.get_sed(inclination = 0, aperture = -1, component = 'dust_scat')


w = sed1.wav
f1 = sed1.val
f2 = sed2.val
f3 = sed3.val
f4 = sed4.val
f5 = sed5.val

#plt.loglog(w, f1)
plt.loglog(w, f2, linestyle='dotted')
#plt.loglog(w, f3, linestyle='dotted')
#plt.loglog(w, f5, linestyle='dotted')
#plt.loglog(w, f4, linestyle='dotted')


plt.show()
