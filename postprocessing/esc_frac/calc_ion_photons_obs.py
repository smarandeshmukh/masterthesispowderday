import numpy as np
import os
import sys
from hyperion.model import ModelOutput
import h5py
import matplotlib.pyplot as plt

def calc_ion_phot(m):

	#Access data
	sed = m.get_sed(aperture = -1, distance = None)
	lambdas = sed.wav
	nu = sed.nu
	flux = sed.val

	#Define key frequency. Note that the frequencies are in ascending order
	nu_break = 3*10**17 / 91.126
	nu_break_id = np.argmin( np.abs(nu - nu_break) )
	#print nu[nu_break_id:]

	n_ion_array = np.zeros(len(flux))

	for i in range(len(flux)):
		
		#Change flux to flux density
		f_dens = flux[i] / nu

		#And integrate from nu = max to nu = nu_break
		n_ion = np.trapz(f_dens[nu_break_id:] / ( 6.62e-27*nu[nu_break_id:] ), nu[nu_break_id:])
		n_ion_array[i] = n_ion

	#print len(nu[nu_break_id:] ), np.amin( 3*10**14 / nu), np.amax( 3*10**14 / nu), 3*10**17/nu[nu_break_id]
	#print lambdas.shape, nu.shape, flux.shape

	#plt.loglog(nu[nu_break_id:], f_dens[nu_break_id:] )
	#plt.show()

	return n_ion_array

if __name__=='__main__':
	try:
		sed_name = sys.argv[1]
		output = sys.argv[2]
	except IndexError:
		raise IndexError("...")

	#Checks....

	model = ModelOutput(sed_name)

	n_ions = calc_ion_phot(model)

	n_ion_str = ''
	for x in n_ions:
		n_ion_str = n_ion_str + ' ' + str(x)

	print n_ion_str

	#Try and get haloid from inp hdf5
	try:
		basename = os.path.basename(sed_name)
		ind = basename.index('gal')
		haloid = basename[ind + 3: ind + 3 + 4]
	except:
		haloid = 'galaxy'

	#Print to file
	if not os.path.exists(output):
		o = open(output, 'w')
		o.write('#Number of ionizing photons observed by stars per unit time\n')
		o.write('#Haloid(0) N_dot(1) \n')
		o.write('#int(0) 1/s(1)\n')
		o.close()
	o = open(output, 'a')
	o.write(str(haloid) + ' ' + n_ion_str + '\n')

	"""
	from hyperion.grid import OctreeGrid
	grid = model.get_quantities(iteration = 5)

	
	print type(grid), type(grid['density'])

	g = h5py.File('temp7.hdf5', 'w')
	grid.write(g, quantities='all')
	
	pf = grid.to_yt()
	from yt.mods import ProjectionPlot
	prj = ProjectionPlot(pf, 'y', ['density','temperature'], center=[0.0, 0.0, 0.0])
	prj.set_cmap('density', 'gist_heat')
	prj.set_cmap('temperature', 'gist_heat')
	prj.set_log('density', True)
	prj.save()
	"""
