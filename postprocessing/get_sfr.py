import numpy as np
from hyperion.model import ModelOutput
import sys
import os
from astropy.cosmology import LambdaCDM
import astropy.units as u
from astropy import constants as const
import matplotlib.pyplot as plt


def main():
	print 'hi'

	try:
		inp = sys.argv[1]
		out = sys.argv[2]
	except:
		print 'Not enough arguments!'
		print 'Fomat: [script name] [input] [output] [additional arguments]'
		print 'Additional arguments come pairwise: [option] [value]'
		print 'options = method(FUV,TIR,24mu,70mu), d_method(TIR,25mu)'
		raise Exception('Not enough arguments!')

	#Set default parameters
	funits = 'ergs/s'
	redshift = 0
	distance = None
	method = 'TIR'
	d_method = None
	
	#Check for additional parameters
	if len(sys.argv) == 3:
		print "Using default parameters. Fluxes will be in erg/s."
	else:
		args = sys.argv[3:]
		if 'method' in args:
			method = args[args.index('method') + 1 ]
		if 'd_method' in args:
			d_method = args[args.index('d_method') + 1 ]
	
	#Print user inputs
	print 'Input file:', inp
	print 'Output file:', out
	print 'SFR calculation method:', method
	print 'SFR calculation dust correction method:', d_method

	#Access input file
	m = ModelOutput(inp)
	sed = m.get_sed(distance = distance, inclination ='all', aperture = -1, units = funits)
	#Get wavelengths, frequencies and fluxes
	wav = sed.wav*(1 + redshift)
	nu = sed.nu/(1 + redshift)
	flux = sed.val

	#Initialize plot
	fig = plt.figure(dpi = 1200, figsize = (16,9))
	ax = fig.add_subplot(1,1,1)

	#Access different inclinations
	for inclination in range(flux.shape[0]):
		#Plot in function of wavelength
		flux_i = flux[inclination,:]
		ax.set_xlim(np.amin(wav), np.amax(wav))
		ax.set_ylim(10e39, np.amax(flux) )
		ax.set_xlabel('wavelength $\lambda$ (microns)')
		ax.set_ylabel('flux $\\nu L_\\nu$ ' + '(' + str(funits) + ')')

		#Calculate SFR
		sfr = get_sfr(wav, nu, flux_i, distance,  method, d_method, quiet=False)
		string = 'inclination_' + str(inclination) + " SFR= " + str("%.2f" % sfr) + ' Msun/yr'
		ax.loglog(wav, flux_i, label = string, marker = "+" )
	ax.legend(loc=2)
	title_string = 'Galaxy SED in rest frame with SFR calculated using ' + method
	if d_method != None:
		title_string += 'using ' + d_method + 'dust_correction'
	ax.set_title(title_string, size=24)

#	plt.show()
	if out != 'None':
		fig.savefig(out)
	else:
		print 'Not saving figure'

def get_sfr(w, f, flux, d, method, d_method, quiet=True):
	if not quiet:
		print 'Using the following band:', method
	if method == 'TIR':
		if not quiet:
			print 'Warning: No dust correction will be used.'
		#Define some constants and TIR wavelength range
		c = const.c.to(u.micron / u.s).value
		#Get index of closest frequencies
		fmin = c/1000
		fmax = c/8
		#Get index of those frequencies
		ifmin = np.argmin( np.abs(fmin-f) )
		ifmax = np.argmin( np.abs(fmax-f) )
#		print ifmin, ifmax, w[ifmin], f[ifmin], w[ifmax], f[ifmax]
		#Convert to flux spectral density and integrate over frequency range
		flux_mod = flux/f
		l =  np.trapz(flux_mod[ifmin:ifmax], f[ifmin:ifmax])*u.erg/u.s
		logC = 43.41
		if not quiet:
			print 'SFR =', np.power(10, np.log10(l.value) - logC)
		return np.power(10, np.log10(l.value) - logC)

	if method == 'FUV':
		c = const.c.to(u.micron / u.s).value
		#Calculate FUV lum for lambda = 152.8nm (emitted). Closest wavelength to that.
		lambda_FUV = 0.1528
		ilambda_FUV = np.argmin( np.abs(lambda_FUV - w) )
		l_FUV = flux[ilambda_FUV]
		if not quiet:
			print 'FUV luminosity', l_FUV
		#Apply the correction
		if d_method == 'TIR':
			if not quiet:
				print 'Dust correction method:', d_method
			#Get frequencies and corresponding indices
			fmin = c/1000
			fmax = c/8
			ifmin = np.argmin( np.abs(fmin-f) )
			ifmax = np.argmin( np.abs(fmax-f) )
			#Convert to flux spectral density and integrate over frequency range
			flux_mod = flux/f
			l_TIR =  np.trapz(flux_mod[ifmin:ifmax], f[ifmin:ifmax])*u.erg/u.s
			l_FUV_corr = l_FUV + 0.46*l_TIR.value
		if d_method == '25mu':
			if not quiet:
				print 'Dust correction method:', d_method
			#Get indice for 25mu
			imu_25 = np.argmin( np.abs(25-w) )
			l_25 = flux[imu_25]
			l_FUV_corr = l_FUV + 3.89*l_25
		if not quiet:
			print 'Corrected FUV luminosity =', l_FUV_corr
		logC = 43.35
		if not quiet:
			print '\t SFR =', np.power(10, np.log10(l_FUV_corr) - logC)
		return np.power(10, np.log10(l_FUV_corr) - logC)

	if method == '70mu':
		iwav = np.argmin( np.abs(70 - w) )
#		print w[iwav], flux[iwav], np.log10(flux[iwav])
		logC = 43.23
		if not quiet:
			print '\t SFR =', np.power(10, np.log10(flux[iwav]) - logC)
		return np.power(10, np.log10(flux[iwav]) - logC)
	if method == '24mu':
		iwav = np.argmin( np.abs(24 - w) )
#		print w[iwav], flux[iwav], np.log10(flux[iwav])
		logC = 42.69
		if not quiet:
			print '\t SFR =', np.power(10, np.log10(flux[iwav]) - logC)
		return np.power(10, np.log10(flux[iwav]) - logC)
	
	return None


if __name__=="__main__":
	main()
