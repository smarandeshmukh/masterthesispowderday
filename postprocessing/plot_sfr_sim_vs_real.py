import numpy as np
import sys
import os
import matplotlib.pyplot as plt

def main():
	print 'hi'

	try:
		sed = sys.argv[1]
		sim = sys.argv[2]
		out = sys.argv[3]
	except:
		print 'Syntax: [script name] [sed_sfr file] [sim sfr file] [output root folder]'
		raise Exception('Wrong input')

	#Print user input here

	#Create root folder
	if not os.path.exists(out):
		os.makedirs(out)

	#Access sed file and column numbers
	sed_sfr = np.loadtxt(sed)
	face_TIR_col = 3
	face_24mu = 4
	face_FUV_25mu = 6
	face_FUV_TIR = 7

	#Access sim file and column numbers
	sim_sfr = np.loadtxt(sim)
	haloid_col = 0
	ages = np.array([10,40,70,100])
	halos = sim_sfr[:,haloid_col]

	#Read data
	TIR_sfr = sed_sfr[:,3]
	mu24_sfr = sed_sfr[:,4]
	FUV_25mu_sfr = sed_sfr[:,6]
	FUV_TIR_sfr = sed_sfr[:,7]
	methods = ['TIR', '24$\mu m$', 'FUV(25$\mu m$)', 'FUV(TIR)']
	sfr_array = np.array([TIR_sfr, mu24_sfr, FUV_25mu_sfr, FUV_TIR_sfr])


	for i in range(len(ages))[:]:
		fig = plt.figure(dpi = 800, figsize = (16,9))
		ax = fig.add_subplot(1,1,1)
		ax.plot(sim_sfr[:,i+1], label='SFR from sim, $\Delta t$ (Myr) = ' + str(ages[i]))

		for j in range(len(sfr_array))[:]:
			ax.plot(sfr_array[j], '--',  label=methods[j])

		ax.set_xlabel('Pseudo Halo index', size = 24)
		ax.set_ylabel('SFR (M$_\odot$/yr)', size = 24)
		ax.set_ylim(top = 20)
		ax.set_title('SED inferred SFR vs simulation SFR for $\Delta t$ (Myr) = ' + str(ages[i]), size = 24)
		ax.legend(loc = 1)
		fig.savefig(out + '/' + str(ages[i]) + '.pdf')




if __name__ == '__main__':
	main()
