import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import scipy.optimize

def main():

	def linear_eq(x,a,b):
		return a*x + b
	
	#Write file names here
	mass_file_name = "/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/03052016/mass_02vir.txt"
	sfr_file_name_TIR = "/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/03052016/sfr_sed2TIR.txt"
	sfr_file_name_24mu = "/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/03052016/sfr_sed224mu.txt"
	sfr_file_name_70mu = "/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/03052016/sfr_sed270mu.txt"
	sfr_file_name_FUV_25mu = "/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/03052016/sfr_sed2FUV_25mu.txt"
	sfr_file_name_FUV_TIR = "/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/03052016/sfr_sed2FUV_TIR.txt"

	mass = np.loadtxt(mass_file_name)
	sfr_TIR = np.loadtxt(sfr_file_name_TIR)
	sfr_24mu = np.loadtxt(sfr_file_name_24mu)
	sfr_70mu = np.loadtxt(sfr_file_name_70mu)
	sfr_FUV_25mu = np.loadtxt(sfr_file_name_FUV_25mu)
	sfr_FUV_TIR = np.loadtxt(sfr_file_name_FUV_TIR)

#	print mass.shape, sfr.shape
#	print mass[:, 0], sfr[:,0]

	#Access m_cube data
	m_cube = mass[:,2]

	#Access face-on SFR data for TIR
	m_sfr_face_TIR = sfr_TIR[:,1]
	m_sfr_face_24mu = sfr_24mu[:,1]
	m_sfr_face_FUV_25mu = sfr_FUV_25mu[:,1]
	m_sfr_face_FUV_TIR = sfr_FUV_TIR[:,1]

	#initiliaze figure
	fig = plt.figure(dpi = 800, figsize = (16,9))
	ax = fig.add_subplot(1,1,1)

	print m_cube.shape, m_sfr_face_TIR.shape

	#Calculate linear fits
	params_TIR, sigma2_TIR = scipy.optimize.curve_fit(linear_eq, np.log10(m_cube), np.log10(m_sfr_face_TIR))
	params_24mu, sigma2_24mu = scipy.optimize.curve_fit(linear_eq, np.log10(m_cube), np.log10(m_sfr_face_24mu))
	params_FUV_25mu, sigma2_FUV_25mu = scipy.optimize.curve_fit(linear_eq, np.log10(m_cube), np.log10(m_sfr_face_FUV_25mu))
	params_FUV_TIR, sigma2_FUV_TIR = scipy.optimize.curve_fit(linear_eq, np.log10(m_cube), np.log10(m_sfr_face_FUV_TIR))

	#write labels
	label_TIR = 'TIR; a = ' + '%.2f' % params_TIR[0] + '$\pm$' + '%.2f' % np.sqrt(sigma2_TIR[0,0]) + '; log(b) = ' + '%.2f' % params_TIR[1] + '$\pm$' + '%.2f' % np.sqrt(sigma2_TIR[1,1])
	label_24mu = '24$\mu m$; a = ' + '%.2f' % params_24mu[0] + '$\pm$' + '%.2f' % np.sqrt(sigma2_24mu[0,0]) + '; log(b) = ' + '%.2f' % params_24mu[1] + '$\pm$' + '%.2f' % np.sqrt(sigma2_24mu[1,1])
	label_FUV_25mu = 'FUV(25$\mu m$); a = ' + '%.2f' % params_FUV_25mu[0] + '$\pm$' + '%.2f' % np.sqrt(sigma2_FUV_25mu[0,0]) + '; log(b) = ' + '%.2f' % params_FUV_25mu[1] + '$\pm$' + '%.2f' % np.sqrt(sigma2_FUV_25mu[1,1])
	label_FUV_TIR = 'FUV(TIR); a = ' + '%.2f' % params_FUV_TIR[0] + '$\pm$' + '%.2f' % np.sqrt(sigma2_FUV_TIR[0,0]) + '; log(b) = ' + '%.2f' % params_FUV_TIR[1] + '$\pm$' + '%.2f' % np.sqrt(sigma2_FUV_TIR[1,1])

	#plot data
	ax.loglog(m_cube, m_sfr_face_TIR, 'ro', label = label_TIR)
	ax.loglog(m_cube, m_sfr_face_24mu, 'mx', label = label_24mu)
	ax.loglog(m_cube, m_sfr_face_FUV_25mu, 'b*', label = label_FUV_25mu)
	ax.loglog(m_cube, m_sfr_face_FUV_TIR, 'c*', label = label_FUV_TIR)
	ax.grid(True, which='both')

	#plot fits
	data_x = np.array([np.amin(m_cube), np.amax(m_cube)])
	TIR_fit_data =  np.array( [10**( linear_eq(np.log10(x), params_TIR[0], params_TIR[1]) ) for x in data_x] )
	mu24_fit_data = np.array( [10**( linear_eq(np.log10(x), params_24mu[0], params_24mu[1]) ) for x in data_x] )
	FUV_25mu_fit_data = np.array( [10**( linear_eq(np.log10(x), params_FUV_25mu[0], params_FUV_25mu[1]) ) for x in data_x] )
	FUV_TIR_fit_data = np.array( [10**( linear_eq(np.log10(x), params_FUV_TIR[0], params_FUV_TIR[1]) ) for x in data_x] )

	ax.loglog(  data_x, TIR_fit_data, 'r')
	ax.loglog(  data_x, mu24_fit_data, 'm')
	ax.loglog(  data_x, FUV_25mu_fit_data, 'b')
	ax.loglog(  data_x, FUV_TIR_fit_data, 'c')
	
	#set labels
	ax.set_xlabel('Galaxy Stellar Mass ( $M_{\odot}$ )', size = 24)
	ax.set_ylabel('SFR ( $M_{\odot}/yr$ )', size = 24)
	ax.set_title('SFR vs Galaxy Stellar Mass with linear fits: $\mathrm{log(SFR)} = a\cdot\mathrm{log(M_\odot)} + b$', size = 24)
	ax.legend(loc=4)
	fig.savefig('/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/sfr-m4.pdf')




if __name__=='__main__':
	main()
