#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/sed_analysis/get_flux_in_range.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/31082016'
base_txt='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/31082016/IR_24_rest.txt'

wmin='24'
wmax=""

start=$[0]
target=$[162]

full_list=($inp_root/*)
sub_list=${full_list[@]:$start:$target}

rm -f ${base_txt:0:-4}*

header_str="'#Computing fluxes in wavelengths: ' $wmin ' to ' $wmax \n"

for i in $sub_list
do
	file_name=$i/*.rtout
	echo $file_name
	python $exe $file_name mono 0 $base_txt wmin $wmin wmax $wmax quiet
done
