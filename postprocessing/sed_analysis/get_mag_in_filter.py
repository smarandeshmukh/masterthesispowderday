import numpy as np
import os
import sys
from hyperion.model import ModelOutput
from scipy.interpolate import interp1d

quiet = False

def get_flux_in_filter(m, f, z):
	
	uncertainity_bool = False

	#Acces the sed file here at z = 0. Try with/without uncertainties
	try:
		sed = m.get_sed(distance = None, aperture = -1, inclination = 'all', uncertainties = True)
		uncertainity_bool = True
	except:
		sed = m.get_sed(distance = None, aperture = -1, inclination = 'all', uncertainties = False)
	nu = sed.nu
	wav = sed.wav
	flux = sed.val
	#Check if there are uncertainties
	if uncertainity_bool:
		unc = sed.unc
	else:
		unc = np.zeros(flux.shape)

	#Interpolate the filter file
	filt_wav = f[:,0] / 10000.0 * 1 / (1+z) #Change to microns and redshift
	filt_trans = f[:,1]
	func = interp1d(filt_wav, filt_trans, kind = 'linear', bounds_error = False, fill_value = 0)
	#Get the norm
	norm = np.trapz(filt_trans, filt_wav)
	
	"""
	import matplotlib.pyplot as plt
	plt.plot( np.arange(0, 1000, 0.0001), func( np.arange(0, 1000, 0.0001) ) )
	plt.show()
	"""

	#Reshape the flux to a 2d array if necessary
	if flux.ndim == 1:
		flux = flux[None, :]
		unc = unc[None, :]

	#Initialize the final flux array
	filt_flux = np.zeros( len(flux) )

	#Sample transmission at sed wavelengths
	filt_trans_arr = func(wav)
	
	#Calculate the mean wavelength of the filter
	mean_wavelength = np.trapz( filt_wav * filt_trans, filt_wav) / norm

	#Access each inclination and iterate
	for i in range(len(flux)):
		
		#Change to flux density
		f_dens = flux[i] / wav

		"""
		import matplotlib.pyplot as plt
		plt.plot( wav, func( wav ), '+' )
		plt.show()
		"""

		#Integrate now and divide by the norm
		filt_flux[i] = np.abs( np.trapz( filt_trans_arr * flux[i], wav) / norm )

#	print nu.shape, wav.shape, flux.shape, unc.shape, norm, mean_wavelength

	return filt_flux, mean_wavelength

def get_magnitude(flux, mean):

	#Now start converting the luminosity to Magnitude system AB
	dis = 4*np.pi*(10*3.086e18)**2

	#Get frequency of the light
	freq = 3*10**14 / mean

	#Norm factor for AB system
	J = 3631 #Jy

	#Finally convert to magnitudes
	Mag = -2.5*np.log10( (flux / (dis * freq)) * 1e23 / J  )

	return Mag


if __name__=='__main__':
	
	#Check user input
	num_of_args = len( sys.argv )
	if num_of_args < 4:
		print 'This calculates the magnitudes given a filter profile'
		print 'Syntax: [script name] [sed file] [output] [redshift] [filter files]'
		raise Exception('Check Syntax')

	#Check if input (1) exists (2) is compatible with Hyperion
	sed_name = sys.argv[1]
	assert os.path.exists(sed_name), "You're sed file does not exist:\n %s " % sed_name
	try:
		m = ModelOutput(sys.argv[1])
		sed_file = m.get_sed()
		del sed_file
	except:
		raise Exception("Hyperion was unable to open the sed file")

	#Check whether to run in quiet mode
	if 'quiet' in sys.argv:
		quiet = True

	#Access the output
	output = sys.argv[2]

	#Get the redshift
	z = float(sys.argv[3])

	#Access the filter files
	filter_files = sys.argv[4:]

	#Initialize final array
	final_array = []
	wavelength_array = []

	for f in filter_files:

		#Check if the transmission file is ok
		try:
			filt = np.loadtxt(f)
		except:
			raise IOError('Could not access the filter file: %s' % f)
		assert filt.ndim > 1, 'filter file has to have at least 2 dimensions'

		flux_for_f, mean_wavelength =  get_flux_in_filter(m, filt, z)

		magnitude = get_magnitude(flux_for_f, mean_wavelength)
		final_array.append(magnitude)
		wavelength_array.append(mean_wavelength)

	#Write to file
	if not os.path.exists(output):
		o = open(output, 'w')
		o.write('#Magnitudes at different inclinations for different filters\n')
		o.write('#' + ' '.join(map(str, wavelength_array))  + '\n')
		o.write('#Filter name(0) Inclination(1)\n')
		o.close()
	o = open(output, 'a')
	for filter_id in range(len(filter_files)):
		o.write( os.path.basename( filter_files[filter_id] ) + ' ' )
		for mag_at_inclination in final_array[filter_id]:
			o.write(str(mag_at_inclination) + ' ')
		o.write('\n')

