import numpy as np
import os
import sys
from hyperion.model import ModelOutput
from scipy import interpolate
from astropy import units as u

quiet = False

int_min_limit = ''
int_max_limit = ''

def rest(model_hyp, mode):

	global int_max_limit
	global int_min_limit
	
	if not quiet:
		print "Running in 'restframe' mode"

	#Check which inclinations to analyze
	if 'faceon' in sys.argv:
		inclination = 0
		if not quiet:
			print 'Calculating for face on inclinations'
	elif 'edgeon' in sys.argv:
		inclination = 2
		if not quiet:
			print 'Calculating for edge on inclinations'
	else:
		inclination = 'all'
		if not quiet:
			print 'Calculating for all inclinations'
	
	sed = model_hyp.get_sed(inclination = inclination, uncertainties = True, aperture = -1)

	#Access data from sed
	lambdas = sed.wav
	nu = sed.nu
	flux = sed.val
	unc = sed.unc

	#Reshape array to maintain generality
	if inclination != 'all':
		flux = np.array( flux )[None, :]
		unc  = np.array( unc )[None, :]

	#Get the monochromatic wavelength
	try:
		lambda_min = sys.argv[ sys.argv.index('wmin') + 1]
		lambda_min = float(lambda_min)
		int_min_limit = str(lambda_min)
		if not quiet:
			print 'wmin = ', lambda_min, 'microns'
	except ValueError:
		raise ValueError('wmin entry not valid. wmin has to a float!')
	if mode == 'integrated':
		try:
			lambda_max = sys.argv[ sys.argv.index('wmax') + 1]
			lambda_max = float(lambda_max)
			int_max_limit = str(lambda_max)
			if not quiet:
				print 'wmax = ', lambda_max, 'microns'
		except:
			raise ValueError('wmax entry required in "integrated" mode. wmax entry not valid. wmax has to a float!')

	#Enter loop for analysis
	rest_flux_arr = np.array([])
	err_rest_flux_arr = np.array([])
	for i in range( len(flux) ):
		f_dens = interpolate.interp1d(lambdas, flux[i]/lambdas, bounds_error = False, fill_value = 0)
		f_dens_err = interpolate.interp1d(lambdas, unc[i]/lambdas, bounds_error = False, fill_value = 0)
		
		if mode == 'mono':
			rest_flux, err_rest_flux = mono(f_dens, f_dens_err, lambda_min)
		if mode == 'integrated':
			rest_flux, err_rest_flux = integrated(f_dens, f_dens_err, lambda_min, lambda_max)
		
		rest_flux_arr = np.append(rest_flux_arr,  rest_flux )
		err_rest_flux_arr = np.append(err_rest_flux_arr, err_rest_flux)

	return rest_flux_arr, err_rest_flux_arr, sed.units

def redshifted(model_hyp, mode):

	global int_min_limit
	global int_max_limit

	if not quiet:
		print 'Running in "redshift" mode'

	#Check which inclinations to analyze
	if 'faceon' in sys.argv:
		inclination = 0
		if not quiet:
			print 'Calculating for face on inclinations'
	elif 'edgeon' in sys.argv:
		inclination = 2
		if not quiet:
			print 'Calculating for edge on inclinations'
	else:
		inclination = 'all'
		if not quiet:
			print 'Calculating for all inclinations'
	

	#Access the distance to use from the cosmology. Assume Flat Cosmology and luminosity distance
	#Enter default values here
	H0 = 70.
	Om0 = 0.3
	Ode0 = 0.7

	#Read user cosmo values values here if written
	if 'H0' in sys.argv:
		try:
			H0 = float(sys.argv[ sys.argv.index('H0') + 1] )
			Om0 = float(sys.argv[ sys.argv.index('Om0') + 1] )
			Ode0 = float(sys.argv[ sys.argv.index('Ode0') + 1] )
		except:
			raise Exception('Error reading the cosmological parameters.')
	
	z = float(sys.argv[3])

	from astropy.cosmology import LambdaCDM
	cosmo = LambdaCDM(H0, Om0, Ode0)
	distance = cosmo.luminosity_distance(z).to(u.cm).value

	if not quiet:
		print 'Cosmology used to calculate the luminosity distance:'
		print 'H0=', H0, 'Om0=', Om0, 'Ode0=', Ode0
		print 'z = ', z
		print 'Luminosity distance:', (distance*u.cm).to(u.Mpc)

	sed = model_hyp.get_sed(inclination = inclination, uncertainties = True, aperture = -1, distance = distance)

	#Access data from sed
	lambdas = sed.wav
	nu = sed.nu
	flux = sed.val
	unc = sed.unc

	#Reshape array to maintain generality
	if inclination != 'all':
		flux = np.array( flux )[None, :]
		unc  = np.array( unc )[None, :]

	#Get the monochromatic wavelength
	try:
		lambda_min = sys.argv[ sys.argv.index('wmin') + 1]
		lambda_min = float(lambda_min)
		int_min_limit = str(lambda_min)
		if not quiet:
			print 'wmin = ', lambda_min, 'microns'
	except ValueError:
		raise ValueError('wmin entry not valid. wmin has to a float!')
	if mode == 'integrated':
		try:
			lambda_max = sys.argv[ sys.argv.index('wmax') + 1]
			lambda_max = float(lambda_max)
			int_max_limit = str(lambda_max)
			if not quiet:
				print 'wmax = ', lambda_max, 'microns'
		except ValueError:
			raise ValueError('wmax entry required in "integrated" mode. wmax entry not valid. wmax has to a float!')
		except IndexError:
			raise IndexError('Please enter a value for "wmax"')

	#Enter loop for analysis
	rest_flux_arr = np.array([])
	err_rest_flux_arr = np.array([])
	for i in range( len(flux) ):
		f_dens = interpolate.interp1d(lambdas, flux[i]/lambdas, bounds_error = False, fill_value = 0)
		f_dens_err = interpolate.interp1d(lambdas, unc[i]/lambdas, bounds_error = False, fill_value = 0)
		
		if mode == 'mono':
			rest_flux, err_rest_flux = mono(f_dens, f_dens_err, lambda_min)
		if mode == 'integrated':
			rest_flux, err_rest_flux = integrated(f_dens, f_dens_err, lambda_min, lambda_max)
		
		rest_flux_arr = np.append(rest_flux_arr,  rest_flux )
		err_rest_flux_arr = np.append(err_rest_flux_arr, err_rest_flux)

	return rest_flux_arr, err_rest_flux_arr, sed.units

def mono(func, err, wavelength):
	return func(wavelength) * wavelength, err(wavelength) * wavelength

def integrated(func, err, w0, w1):
	from scipy.integrate import quadrature as int_method
	step_size = np.abs( (w1-w0) / 2000 )
	x_values = np.arange( w0, w1, step_size )
	return np.trapz( func(x_values), x_values), np.trapz( err(x_values), x_values)
	#1.69723519574e+44 7.271061326e+41

if __name__ == '__main__':
	
	num_of_args = len( sys.argv )
	#If no arguments print syntax
	if num_of_args < 4:
		print 'This script will calculate the flux at a single wavelength or the integrated flux in a range.'
		print 'General syntax:'
		print '[script name] [sed file from Hyperion] [mode] [redshift] [output] [minimum wavelength] [extra args]'
		print 'Details: (All paths should be absolute) \n[sed file from Hyperion] -> usually ends in .rtout'
		print '[mode] -> mode = "mono"/"integrated" '
		print '[redshift] -> a float'
		print '[output] -> full path of the text file. Different inclinations will be written to different ouputs using the same basename'
		print '[minimum wavelength] -> in microns. e.g.: script.py [other args] wmin 0.5 \n extra args:\n'
		print '[max wavelength] -> in microns. e.g.: script.py [other args] wmax 0.6'
		print '[cosmology for LambdaCDM] e.g: H0 69.3 Om0 0.3 Ode0 0.7'
		print 'quiet -> if you want to run with no print statements'
		raise SyntaxError("")
	
	#Check whether to run in quiet mode
	if 'quiet' in sys.argv:
		quiet = True
	
	#Check in what mode to run: mono/integrated
	mode = sys.argv[2]
	assert mode in ('mono', 'integrated'), 'Mode has to be "mono" or "integrated"'
	if not quiet:
		print 'Running in', mode, 'mode'
	
	#Check if input (1) exists (2) is compatible with Hyperion
	sed_name = sys.argv[1]
	assert os.path.exists(sed_name), "You're sed file does not exist:\n %s " % sed_name
	try:
		m = ModelOutput(sys.argv[1])
		sed_file = m.get_sed()
		del sed_file
	except:
		raise Exception("Hyperion was unable to open the sed file")
	
	#Check whether to push back galaxy
	try:
		to_z = float(sys.argv[3])
	except ValueError:
		raise ValueError('Redshift as to be a number')

	#Get basename of output and check existence....
	output = sys.argv[4]
	if not os.path.exists( os.path.dirname(output) ):
		os.makedirs( os.path.dirname(output) )
		if not quiet:
			print "Creating directory structure"
	
	#Print user inputs
	if not quiet:
		print 'SED file:', sed_name
		print 'Redshift:', to_z
		print 'Output file basename:', output

	#Point to correct branch. Rest() if to_z == 0, redshifted() otherwise
	gal_pos_dict = {True: rest, False: redshifted}
	final_flux, err_final_flux, flux_units = gal_pos_dict[to_z == 0](m, mode)
	

	#Get haloid from name of sed file
	sed_base = os.path.basename(sed_name)
	try:
		ind = sed_base.index('gal')
		halo_id = sed_base[ind + 3: ind + 7]
	except:
		halo_id = str(0000)

	#Parse base outputfile to create new ones
	o_base = os.path.join(os.path.dirname(output), os.path.basename(output).split('.')[0] + '_' )
	#Print to files
	for i in range( len(final_flux) ):
		file_path = o_base + str(i) + '.txt'
		if not os.path.exists(file_path):
			write_mode = 'w'
			f = open(file_path, write_mode)
			intro_string = '#Computing fluxes in the wavelength range of [microns]: ' + int_min_limit + ' to ' + int_max_limit + '\n'
			units_string = '#int(0) ' + flux_units + '(1) ' + flux_units + '(2) \n'
			f.write(intro_string + '#Halo id(0) Flux(1) sig_f(2)\n' + units_string )
			f.close()
		write_mode = 'a'
		f = open(file_path, write_mode)
		f.write(halo_id + ' ' + str(final_flux[i]) + ' ' + str(err_final_flux[i]) + '\n')
		f.close()
	
	if not quiet:
		print 'Flux units:', flux_units
		print 'Flux values for inclinations:', final_flux
		print 'Flux uncertainties:', err_final_flux


"""
Example syntax:
python get_flux_in_range.py /vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/24062016/0000/gal0000_lumtest_n1_p600.rtout.sed integrated 0 /vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/results/04072016/flux_test.txt wmin 8 wmax 1000 H0 67.8 Om0 0.28 Ode0 0.72 faceon
"""
