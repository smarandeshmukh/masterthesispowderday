#!/bin/bash

exe='/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/postprocessing/sed_analysis/get_mag_in_filter_with_error.py'
inp_root='/vol/aibn1058/data1/sdeshmukh/RT_files/bigruns/outputs/31082016'
base_txt='fast_mags.txt'

wmin='24'
wmax=""

start=$[0]
target=$[162]

full_list=($inp_root/*)
sub_list=${full_list[@]:$start:$target}

rm -f ${base_txt:0:-4}*

header_str="'#Computing fluxes in wavelengths: ' $wmin ' to ' $wmax \n"

for i in $sub_list
do
	file_name=$i/*.rtout*
	echo $file_name
	rm -f $i/$base_txt
	python $exe $file_name $i/$base_txt 3.109 /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Subaru_FOCAS.B.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Subaru_FOCAS.V.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_u.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_g.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_r.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_i.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SDSS_z.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/HST_WFPC2.f814w_I_814.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_IRAC.I1_3.6.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_IRAC.I2_4.5.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_IRAC.I3_5.8.dat /vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/Spitzer_IRAC.I4_8.0.dat
done
