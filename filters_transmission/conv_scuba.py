import numpy as np

#Convert the Scuba filter profile to proper stuff

d = np.loadtxt('/vol/aibn1058/data1/sdeshmukh/Downloads/model450.pdf')

#Access the freq column and transmission
freq = d[:,0]
trans = d[:, -1]

#Calculate the wavelegnths
c = 299792458
wave = c / (freq*10**9)

#Convert to angstrom
wave = wave * 10**10

#Reverse it
wave = wave[::-1]
trans = trans[::-1]

final = np.vstack( (wave,trans) ).T
np.savetxt('/vol/aibn1058/data1/sdeshmukh/gitrepo/powderday/filters_transmission/SCUBA2_450.dat', final)
