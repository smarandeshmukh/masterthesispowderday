import h5py
import sys
import os
import astropy.cosmology as ac
import numpy as np
from mpi4py import MPI

def main():

	#Initialize MPI communicator
	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()

	#If we run into memory issues divide into further subchunks
	subchunks = 40

	#Define some functions
	def get_age_at_z(redshift):
		
		H0 = f['Header'].attrs['HubbleParam']
		OmegaLambda = f['Header'].attrs['OmegaLambda']
		Omega0 = f['Header'].attrs['Omega0']
		cosmo = ac.LambdaCDM(100*H0, Omega0, OmegaLambda)
		return cosmo.lookback_time(redshift).value

	def get_scale_factor(age):
		redshift =  ac.z_at_value(get_age_at_z, age)
		return 1/(1+redshift)

	#Root node will verify user input and give the good-to-go signal to other processes
	if rank == 0:
		try:
			hdf5name = sys.argv[1]
			out = sys.argv[2]
		except IndexError:
			print "Not enough arguments."
			print "Usage:"
			print "[script name] [hdf5 file name] [outputfile]"
			comm.Abort()

		if not os.path.exists(hdf5name):
			print "Input file does not exist."
			comm.Abort()
			raise TypeError("Input file does not exist.")

		#Print user inputs
		print "Your hdf5 file is:", hdf5name

		#Open file
		f = h5py.File(hdf5name)

		#Check if ages exist for stars
		try:
			f['PartType4']['StellarFormationTime']
		except:
			print 'The block "PartType4/StellarFormationTime" does not exist'
			f.close()
			comm.Abort()

		#Check for size of star list
		nstars = f['PartType4']['StellarFormationTime'].shape[0]
#		nstars = 100
		print "Number of stars:", nstars

		#send chunks to other processes
		chunk_no = 0
		istart = 0
		size = comm.Get_size()
		for i in range(1, size):
			print "sending to " + str(i)
			iend = int( float(nstars)/(size - 1) * i  )
			envelope = [istart, iend]
			comm.send(envelope, dest = i, tag = 1)
			chunk_no += 1
			istart = iend
		#Initialize result array
		result_list = [None] * ((size-1)*subchunks)

		#Receive data from other processes
		packets_received = 0
		target = chunk_no * subchunks
		while packets_received != target:
			info = MPI.Status()
			check = comm.Iprobe(source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG)
			if check:
				data = comm.recv(source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG, status = info)
				packets_received += 1
				print "Yes! From " + str(info.source) + " = " + " with tag: " + str(info.tag) + " " + str(packets_received) + "/" + str(target)
				#Place data in new array
				result_list[info.tag] = data

		#Combine the chunks into one numpy array
		scale_arr = result_list[0]
		for element in result_list[1:]:
			scale_arr = np.append(scale_arr, element, axis = 0)
		
		#Initialize empty output file
		if os.path.exists(out):
			print "Removing Existing output file...."
			os.remove(out)
		o = h5py.File(out)

		#Start copy all non-star groups
		for group in f.keys():
			if group != 'PartType4':
				print "Copying", group
				f[group].copy(source = f[group], dest = o)
			else:
				o.create_group(group)
				for dataset in f[group].keys():
					if dataset != 'StellarFormationTime':
						path_inp =  f[group][dataset].name
						print "Copying path.....", path_inp
						f[group].copy(source = path_inp, dest = o[group])
					else:
						print "Writing 'StellarFormationTime' to", group
						print o[group].keys()
						o[group].create_dataset('StellarFormationTime', data = scale_arr)
		
		
		
		
		f.close()
		o.close()
		

	else:
		
		#Receive initial information about what to process
		info = MPI.Status()
		mail = comm.recv(source = 0, tag = MPI.ANY_TAG, status = info)
		print "Received: " + str(mail) + " with tag " + str(info.tag) + " in rank " + str(rank)
		
		#Open file
		f = h5py.File(sys.argv[1], 'r')

		#Star ages (Gyr) are written relative to the simulation redshift, while scale factor
		#is defined relative to present day. To correctly get the scale factor, we calculate the 
		#"simulation" age, add it to stellar ages and compute the scale factor from there.
		sim_age = get_age_at_z(f['Header'].attrs['Redshift'])
		
		#Do work here and send answer
		subchunk_no = 0
		istart = mail[0]
		while subchunk_no != subchunks:
			iend = int( (mail[1]-mail[0]) / (subchunks - subchunk_no) ) + mail[0]
			#Access orginal data here and add the simulation age
			original = f['PartType4']['StellarFormationTime'][istart:iend] + sim_age
			#create the new array here
			result = np.array([get_scale_factor(age) for age in original])
#			result = np.array([istart,iend])
			#Send to root
			comm.send(result, dest = 0, tag = subchunk_no + (rank-1)*subchunks)
			istart = iend
			subchunk_no += 1




	print rank
	"""
		H0 = f['Header'].attrs['HubbleParam']
		OmegaLambda = f['Header'].attrs['OmegaLambda']
		Omega0 = f['Header'].attrs['Omega0']

		a = f['PartType4']['StellarFormationTime'][:]

		print get_scale_factor(8 )
		age_in_sim = get_age_at_z(f['Header'].attrs['Redshift'])
		print "age in sim:", age_in_sim
		c = np.array([get_scale_factor(age + age_in_sim) for age in a[:5] ])
		print len(c), type(c), c[:10]
		f.close()
	"""
	

if __name__ == "__main__":
	main()
