import numpy
import matplotlib.pyplot as p
import math
#from age_universe import *
from matplotlib import gridspec
from matplotlib.colors import LinearSegmentedColormap


#wrapper to mimic scipy.io.fread
def fread(fileobj, size, datatype):
    return numpy.fromfile(fileobj, count=size, dtype=datatype)


def getParticleType(index, npart):
    if index < npart[0]:
        return 0
    elif index < npart[0]+npart[1]:
	return 1
    elif index < npart[0]+npart[1]+npart[2]:
	return 2
    elif index < npart[0]+npart[1]+npart[2]+npart[3]:
	return 3
    elif index < npart[0]+npart[1]+npart[2]+npart[3]+npart[4]:
	return 4
    elif index < npart[0]+npart[1]+npart[2]+npart[3]+npart[4]+npart[5]:
	return 5


#class to handle GADGET File
class GadgetFile:
    '''class containing information about a Gadget File'''
    def __init__(self):
        self.npart = None
        self.massarr = None
        self.time = None
        self.redshift = None
        self.flag_sfr = None
        self.flag_feedback = None
        self.npartTotal = None
        self.flag_cooling = None
        self.num_files = None
        self.BoxSize = None
        self.Omega0 = None
        self.OmegaLambda = None
        self.HubbleParam = None
        self.flag_age = None
        self.flag_metal = None
        self.npartTotalHW = None
        self.pos = None
        self.vel = None
        self.ids = None
        self.mass = None
        self.u = None
        self.rho = None
        self.ne = None
        self.nh = None
        self.hsml = None
        self.sfr = None
        self.age = None
        self.z = None
        self.winds = None
        self.pot = None
        self.acc = None

#function to read a GADGET file into the GadgetFile class
def ReadGadget(fname, fformat=0, longids=False, discard=[], opt_fields=[],  stop_at='', quiet=False, fill_mass=False):

    '''
    ReadGadget

    reads Gadget 2/3 file(s). 

    Usage:
    ReadGadget(fname, fnr, fformat=0, longids=False, discard=[], opt_fields=[],  stop_at='', global_vars=True, quiet=False, fill_mass=False)

    fname = [string] file name (no. number if multiple files)
    fformat = [int] output format (currently 1 or 2, no HDF5. Default value of 0 means: try to guess)
    longids = [bool] if the IDs are stored as long int or not
    discard = [list of string] list of fields to be (read and) discarded, to save memory
    opt_fields = [list of string] list of the optional fields present in the file
    stop_at = [string] skip everything *beyond* the given field
    ---the possible values for [opt_fields,]stop_at,discard are: ['header','pos','vel','ids','mass'],'u','rho','ne','nh','hsml','sfr','age','z','wind','pot','acc',''
    quiet = [bool] no message
    fill_mass = [bool] fill the mass array with masses also for particles with npart>0 && massarr>0
    '''

    # returning class
    gfile = GadgetFile()

    def checkBlockHeaders(bh1, bh2, where):
        if bh1 != bh2:
            print 'FATAL ERROR: blockheader not equal at ',where,'! Aborting...'
            return False
        else:
            return True


    def readBlockLabel(fileobj):
        blockheader1  =fread(fileobj,1,numpy.int32)
        title         =fread(fileobj,4,numpy.int8)
	if not quiet:
	    print chr(title[0]), chr(title[1]), chr(title[2]), chr(title[3])
        dummy         =fread(fileobj,1,numpy.int32)
        blockheader2  =fread(fileobj,1,numpy.int32)
        return blockheader1, blockheader2


    if fformat==0:
        tf = open(fname,"rb")
        blockheader1  =fread(tf,1,numpy.int32)
        if blockheader1 == 8:
            fformat = 2
            if not quiet:
                print "I'm guessing the file format is 2"
        elif blockheader1 == 256:
            fformat = 1
            if not quiet:
                print "I'm guessing the file format is 1"
        else:
            print "ERROR: file format not recognised!"
            tf.close()
            return
        tf.close()
        


    ##
    ## here we read the header of the snapshot
    ##

    if not quiet:
        print "Reading file ",fname

    tf = open(fname,"rb")

    if not quiet:
        print "reading header.."

    if fformat==2:
        blockheader1, blockheader2 = readBlockLabel(tf)
        ok = checkBlockHeaders(blockheader1, blockheader2, 'HEAD LABEL')
        if (not ok):
            tf.close()
            return gfile

    blockheader1  =fread(tf,1,numpy.int32)
    gfile.npart         =fread(tf,6,numpy.int32)
    gfile.massarr       =fread(tf,6,numpy.float64)
    gfile.time          =fread(tf,1,numpy.float64)
    gfile.redshift      =fread(tf,1,numpy.float64)
    gfile.flag_sfr      =fread(tf,1,numpy.int32)
    gfile.flag_feedback =fread(tf,1,numpy.int32)
    gfile.npartTotal    =fread(tf,6,numpy.int32)
    gfile.flag_cooling  =fread(tf,1,numpy.int32)
    gfile.num_files     =fread(tf,1,numpy.int32)
    gfile.BoxSize       =fread(tf,1,numpy.float64)
    gfile.Omega0        =fread(tf,1,numpy.float64)
    gfile.OmegaLambda   =fread(tf,1,numpy.float64)
    gfile.HubbleParam   =fread(tf,1,numpy.float64)
    gfile.flag_age      =fread(tf,1,numpy.int32)
    gfile.flag_metal    =fread(tf,1,numpy.int32)
    gfile.npartTotalHW  =fread(tf,6,numpy.int32)
    bytesleft=256-6*4 - 6*8 - 8 - 8 - 2*4 - 6*4 - 4 - 4 - 8 - 8 - 8 - 8 - 4 - 4 - 6*4
    la            =fread(tf,bytesleft,numpy.int8)
    blockheader2  =fread(tf,1,numpy.int32)
    ok = checkBlockHeaders(blockheader1, blockheader2, 'HEAD BLOCK')
    if (not ok):
        tf.close()
        return gfile

    if not quiet:
        print " "
        print "Header Data:"
        print " "
        print "Redshift: ", gfile.redshift
        print "Time: ", gfile.time
        print "Number of particles: ", gfile.npartTotal
        print "Number of particles in this file: ", gfile.npart
        print "Particle Massarr: ", gfile.massarr
        print " "

    if stop_at == 'header':
        tf.close()
        return gfile

    ## total number of particles in this file
    N = sum(gfile.npart)

    ##
    ## here we start reading the snapshot files
    ##

    if not quiet:
        print "reading positions..."

    if fformat==2:
        blockheader1, blockheader2 = readBlockLabel(tf)
        ok = checkBlockHeaders(blockheader1, blockheader2, 'POS LABEL')
        if (not ok):
            tf.close()
            return gfile

    blockheader1 = fread(tf, 1, numpy.int32)
    gfile.pos = fread(tf, 3*N, numpy.float32).reshape(N,3)
    blockheader2 = fread(tf, 1, numpy.int32)
    ok = checkBlockHeaders(blockheader1, blockheader2, 'POS BLOCK')
    if (not ok):
        tf.close()
        return gfile
    if 'pos' in discard:
        pos=None
        if not quiet:
            print 'discarded positions'
    if stop_at=='pos':
        tf.close()
        return gfile

    if not quiet:
        print  "reading velocities..."

    if fformat==2:
        blockheader1, blockheader2 = readBlockLabel(tf)
        ok = checkBlockHeaders(blockheader1, blockheader2, 'VEL LABEL')
        if (not ok):
            tf.close()
            return gfile

    blockheader1 = fread(tf, 1, numpy.int32)
    gfile.vel = fread(tf, 3*N, numpy.float32).reshape(N,3)
    blockheader2 = fread(tf, 1, numpy.int32)
    ok = checkBlockHeaders(blockheader1, blockheader2, 'VEL BLOCK')
    if (not ok):
        tf.close()
        return gfile
    if 'vel' in discard:
        vel=None
        if not quiet:
            print 'discarded velocities'

    if stop_at=='vel':
        tf.close()
        return gfile

    if not quiet:
        print  "reading ids..."

    if fformat==2:
        blockheader1, blockheader2 = readBlockLabel(tf)
        ok = checkBlockHeaders(blockheader1, blockheader2, 'IDS LABEL')
        if (not ok):
            tf.close()
            return gfile

    blockheader1 = fread(tf, 1, numpy.int32)
    if(longids):
        gfile.ids = fread(tf, N, numpy.uint64)
    else:
        gfile.ids = fread(tf, N, numpy.uint32)
    blockheader2 = fread(tf, 1, numpy.int32)
    ok = checkBlockHeaders(blockheader1, blockheader2, 'IDS BLOCK')
    if (not ok):
        tf.close()
        return gfile
    if 'ids' in discard:
        gfile.ids=None
        if not quiet:
            print 'discarded IDs'

    if stop_at=='ids':
        tf.close()
        return gfile

    #compute how many mass entries I have
    Nwithmass = 0
    for j in range(6):
        if ((gfile.npart[j] > 0) and (gfile.massarr[j] == 0.0)):
            Nwithmass += gfile.npart[j]

    if (Nwithmass > 0):
        if not quiet:
            print "reading masses..."

        if fformat==2:
            blockheader1, blockheader2 = readBlockLabel(tf)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'MASS LABEL')
        if (not ok):
            tf.close()
            return gfile

        blockheader1 = fread(tf, 1, numpy.int32)

    if fill_mass:
        gfile.mass = numpy.empty(N)
        for idx in range(6):
            if ((gfile.npart[idx]>0)&(gfile.massarr[idx]>0)):
                gfile.mass[sum(gfile.npart[0:idx]):sum(gfile.npart[0:idx+1])]=numpy.tile(gfile.massarr[idx],gfile.npart[idx])
            elif ((gfile.npart[idx]>0)&(gfile.massarr[idx]==0)):
                gfile.mass[sum(gfile.npart[0:idx]):sum(gfile.npart[0:idx+1])]=fread(tf, gfile.npart[idx], numpy.float32)
    else:
        gfile.mass = fread(tf, Nwithmass, numpy.float32)
    if (Nwithmass > 0):
        blockheader2 = fread(tf, 1, numpy.int32)
        ok = checkBlockHeaders(blockheader1, blockheader2, 'MASS BLOCK')
        if (not ok):
            tf.close()
            return gfile

    if 'mass' in discard:
        gfile.mass=None
        if not quiet:
            print 'discarded masses'

    if stop_at=='mass':
        tf.close()
        return gfile


    if (gfile.npart[0] > 0):

        if 'u' in opt_fields:
            if not quiet:
                print  "reading internal energies..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'U LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader1 = fread(tf, 1, numpy.int32)
            gfile.u = fread(tf, gfile.npart[0], numpy.float32)
            blockheader2 = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'U BLOCK')
            if (not ok):
                tf.close()
                return gfile

            if 'u' in discard:
                gfile.u=None
                if not quiet:
                    print 'discarded internal energies'

        if stop_at=='u':
            tf.close()
            return gfile


        if 'rho' in opt_fields:
            if not quiet:
                print  "reading densites..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'RHO LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader = fread(tf, 1, numpy.int32)
            gfile.rho = fread(tf, gfile.npart[0], numpy.float32)
            blockheader = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'RHO BLOCK')
            if (not ok):
                tf.close()
                return gfile
            if 'rho' in discard:
                gfile.rho=None
                if not quiet:
                    print 'discarded densities'

        if stop_at=='rho':
            tf.close()
            return gfile


        if 'ne' in opt_fields:
            if not quiet:
                print "reading electron number densities..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'NE LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader1 = fread(tf, 1, numpy.int32)
            gfile.ne = fread(tf, gfile.npart[0], numpy.float32)
            blockheader2 = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'NE BLOCK')
            if (not ok):
                tf.close()
                return gfile
            if 'ne' in discard:
                gfile.ne=None
                if not quiet:
                    print 'discarded electron number densities'

        if stop_at=='ne':
            tf.close()
            return gfile


        if 'nh' in opt_fields:
            if not quiet:
                print "reading hydrogen number densities..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'NH LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader1 = fread(tf, 1, numpy.int32)
            gfile.nh = fread(tf, gfile.npart[0], numpy.float32)
            blockheader2 = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'NH BLOCK')
            if (not ok):
                tf.close()
                return gfile
            if 'nh' in discard:
                gfile.nh=None
                if not quiet:
                    print 'discarded hydrogen number densities'

        if stop_at=='nh':
            tf.close()
            return gfile


        if 'hsml' in opt_fields:
            if not quiet:
                print "reading smoothing lengths..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'HSML LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader1 = fread(tf, 1, numpy.int32)
            gfile.hsml = fread(tf, gfile.npart[0], numpy.float32)
            blockheader2 = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'HSML BLOCK')
            if (not ok):
                tf.close()
                return gfile
            if 'hsml' in discard:
                gfile.hsml=None
                if not quiet:
                    print 'discarded smoothing lengths'

        if stop_at=='hsml':
            tf.close()
            return gfile


        if 'sfr' in opt_fields:
            if not quiet:
                print "reading SFR..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'SFR LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader1 = fread(tf, 1, numpy.int32)
            gfile.sfr = fread(tf, gfile.npart[0], numpy.float32)
            blockheader2 = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'SFR BLOCK')
            if (not ok):
                tf.close()
                return gfile
            if 'sfr' in discard:
                gfile.sfr=None
                if not quiet:
                    print 'discarded SFR'

        if stop_at=='sfr':
            tf.close()
            return gfile


    if (gfile.npart[4] > 0):

        if 'age' in opt_fields:
            if not quiet:
                print  "reading stars ages..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'AGE LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader1 = fread(tf, 1, numpy.int32)
            gfile.age = fread(tf, gfile.npart[4], numpy.float32)
            blockheader2 = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'AGE BLOCK')
            if (not ok):
                tf.close()
                return gfile

            if 'age' in discard:
                gfile.age=None
                if not quiet:
                    print 'discarded stars ages'

        if stop_at=='age':
            tf.close()
            return gfile


    if (gfile.npart[0]+gfile.npart[4] > 0):

        if 'z' in opt_fields:
            if not quiet:
                print  "reading metallicities..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'Z LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader1 = fread(tf, 1, numpy.int32)
            gfile.z = fread(tf, gfile.npart[0]+gfile.npart[4], numpy.float32)
            blockheader2 = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'Z BLOCK')
            if (not ok):
                tf.close()
                return gfile

            if 'z' in discard:
                gfile.z=None
                if not quiet:
                    print 'discarded metallicities'

        if stop_at=='z':
            tf.close()
            return gfile


    if 'pot' in opt_fields:
        if not quiet:
            print  "reading gravitational potentials..."

        if fformat==2:
            blockheader1, blockheader2 = readBlockLabel(tf)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'POT LABEL')
            if (not ok):
                tf.close()
                return gfile

        blockheader1 = fread(tf, 1, numpy.int32)
        gfile.pot = fread(tf, N, numpy.float32)
        blockheader2 = fread(tf, 1, numpy.int32)
        ok = checkBlockHeaders(blockheader1, blockheader2, 'POT BLOCK')
        if (not ok):
            tf.close()
            return gfile
        if 'pot' in discard_pot:
            gfile.pot=None
            if not quiet:
                print 'discarded potentials'

    if(stop_at=='pot'):
        tf.close()
        return gfile


    if 'acc' in opt_fields:
        if not quiet:
            print  "reading accerations..."

        if fformat==2:
            blockheader1, blockheader2 = readBlockLabel(tf)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'ACC LABEL')
            if (not ok):
                tf.close()
                return gfile

        blockheader1 = fread(tf, 1, numpy.int32)
        gfile.acc = fread(tf, 3*N, numpy.float32).reshape(N,3)
        blockheader2 = fread(tf, 1, numpy.int32)
        ok = checkBlockHeaders(blockheader1, blockheader2, 'ACC BLOCK')
        if (not ok):
            tf.close()
            return gfile
        if 'acc' in discard:
            gfile.acc=None
            if not quiet:
                print 'discarded accerations'

    if stop_at=='acc':
        tf.close()
        return gfile


    if (gfile.npart[0] > 0):

        if 'wind' in opt_fields:
            if not quiet:
                print  "reading winds..."

            if fformat==2:
                blockheader1, blockheader2 = readBlockLabel(tf)
                ok = checkBlockHeaders(blockheader1, blockheader2, 'WIND LABEL')
                if (not ok):
                    tf.close()
                    return gfile

            blockheader1 = fread(tf, 1, numpy.int32)
            gfile.wind = fread(tf, 2*gfile.npart[0], numpy.float32)
            blockheader2 = fread(tf, 1, numpy.int32)
            ok = checkBlockHeaders(blockheader1, blockheader2, 'WIND BLOCK')
            if (not ok):
                tf.close()
                return gfile

            if 'wind' in discard:
                gfile.wind=None
                if not quiet:
                    print 'discarded winds'

        if stop_at=='z':
            tf.close()
            return gfile

    tf.close()
    
    if not quiet:
        print "Everything Done!"

    return gfile


#shortcut to files
def getGadgetFile(model, sim_type, resolution, snap):
    catalog = "/vol/porciani2/data1/emiliord/accretion/data/halo"+str(model)+"_"+str(resolution)
    if sim_type == 'dm':
        catalog = catalog+"_dm"
    elif sim_type == 'all' or sim_type == 'nounbind':
        catalog = catalog+"_baryons"
    else:
        print "sim_type NOT RECOGNISED!"
        return

    catalog = catalog+"/nbody/gdgt_"+str(snap).zfill(3)

    return catalog


## get property from summary file
def getProperty(haloID, resolution, sim_type, prop):
    #sim_type can be 'dm' or 'all'
    #prop is one of 'catalogID', 'Mvir', 'Rvir', 'Nsub', 'Npart', 'center'
    types = {'dm':0, 'all':1, 'nounbind':1}
    column = {'catalogID':[4], 'Mvir':[5], 'Rvir':[6], 'Nsub':[7], 'Npart':[8], 'center':[9,10,11]}

    #check sim_type
    if sim_type != 'dm' and sim_type != 'all' and sim_type != 'nounbind':
        print 'sim_type NOT RECOGNISED!'
        return

    #load summary file
    if sim_type == 'nounbind':
        model,res,stype = numpy.loadtxt('/vol/porciani2/data1/emiliord/accretion/sf/halos_unbind.txt', skiprows=1, usecols=[0,1,3], unpack=True)
        data = numpy.loadtxt('/vol/porciani2/data1/emiliord/accretion/sf/halos_unbind.txt', skiprows=1, usecols=column[prop])
    else:
        model,res,stype = numpy.loadtxt('/vol/porciani2/data1/emiliord/accretion/sf/halos.txt', skiprows=1, usecols=[0,1,3], unpack=True)
        data = numpy.loadtxt('/vol/porciani2/data1/emiliord/accretion/sf/halos.txt', skiprows=1, usecols=column[prop])

    #select correct row
    w, = numpy.where((model == haloID) & (stype == types[sim_type]) & (res == resolution))

    if len(w) == 0:
        print 'halo NOT FOUND!'
        return
    else:
        return data[w][0]

## shortcuts
def getCenter(haloID, res, sim_type):
    return getProperty(haloID, res, sim_type, 'center')

def getRvir(haloID, res, sim_type):
    return getProperty(haloID, res, sim_type, 'Rvir')

def getMvir(haloID, res, sim_type):
    return getProperty(haloID, res, sim_type, 'Mvir')

def printHaloVirialProperties(haloID, res, sim_type):
    Rvir = getRvir(haloID,res,sim_type)/0.679
    Mvir = getMvir(haloID,res,sim_type)*1e11
    vcirc = (4.302e-6 * Mvir / Rvir)**0.5
    Tvir = 0.5*0.59*1.67e-27*vcirc**2*1e6 / 1.3806e-23
    print "halo ",id,"  :  Rvir  = %.2f kpc"%Rvir
    print "                Mvir  = %.2e Msol"%Mvir
    print "                vcirc = %.3e km/s"%vcirc
    print "                Tvir  = %.3e K"%Tvir


def RadialProfile(pos, var, c, Rmin, Rmax, Nbin, dump=False, dumpfile='RDP.txt'):
    '''
     RadialProfile
      produces the *radial* profile of a given variable using logarithmically-spaced bins.
     Usage:
     RadialProfile(pos, var, c, Rmin, Rmax, Nbin, dump=False, dumpfile='RDP.txt')
       pos = [array, shape=(N,3)] array of particle positions
       var = [array, shape=(N)] array of particle variable to profile
       c = [array, shape=(3)] the center of the profile
       Rmin = [float] minimum radius to be considered
       Rmax = [float] maximum radius to be considered
       Nbin = [int] number of bins
       dump = [bool, default=False] write to a file
       dumpfile = [string, default='RDP.txt'] file name for dumping
    '''

    #prepare the bin
    bins=numpy.zeros(Nbin) ##logarithmic binning: ln(Ri/Rmin)=Dr*i => Ri=Rmin*exp(Dr*i)
    width=numpy.log(Rmax/Rmin)/Nbin
    cbins = 0.5*numpy.exp(numpy.arange(Nbin)*width)*(1.0+numpy.exp(width))          ##center of the bins
    vbins = 4.0*math.pi/3.0*(Rmin**3)*numpy.exp(3.0*numpy.arange(Nbin)*width)*(numpy.exp(3.0*width)-1.0)   ##volume of the bins

    N=len(var)

    ## now start binning
    #computing distance between particle and center
    d=((pos[:,0]-c[0])**2+(pos[:,1]-c[1])**2+(pos[:,2]-c[2])**2)**0.5

    ##remove particles outside [Rmin,Rmax)
    w = (d>=Rmin) & (d<Rmax)
    d=d[w]
    vv=var[w]

    #computing to which bin the particle belong to
    b = (numpy.log(d/Rmin)/width).astype(numpy.int)
    b = b[b<Nbin]  #some particles could end up in the first non-existing bin as a consequence of roundings error when computing b. So we remove them.
    for idx,v in zip(b,vv):
        bins[idx] += v
    bins /= vbins
    
    if dump:
        print 'Dumping profile on: ', dumpfile
        fout=open(dumpfile, "w")
        fout.write('#c: '+str(c)+'Rmin: '+str(Rmin)+'   Rmax: '+str(Rmax)+'   Nbin: '+str(Nbin))
        for h in range(Nbin):
            fout.write('%f  %15.5e'%(cbins[h], bins[h]))
        fout.close()

    return cbins, bins



## Plotting

def PlotIntensityMap(pos, prop, center, Radius, outname='/users/egaraldi/Desktop/accretion/intensity_map.png', title=''):
    w, = numpy.where((pos[:,0]-center[0])**2 + (pos[:,1]-center[1])**2 + (pos[:,2]-center[2])**2 < Radius**2)

    p.clf()
    p.scatter(pos[w,2], pos[w,1], s=2, c=prop[w], cmap='hot', lw=0)
    p.colorbar()
    p.xlabel('x (kpc/h)')
    p.ylabel('y (kpc/h)')
    p.title(title)
    p.savefig(outname)
    print 'saved on: ',outname


def PlotHistogram(pos, prop, center, Radius, outname='/users/egaraldi/Desktop/accretion/histogram.png', xlabel='', xlog=False, ylog=False, title='',plot_perc=0):
    # plot_perc : if > 0 plot a dashed line where sum(bins[0:i])==plot_perc%
    w, = numpy.where((pos[:,0]-center[0])**2 + (pos[:,1]-center[1])**2 + (pos[:,2]-center[2])**2 < Radius**2)
    
    print 'max: ', max(prop[w])
    print 'min: ', min(prop[w])

    p.clf()
#    dummy = p.hist(prop[w], bins=1000)
#    counts=dummy[0]*1.0/sum(dummy[0])
#    bins=dummy[1]
    counts, bins = numpy.histogram(prop[w], bins=1000)
    pcounts = counts.astype(numpy.float32)/sum(counts)
#    pcounts = counts.astype(numpy.float32)
    bins = 0.5*(bins[:-1]+bins[1:])
    histo, = p.plot(bins, pcounts, 'b-', ls='steps-mid')

    if plot_perc > 0:
        perc_line = bins[numpy.where(numpy.cumsum(pcounts) >= plot_perc)[0][0]]
        p.vlines(perc_line,0.0,numpy.max(pcounts),linestyles='--',colors='k')

    loc = bins[numpy.argmax(counts)]
    print 'maximum located at: ', loc
#    print 'its bin contains ',100.0*max(counts)/sum(counts),'% of ', sum(counts),' particles'
    print 'its bin contains ',100.0*max(pcounts),'% of ', sum(counts),' particles'

#    p.text(0.9*loc, max(counts), '%.2f kpc/h'%loc)
    p.text(0.9*loc, max(pcounts), '%.2f'%loc)
    p.xlabel(xlabel)
#    p.ylabel('counts')
    p.ylabel('normalized counts')
    if xlog:
        p.xscale('symlog')
    if ylog:
        p.yscale('symlog')

    p.title(title)
    p.savefig(outname)
    print 'saved on: ',outname


def produceSmoothDensityMap(resolution, center, zoom, snapbase, fnr=1, slice_factor=0.1, quiet=False, rotate=False, rotAngle=0.0):
    '''
    produceSmoothDensityMap
     return a 2d numpy array containing the pixel intensity (in arbitrary units)
    Usage: produceSmoothDensityMap(resolution, center, zoom, snapbase, fnr=1, slice_factor=0.1, quiet=False)
     - resolution = [int] number of pixel per dimension in the output
     - center = [array, float, shape=(3)] coordinates (in kpc/h) of the map center
     - zoom = [float] inverse of magnification (e.g. 10x is zoom=0.1)
     - snapbase = [string] (path and) name of the snapshot (without .NUM ext)
     - fnr = [int] number of file(s) per snapshot
     - slice_factor = [float] width of the slice in units of BoxSize
     - quiet = [bool] printing infos?
     - rotate = [bool] Do you want to rotate particles (w.r.t the center) before sampling the density field?
     - rotAngle = [float] rotation angle
    '''

    #coordinates of the center

    #-- snap035 ------
    # 42 (Z110)
    #x0 = 40720.728
    #y0 = 43749.282
    #z0 = 40498.799

    # Marvin (Z110)
    #x0 = 40800.0
    #y0 = 42960.0
    #z0 = 39500.0

    #Zaphod1 (Z7)
    #x0 = 39628.9
    #y0 = 35926.2
    #z0 = 36818.1

    #Zaphod2 (Z7)
    #x0 = 39656.5
    #y0 = 35755.6
    #z0 = 37212.9
    #---------------

    #-- snap025 -----
    #42 (Z110)
    #x0 = 40620.3
    #y0 = 43623.6
    #z0 = 40659.8
    #---------------

    #-- snap015 ---
    #42 (Z110)
    #x0=40556.4
    #y0=43542.5
    #z0=40793.7
    #--------------

    #-- snap005 ---
    #42 (Z110)
    #x0=40497.1
    #y0=43470.1
    #z0=40852.7
    #-------------

    #x0=40000
    #y0=43000
    #z0=40000


    ### Allocate variables
    dens_field_1=numpy.tile(0.01, (resolution,resolution))
    #dens_field_0=numpy.tile(0.0, (resolution,resolution,resolution))

    #centre
    x0 = center[0]
    y0 = center[1]
    z0 = center[2]

    for j in range(fnr):
        ### Read-in particle data
        if (fnr > 1):
            fname=snapbase+"."+str(j+1)
        else:
            fname=snapbase

        if(not quiet):
            print 'Reading particle positions from ', fname

        gdgt = ReadGadget(fname,  fformat=0,  stop_at='vel', quiet=quiet)

        # Define slice thickness
        res = zoom*gdgt.BoxSize/float(resolution)
        hsml=res
        slice_thickness = slice_factor * gdgt.BoxSize

        # define the number of particles of each type
        Ngas  =gdgt.npart[0]
        Nhalo =gdgt.npart[1]
        Ndisk =gdgt.npart[2]
        Nbulge=gdgt.npart[3]
        Nstars=gdgt.npart[4]
        Nbndry=gdgt.npart[5]
        pos   =gdgt.pos
        vel   =gdgt.vel

        by_type = False
        if(by_type):
            ## To make this work, above you should read also the masses. But in this way we save time ##

            if(Ngas > 0):
                xgas=pos[0:Ngas,0]
                ygas=pos[0:Ngas,1]
                zgas=pos[0:Ngas,2]
            if(massarr[0] == 0.0):
                mgas=mass[0:Ngas]
            else:
                mgas=numpy.tile(massarr[0], Ngas)

            if(Nhalo > 0):
                xhalo=pos[Ngas:Ngas+Nhalo,0]
                yhalo=pos[Ngas:Ngas+Nhalo,1]
                zhalo=pos[Ngas:Ngas+Nhalo,2]
            if(massarr[1] == 0.0):
                skip=0
                for t in range(1):
                    if (npart[t] > 0) and (massarr[t] == 0.0):
                        skip += npart[t]
                mhalo=mass[skip:skip+Nhalo]
            else:
                mhalo=numpy.tile(massarr[1], Nhalo)

            if(Ndisk > 0):
                xdisk=pos[Ngas+Nhalo:Ngas+Nhalo+Ndisk,0]
                ydisk=pos[Ngas+Nhalo:Ngas+Nhalo+Ndisk,1]
                zdisk=pos[Ngas+Nhalo:Ngas+Nhalo+Ndisk,2]
            if(massarr[2] == 0.0):
                skip=0
                for t in range(2):
                    if (npart[t] > 0) and (massarr[t] == 0.0):
                        skip += npart[t]
                mdisk=mass[skip:skip+Ndisk]
            else:
                mdisk=numpy.tile(massarr[2], Ndisk)

            if(Nbulge > 0):
                xbulge=pos[Ngas+Nhalo+Ndisk:Ngas+Nhalo+Ndisk+Nbulge,0]
                ybulge=pos[Ngas+Nhalo+Ndisk:Ngas+Nhalo+Ndisk+Nbulge,1]
                zbulge=pos[Ngas+Nhalo+Ndisk:Ngas+Nhalo+Ndisk+Nbulge,2]
            if(massarr[3] == 0.0):
                skip=0
                for t in range(3):
                    if (npart[t] > 0) and (massarr[t] == 0.0):
                        skip += npart[t]
                mbulge=mass[skip:skip+Nbulge]
            else:
                mbulge=numpy.tile(massarr[3], Nbulge)

            if(Nstars > 0):
                xstars=pos[Ngas+Nhalo+Ndisk+Nbulge:Ngas+Nhalo+Ndisk+Nbulge+Nstars,0]
                ystars=pos[Ngas+Nhalo+Ndisk+Nbulge:Ngas+Nhalo+Ndisk+Nbulge+Nstars,1]
                zstars=pos[Ngas+Nhalo+Ndisk+Nbulge:Ngas+Nhalo+Ndisk+Nbulge+Nstars,2]
            if(massarr[4] == 0.0):
                skip=0
                for t in range(4):
                    if (npart[t] > 0) and (massarr[t] == 0.0):
                        skip += npart[t]
                mstars=mass[skip:skip+Nstars]
            else:
                mstars=numpy.tile(massarr[4], Nstars)

            if(Nbndry > 0):
                xbndry=pos[Ngas+Nhalo+Ndisk+Nbulge+Nstars:,0]
                ybndry=pos[Ngas+Nhalo+Ndisk+Nbulge+Nstars:,1]
                zbndry=pos[Ngas+Nhalo+Ndisk+Nbulge+Nstars:,2]
            if(massarr[5] == 0.0):
                skip=0
                for t in range(5):
                    if (npart[t] > 0) and (massarr[t] == 0.0):
                        skip += npart[t]
                mbndry=mass[skip:skip+Nbndry]
            else:
                mbndry=numpy.tile(massarr[5], Nbndry)

        if(not quiet):
            print '...done!'

        if(not quiet):
            print "Select particles to be displayed..."

        #from now on I need only the x and y coord of the particles in the slice box, so:
        ind_slc, = numpy.where(numpy.absolute(pos[:,2] - z0) < 0.5*slice_thickness)
        pos2d = pos[ind_slc, 0:2]
        vel2d = vel[ind_slc, 0:2]
        #Centering the slice
        pos2d[:,0] -= x0-0.5*gdgt.BoxSize
        pos2d[:,1] -= y0-0.5*gdgt.BoxSize
        pos2d[pos2d < 0.0] += gdgt.BoxSize
        pos2d[pos2d > gdgt.BoxSize] -= gdgt.BoxSize

        if(rotate and rotAngle != 0.0):
             #degrees -> radiants
            angle = rotAngle*math.pi/180.0
            #rotation matrix
            rotMatrix = numpy.array([[numpy.cos(angle), -numpy.sin(angle)],[numpy.sin(angle), numpy.cos(angle)]])
            #positions
	    pos2d = rotMatrix.dot(pos2d.T-0.5*BoxSize).T + 0.5*gdgt.BoxSize
        #velocities
	    vel2d = rotMatrix.dot(vel2d.T).T

        ind_img, = numpy.where( (numpy.absolute(pos2d[:,0] - 0.5*gdgt.BoxSize) < zoom*0.5*gdgt.BoxSize) &
                                (numpy.absolute(pos2d[:,1] - 0.5*gdgt.BoxSize) < zoom*0.5*gdgt.BoxSize) )
        pos2d = pos2d[ind_img,:]
        vel2d = vel2d[ind_img,:]
        #print 'Fraction in image:', len(pos2d)/len(pos)

        if(not quiet):
            print 'Binning CDM particles...'

        ImSize = zoom * gdgt.BoxSize

        Bshift = 0.5*(gdgt.BoxSize - ImSize)

        length = len(pos2d)

        for nh in range(length):

            if ((10*nh)%length == 0) and (not quiet):
                print '%i %% done!'%((100*nh)/length)


            nx = int(round( (pos2d[nh, 0]-Bshift) / ImSize * resolution))
            ny = int(round( (pos2d[nh, 1]-Bshift) / ImSize * resolution))

            # Cloud-in-cell assignment
            for ii in [-1,0,1]:           ## previous and next grid points
                for jj in [-1,0,1]:
                    if(nx+ii < 0 or ny+jj < 0 or nx+ii >= resolution or ny+jj >= resolution):
                        continue ## discard grid cells out of the box
                    xg = res*(nx+ii+0.5)
                    yg = res*(ny+jj+0.5)

                    dx = abs(pos2d[nh, 0]-Bshift-xg)/res
                    dy = abs(pos2d[nh, 1]-Bshift-yg)/res

                    r = ((dx**2 + dy**2)**0.5)/hsml

                    if(r <= 1.0):
                        dens_field_1[nx+ii,ny+jj] += 10.0/(7.0*math.pi*hsml**2) * (1.0 - 6.0*r**2 + 6.0*r**3)  ## mass assignment
                    elif(r <= 2.0):
                        dens_field_1[nx+ii,ny+jj] += 10.0/(7.0*math.pi*hsml**2) * (2.0*(1-r)**3)  ## mass assignment

        print '2D Velocity of the CM of particles inside pic:'
        print vel2d.sum(axis=0)/len(vel2d)
        if(not quiet):
            print '...done!'

    return dens_field_1


def myCMap(name):

    '''
    myCMap
     return a custom colormap
    Usage: myCMap(name)
     - name = [string] name of the colormap to return. Actually implemented:
                       'grey-to-yellow', 'purple-to-yellow', 'red-to-yellow', 'baryons'
    #for the implemented cmaps: http://matplotlib.org/examples/color/colormaps_reference.html
    '''

    my_cmap=None

    if(name == 'grey-to-yellow'):
        ## grey-to-yellow color table
        cdict = { 'red'   : ((0.0, 0.5, 0.5),     #(x, y0, y1)
                             (1.0, 1.0, 1.0)),    # for each range between x[i] and x[i+1] (x of the next tuple), the
                  'green' : ((0.0, 0.5, 0.5),     # color is interpoated betwee y1[i] and y0[i+1]
                             (1.0, 1.0, 1.0)),    # => if you have no discontinuities you have y0=y1 and the interpolation between y[i] and y[i+1]
                  'blue'  : ((0.0, 0.5, 0.5),
                             (1.0, 0.0, 0.0))
                }
        my_cmap = LinearSegmentedColormap('GreyToYellow', cdict)

    elif(name == 'purple-to-yellow'):
        ## purple-to-yellow color table
        cdict = { 'red'   : ((0.0, 0.0   , 0.0),
                             (0.5, 0.5   , 0.5),
                             (1.0, 1.0   , 1.0)),
                  'green' : ((0.0, 0.0   , 0.0),
                             (0.5, 0.4167, 0.4167),
                             (1.0, 0.8333, 0.8333)),
                  'blue'  : ((0.0, 0.0   , 0.0),
                             (0.5, 1.0   , 1.0),
                             (1.0, 0.0   , 0.0))

                }
        my_cmap = LinearSegmentedColormap('PurpleToYellow', cdict)

    elif(name == 'red-to-yellow'):
        ## red-to-yellow color table
        cdict = { 'red'   : ((0.0, 0.0, 0.0),
                             (0.5, 0.5, 0.5),
                             (1.0, 1.0, 1.0)),
                  'green' : ((0.0, 0.0, 0.0),
                             (0.5, 0.0, 0.0),
                             (1.0, 1.0, 1.0)),
                  'blue'  : ((0.0, 0.0, 0.0),
                             (0.5, 0.0, 0.0),
                             (1.0, 0.0, 0.0))

                }
        my_cmap = LinearSegmentedColormap('RedToYellow', cdict)

    elif(name == 'baryons'):
        ## baryon color table
        cdict = { 'red'   : ((0.0, 0.0, 0.0),
                             (0.5, 0.0, 0.0),
                             (1.0, 1.0, 1.0)),
                  'green' : ((0.0, 0.0, 0.0),
                             (0.5, 0.5, 0.5),
                             (1.0, 1.0, 1.0)),
                  'blue'  : ((0.0, 0.0, 0.0),
                             (0.5, 0.5, 0.5),
                             (1.0, 1.0, 1.0))

                }
        my_cmap = LinearSegmentedColormap('Baryon', cdict)

    return my_cmap


def drawSmoothDensityMap(dens_field_1, fout='only_plot_output.eps', cmap=p.get_cmap('jet'),
		                 clipping=True, ma_ref=-1, ma_ref_frac=2.0, mi_frac_ma=300.0,
		                 profile1d=False, p1dLength=0, p1dWidth=0, p1dAlpha=0.0, p1dCentre=[],
			             arrow=False, arrowOrigin=[], arrowDirection=[], arrowLength=0):
    '''
    drawSmoothDensityMap
     actually draws the density map produced by produceSmoothDensityMap
     - usage; drawSmoothDensityMap(dens_field_1, fout='only_plot_output.eps', ma_ref=-1, ma_ref_frac=2.0, mi_frac_ma=300.0, clipping=True, profile1d=False, p1dLength=0, p1dWidth=0, p1dAlpha=0.0, Varrow=False, VCM=[])
     - dens_field_1 = [array, float, shape=(resolution, resolution)] the output of produceSmoothDensityMap
     - fout = [string] output (path and) name
     - cmap = [Matplotlib ColorMap Object] the colormap to use.
     - clipping = [bool] pixel clipping
     - ma_ref = [float] maximum value for pixel clipping (every pixel above ma_ref will be set to ma_ref. If < 0, the clipping will be computed as a fixed fraction of max(dens_field_1)
     - ma_ref_frac = [float] if ma_ref < 0, then ma_ref = max(dens_field_1)/ma_ref_frac
     - mi_frac_ma = [float] minimum value for pixel clipping as a fraction of ma_ref. mi = ma_ref/mi_frac_ma
     - profile1d = [bool] Do you want a 1d profile under the map?
     - p1dLength = [int] length of the 1d profile in pixel
     - p1dWidth = [int] width of the 1d profile in pixel
     - p1dAlpha = [float] angle between the profile slit and the x axis of the 1d profile in pixel
     - p1dCentre = array, float, shape=(2)] centre of the profile slit
     - arrow = [float] Do you want an arrow from the centre of the plot?
     - arrowOrigin = [array, int, shape=(2)] Arrow origin in pixel
     - arrowDirection = [array, float, shape=(2)] Arrow direction
     - arrowLength = [int] Arrow length in pixel
    '''

    #dens_plot = False
    #if(dens_plot):
    #    densplot=numpy.sum(dens_field_0[:,:,500:550], axes = 2)   ## 100 Mpc/h


    Map = numpy.copy(dens_field_1).T
    #Map = numpy.copy(dens_field_old)
    resolution=len(dens_field_1)

    if(ma_ref <= 0.0):
        ma_ref = numpy.amax(Map)#2.0
#ma_ref = 8.90202/2.0
        print 'computed ma_ref: ',ma_ref
#ma_ref=4.42035*4  ## Run_2048_Z110 with z_thicknes = 0.1*BoxSize
#ma_ref=19.4174	## Run_2048_Z7 with z_thicknes = 0.1*BoxSize
    ma= ma_ref
    mi= ma/mi_frac_ma

    ## now do clipping in Map
    if(clipping):
        Map[Map < mi]=mi
        Map[Map > ma]=ma

    #plotting
    p.clf()

    #fig, (ax1, ax2) = p.subplots(2)
    fig=p.figure()
    if(profile1d):
        gs = gridspec.GridSpec(2,1, width_ratios=[1, 0.8], height_ratios=[1, 0.3])
        ax1=p.subplot(gs[0])
        ax2=p.subplot(gs[1])
    else:
        gs = gridspec.GridSpec(1,1)
        ax1=p.subplot(gs[0])

    #density map
    ax1.imshow(numpy.log10(Map), interpolation='nearest', cmap=cmap)
    ax1.xaxis.set_ticklabels([])
    ax1.yaxis.set_ticklabels([])

    #arrow
    if(arrow and arrowOrigin and arrowDirection and arrowLength > 0):
        modv = sum(numpy.array(arrowDirection)**2)**0.5
        dx = arrowDirection[0]*arrowLength/modv
        dy = arrowDirection[1]*arrowLength/modv
        ax1.arrow(arrowOrigin[0],arrowOrigin[1],dx,dy,head_width=2.5, head_length=5, width = 1, fc='r', ec='r')

    #now subplot with the 1d profile

    def LineProfile(point1, point2, num=1000):
        x0, y0 = point1[0], point1[1] # These are in *pixel* coordinates!!
        x1, y1 = point2[0], point2[1]
        x, y = numpy.linspace(x0, x1, num), numpy.linspace(y0, y1, num)
        #zi = Map[x.astype(numpy.int), y.astype(numpy.int)]
        zi = dens_field_1[x.astype(numpy.int), y.astype(numpy.int)]
        return zi

    def SliceProfile(point1, point2, point3, point4, num=1000):
        point1=numpy.array(point1)
        point2=numpy.array(point2)
        point3=numpy.array(point3)
        point4=numpy.array(point4)
        num_short = round(num * sum((point4-point1)**2)**0.5/sum((point2-point1)**2)**0.5)
        num_short = max(1,num_short)
        profile = numpy.zeros(num)
        for i in numpy.arange(num_short+1):
            new_point1 = point1+i/num_short*(point4-point1)
            new_point2 = point2+i/num_short*(point3-point2)
            profile += LineProfile(new_point1, new_point2, num=num)
        return profile

    def DrawRect(ax, point1, point2, point3, point4, num=1000):
        ax.plot([point1[0], point2[0], point3[0], point4[0], point1[0]], [point1[1], point2[1], point3[1], point4[1], point1[1]], 'b-')
        return

    def GetRectPts(length, width, alpha, center=[]):
	##
	# !! In imshow the y axis is reversed (i.e. *top* left corner is 0,0), but here I assume a normal coordinate system in input
	#     so I need to reverse the y axis
        #alpha = 90 - alpha
        ##
        if not center:
            center=[0.5*length, 0.5*length]
        alpha *= math.pi / 180.0 #convert to radiant
        p14=[center[0]+0.5*length*math.cos(alpha), center[1]-0.5*length*math.sin(alpha)]
        p23=[center[0]-0.5*length*math.cos(alpha), center[1]+0.5*length*math.sin(alpha)]
        point1 = [p14[0] + 0.5*width*math.sin(alpha), p14[1] + 0.5*width*math.cos(alpha)]
        point2 = [p23[0] + 0.5*width*math.sin(alpha), p23[1] + 0.5*width*math.cos(alpha)]
        point3 = [p23[0] - 0.5*width*math.sin(alpha), p23[1] - 0.5*width*math.cos(alpha)]
        point4 = [p14[0] - 0.5*width*math.sin(alpha), p14[1] - 0.5*width*math.cos(alpha)]
        return point1, point2, point3, point4

    if(profile1d and p1dLength > 0 and p1dWidth > 0):
        point1, point2, point3, point4 = GetRectPts(p1dLength, p1dWidth, p1dAlpha, center=p1dCentre)  # halo42, z=4
        #point1, point2, point3, point4 = GetRectPts(resolution, 1, 114, center=[resolution/2 + 5, resolution/2])  #halo42, z=5
        #point1, point2, point3, point4 = GetRectPts(resolution, 1, 114)  #halo42, z=5
        #point1, point2, point3, point4 = GetRectPts(resolution, 1, 135)  #halo42, z=[6, 7]

        ax1.autoscale(False)
        DrawRect(ax1, point1, point2, point3, point4)
	sprof = SliceProfile(point1, point2, point3, point4, num=p1dLength)
        ax2.plot(numpy.arange(len(sprof)), sprof)

        #ax1.plot([resolution/2, resolution/2],[0,resolution])
        #ax2.plot(numpy.exp(LineProfile([resolution/2, 0], [resolution/2, resolution-1], num=128)))

        #ax2.plot(numpy.sum(Map, axis = 1), 'b-')

        ax2.set_xlim([0,resolution])
        ax2.xaxis.set_ticklabels([])
        ax2.yaxis.set_ticklabels([])

        fig.subplots_adjust(hspace=0)


    #fig.savefig(fout, format='eps', dpi=1200)
    fig.savefig(fout, format='eps', dpi=1200, bbox_inches='tight')
    print 'writed on: ',fout


## other

def computeCM(haloID, sim_type, center, Radius):
    updmass = numpy.concatenate([mass[0:npart[0]], massarr[1]*numpy.ones(npart[1]), mass[npart[0]:]])
    #particles inside halo
    w, = numpy.where((pos[:,0]-center[0])**2 + (pos[:,1]-center[1])**2 + (pos[:,2]-center[2])**2 < Radius**2)
    CMhalo = ((pos[w,:].T * updmass[w]).T).sum(axis=0)/sum(updmass[w])
    #particles of central galaxy
    w, = numpy.where((pos[:,0]-center[0])**2 + (pos[:,1]-center[1])**2 + (pos[:,2]-center[2])**2 < 0.01*Radius**2)
    CMgal = ((pos[w,:].T * updmass[w]).T).sum(axis=0)/sum(updmass[w])
    print ''
    print 'halo '+str(id)
    print '  CM halo =           ', CMhalo,' kpc/h'
    print '  CM central galaxy = ', CMgal,' kpc/h'
    print '  --> offset = ', (sum((CMhalo-CMgal)**2))**0.5,' kpc/h'


def logbin(q, binsWidth=0.1, centering=False, nonzero=False):
  #produce logarithmic bins
  Nbins = numpy.ceil(numpy.log10(max(q)/min(q))/binsWidth)
  bins = numpy.logspace(numpy.log10(min(q)), numpy.log10(min(q))+Nbins*binsWidth, Nbins+1, endpoint=True)
  counts, edges = numpy.histogram(q, bins=bins)
  
  if nonzero:
    counts = numpy.maximum(1e-32,counts) #nicer plot
  if centering:
    bins = 0.5*(bins[1:]+bins[:-1])  #center bins

  return numpy.log10(bins), counts

"""
def makeSnapRedshiftList(base, start, end, outfile):
  tf = open(outfile,"w")
  tf.write("#snap redshift age(Gyr)\n")
  for i in range(start, end+1):
    print 'snap %i/%i...\n'%(i-start+1, end-start+1)
    ReadGadget(base+'_'+str(i).zfill(3), fformat=0, longids=False, discard=[], opt_fields=[], stop_at='header', quiet=True)
    tf.write("%i  %f  %f\n"%(i,redshift,age_universe(time,HubbleParam,Omega0,OmegaLambda)/1e9))
  tf.close()
  """
