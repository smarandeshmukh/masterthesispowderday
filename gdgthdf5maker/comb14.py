from mpi4py import MPI
#from pygadgetreader import *
import sys
import os
import h5py
import numpy as np
import gadget_utils as gread


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def main(argu):

	conv_dict = {	'gas':'PartType0',
					'dm':'PartType1',
					'disk':'PartType2',
					'bulge':'ParType3',
					'star':'PartType4',
					'bndry':'PartType5',
					'pos':'Coordinates',
					'vel':'Velocities',
					'ids':'ParticleIDs',
					'mass':'Masses',
					'u':'InternalEnergy',
					'rho':'Density',
					'age':'StellarFormationTime',
					'z':'Metallicity',
					'nh':'NeutralHydrogenAbundance',
					'hsml':'SmoothingLength'
				}

	#Process 0 is master node. Initialize and verify all user inputs. It will also do all the writing
	if rank == 0:
		#Verify user inputs
		try:
			folname = argu[1]
			fieldtxt = argu[2]
			outhdf5 = argu[3]
			nstart = int(argu[4])
			nend = int(argu[5])
			itno = int(argu[6])
		except:
			raise AssertionError("Not enough inputs/wrong input types")
		if not os.path.exists(folname):
			raise IOError(folname + " does not exist")


		#Print user inputs
		print "----------------------------"
		print "User inputs:"
		print "Binary folder:", folname
		print "Fields file:", fieldtxt
		print "hdf5 base name:", outhdf5
		print "Processing files:", nstart, "to", nend
		print "Iteration number:", itno
		print "Number of work processors:", size

		#Create the hdf5 chunk. Later all chunks will be combined in one master file
		hdf5chunk = (os.path.basename(outhdf5)).rsplit(".", 1)[0] + str(itno) + ".hdf5"
		hdf5chunk = os.path.join(os.path.dirname(outhdf5),  hdf5chunk)
		print "Writing to file:", hdf5chunk
		if os.path.exists(hdf5chunk):
			print "Removing existing:", hdf5chunk
			os.remove(hdf5chunk)
		o = h5py.File(hdf5chunk)

		#See what particles exist and accordingly create the groups
		headerbin = os.path.join(folname, os.listdir(folname)[nstart])
		print "Reading header from:", headerbin
		tot_parts = gread.ReadGadget(headerbin, fformat = 0, quiet=True).npartTotal
		for i in range(len(tot_parts)):
			if tot_parts[i]:
				o.create_group("PartType" + str(i))

		#Get names of data fields
		f = open(fieldtxt, 'r')
		lines = f.readlines()
		f.close()
		numfields=len(lines)
		send_counter = 0 #Number of fields sent for processing
		compl_counter = 0 #Number of fields that have been processed
		actv_proc = 0 #Number of active processors that will be used

		#Send first batch of data
		print "Number of fields to write", numfields
		for i in range(1, size):
			if i<=numfields:
				comm.send([send_counter,nstart,nend,folname,fieldtxt], dest = i, tag = 1)
				send_counter += 1
			else:
				comm.send([0,0,0], dest = i, tag = 999)

		actv_proc = send_counter

		#Keep sending fields to processors until completion
		while compl_counter != numfields:
			info_recv = MPI.Status()
			compl = comm.recv(source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG, status=info_recv)
			
			#write dataset to file
			l = lines[compl[1]-1].split('/')
			gname = l[0]
			dname = l[1].rstrip()
			o[ conv_dict[gname] ].create_dataset( conv_dict[dname], data=compl[0] )
			
			#update completion counter and send new data set to the jobless processor
			compl_counter += 1
			print "Receiving from: " + str(info_recv.Get_source()) + " lineno: " + str(compl[1]) + "=" + gname + dname
			if send_counter != numfields:
				comm.send([send_counter,nstart,nend], dest = info_recv.Get_source(), tag = 1)
				send_counter += 1
		
		#Send termination signal to all processes
		for i in range(1, actv_proc+1):
			comm.send([1, nstart, nend], dest = i, tag = 999)

		print "End of rank 0"
		o.close()

	else:
		info = MPI.Status()
		post = comm.recv(source = 0, tag = MPI.ANY_TAG, status = info)
		
		#Make 'extra' processors sleep
		if info.Get_tag() == 999:
			print "Process is: " + str(rank) + " is redundant"
		else:
			#Get info about which files to process, and directory details
			info2 = MPI.Status()
			fieldnum = post[0] + 1
			fstart = post[1]
			fend = post[2]
			dirname = post[3]
			flist = os.listdir(dirname)[fstart:fend+1]
			f = open(post[4], 'r')
			lines = f.readlines()
			f.close()
			
			#keep processing until kill signal
			while info2.Get_tag() != 999:
				#Extract data of field
				l = lines[fieldnum-1].split('/')
				gname = l[0]
				partp = int(conv_dict[gname][-1])
				dname = l[1].rstrip()
				first_entry = True
				dataset = 0
				#print gname, dname
				for i in flist:
					fpath = os.path.join(dirname, i)
					gnpart = gread.ReadGadget(fpath, fformat = 0, quiet = True).npart
#					print fpath, gnpart
					
					data = None
					#all particles types have this field
					if dname in ('pos', 'vel', 'mass', 'ids'):
						if gnpart[partp] != 0:
							pstart = sum(gnpart[:max(0,partp)])
							pend = sum(gnpart[:partp + 1])
							#print pstart, pend
							data = getattr(gread.ReadGadget(fpath, fformat = 0, quiet = True, stop_at = dname), dname)[pstart:pend]
					
					#fields which are UNIQUE to gas (partp = 0)
					elif dname in ('u', 'rho', 'hsml'):
						if partp == 0:
							if gnpart[partp] != 0:
								pstart = 0
								pend = sum(gnpart[:1])
								data = getattr(gread.ReadGadget(fpath, fformat = 0, quiet = True, opt_fields = ['u', 'rho', 'hsml'], stop_at = dname), dname)[pstart:pend]

					#fields which are UNIQUE to stars (partp = 4)
					elif dname in ('age'):
						if partp == 4:
							if gnpart[partp] != 0:
								pstart = 0
								pend = gnpart[partp]
#								print pstart, pend, gname, dname
								data = getattr(gread.ReadGadget(fpath, fformat = 0, quiet = True, opt_fields = ['u', 'rho', 'hsml', 'age'], stop_at = dname), dname)[pstart:pend]
#								print data

					#fields which BOTH stars and gas have
					elif dname in ('z'):
						if partp in [0,4]:
							if gnpart[partp] != 0:
								gnpartmod = gnpart
								gnpartmod[1] = 0
								gnpartmod[2] = 0
								gnpartmod[3] = 0
								gnpartmod[5] = 0
								pstart = sum(gnpartmod[:max(0,partp)])
								pend = sum(gnpartmod[:partp + 1])
								data = getattr(gread.ReadGadget(fpath, fformat = 0, quiet = True, opt_fields = ['u', 'rho', 'hsml', 'age', 'z'], stop_at = dname), dname)[pstart:pend]
#								print data, pstart, pend


					if first_entry and data != None:
						dataset = data
						first_entry = False
					elif data != None:
						dataset = np.append(dataset, data, axis = 0)

				#Finally send it back to root
				comm.send([dataset,fieldnum], dest = 0, tag = 2)
				fieldnum = comm.recv(source = 0, tag = MPI.ANY_TAG, status = info2)[0] + 1

if __name__ == '__main__':
	if len(sys.argv) > 1:
		main(sys.argv)
	elif rank==0:
		print "Not enough arguments"
