#!/bin/bash

binfolder='/vol/aibn1058/data1/sdeshmukh/sim_data/512DYN3_output00090'
fieldfile='/vol/aibn1058/data1/sdeshmukh/sim_data/field_names.txt'
hdf5name='/vol/aibn1058/data1/sdeshmukh/sim_data/small.hdf5'
totchunks=15
files=(`ls $binfolder`)

echo "Your binary folder: $binfolder"
echo "Your text file: $fieldfile"
echo "Number of files: ${#files[*]}"
echo "Number of chunks: $totchunks"

export start=0
for i in `seq 1 $(($totchunks-0))`;
do
    end=`bc <<< "scale=1; ($i*${#files[*]}-1)/$totchunks"`
#	end=`bc <<< "scale=1; ($i*10)/$totchunks"`
	end=`printf "%.0f" $end`
#	echo $start $end

	mpiexec -n 12 python comb14.py $binfolder $fieldfile $hdf5name $start $end $i
	start=$(($end+1))
done

python mergehdf5_1.py $binfolder $fieldfile $hdf5name $totchunks
