import h5py
import sys
import os
import astropy.cosmology as ac
import numpy as np
from mpi4py import MPI

def main():
	try:
		hdf5name = sys.argv[1]
		out = sys.argv[2]
	except IndexError:
		print "Not enough arguments."
		print "Usage:"
		print "[script name] [hdf5 file name] [outputfile]"
		sys.exit()

	if not os.path.exists(hdf5name):
		raise TypeError("Input file does not exist.")

	def get_age_at_z(redshift):
		
		H0 = f['Header'].attrs['HubbleParam']
		OmegaLambda = f['Header'].attrs['OmegaLambda']
		Omega0 = f['Header'].attrs['Omega0']
		cosmo = ac.LambdaCDM(100*H0, Omega0, OmegaLambda)
#		print "Your Cosmology:"

#		print "H0 =", cosmo.H0, "Omega_m=", cosmo.Om0, "OmegaLambda=", cosmo.Ode0
		return cosmo.lookback_time(redshift).value

	def get_scale_factor(age):
		redshift =  ac.z_at_value(get_age_at_z, age)
		return 1/(1+redshift)

	#Print user inputs
	print "Your hdf5 file is:", hdf5name

	#Open file
	f = h5py.File(hdf5name)

	#Check if ages exist for stars
	try:
		f['PartType4']['StellarFormationTime']
	except:
		print 'The block "PartType4/StellarFormationTime" does not exist'

	print list(f['Header'].attrs)

	H0 = f['Header'].attrs['HubbleParam']
	OmegaLambda = f['Header'].attrs['OmegaLambda']
	Omega0 = f['Header'].attrs['Omega0']
	print get_age_at_z(20) - get_age_at_z(3)

	a = f['PartType4']['StellarFormationTime'][:]
	b = 2*a

	print type(b), len(b)
	print np.amax(a)

	print get_scale_factor(8 )
	age_in_sim = get_age_at_z(f['Header'].attrs['Redshift'])
	print "age in sim:", age_in_sim
	c = np.array([get_scale_factor(age + age_in_sim) for age in a[:5] ])
	print len(c), type(c), c[:10]

	f.close()

if __name__ == "__main__":
	main()
	print __name__
