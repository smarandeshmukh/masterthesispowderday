import numpy as np
import h5py
import sys
import os
import gadget_utils as gread
import inspect

def main():
	if len(sys.argv) != 5:
		raise IOError("Not enough inputs")

	bindir = sys.argv[1]
	fieldtxt = sys.argv[2]
	hdf5 = sys.argv[3]
	chunks = int(sys.argv[4])

	print "Your base name of your hdf5 files is:", hdf5
	print "Number of hdf5 files:", chunks

	if os.path.exists(hdf5):
		print "Removing existing hdf5 master file..."
		os.remove(hdf5)

	#Use header from first binary file
	file1name = os.path.join(bindir, os.listdir(bindir)[0])
	gdgt = gread.ReadGadget(file1name, fformat=0, stop_at='header', quiet=True)
	print "Reading header from", file1name, ". You header contains (some fields will be modified):"
	headkeys = [item for item in vars(gdgt) if getattr(gdgt, item)!= None]
	print headkeys

	#Access hdf5 files and verify their existence
	hdf5base = hdf5.rsplit('.', 1)[0]
	hdf5files = [hdf5base + str(num) + ".hdf5" for num in range(1, chunks+1)]
	for f in hdf5files:
		if not os.path.exists(f):
			raise IOError(f + " does not exist.")

	#Verify total number of particles match npartTotal
	print "Verifying that sum of particles in different files match npartTotal..."
	print "npartTotal =", getattr(gdgt, 'npartTotal')
	print getattr(gdgt, 'redshift')
	numpart = np.array([0,0,0,0,0,0])
	for f in hdf5files:
		fhdf5 = h5py.File(f)
		for ptype in range(0,6):
			string = 'PartType' + str(ptype)
			if string in fhdf5.keys():
				numpart[ptype] += fhdf5[string]['ParticleIDs'].shape[0]
	fhdf5.close()
	if not np.array_equiv(numpart, getattr(gdgt, 'npartTotal')):
		print "WARNING: npartTotal (from header) does not match sum of particles in the hdf5 files."
		print "Header says:", getattr(gdgt, 'npartTotal'), "Files say:", numpart

	#Initialise final file
	o = h5py.File(hdf5)
	o.create_group('Header')
	#Create particle groups
	for i in range(6):
		if getattr(gdgt, 'npartTotal')[i]:
			o.create_group("PartType" + str(i))

	#Write header

	o['Header'].attrs['NumPart_ThisFile'] = numpart
	o['Header'].attrs['Flag_Sfr'] = getattr(gdgt, 'flag_sfr')[0]
	o['Header'].attrs['MassTable'] = getattr(gdgt, 'massarr')
	o['Header'].attrs['Flag_StellarAge'] = getattr(gdgt, 'flag_age')[0]
	o['Header'].attrs['Redshift'] = getattr(gdgt, 'redshift')[0]
	o['Header'].attrs['Time'] = getattr(gdgt, 'time')[0]
	o['Header'].attrs['BoxSize'] = getattr(gdgt, 'BoxSize')[0]
	o['Header'].attrs['Omega0'] = getattr(gdgt, 'Omega0')[0]
	o['Header'].attrs['HubbleParam'] = getattr(gdgt, 'HubbleParam')[0]
	o['Header'].attrs['Flag_Cooling'] = getattr(gdgt, 'flag_cooling')[0]
	o['Header'].attrs['NumPart_Total_HighWord'] = getattr(gdgt, 'npartTotalHW')
	o['Header'].attrs['Flag_Feedback'] = getattr(gdgt, 'flag_feedback')[0]
	o['Header'].attrs['Flag_Metals'] = getattr(gdgt, 'flag_metal')[0]
	o['Header'].attrs['OmegaLambda'] = getattr(gdgt, 'OmegaLambda')[0]
	o['Header'].attrs['NumPart_Total'] = numpart
	o['Header'].attrs['Flag_DoublePrecision'] = 0
	o['Header'].attrs['NumFilesPerSnapshot'] = 1

	#Create empty datasets with appropriate size
	samplehdf5 = h5py.File(hdf5files[0])
#	for gname in samplehdf5.keys():
#		print gname
#		print samplehdf5[gname].keys()
#		for dname in samplehdf5[gname].keys():
#			print "\t" + dname
#			print samplehdf5[gname].keys()
#			o[gname].create_dataset(dname, shape = samplehdf5[gname + '/' + dname].shape)
#	dname = samplehdf5[gname].keys()[0]
#	print dname
#	print samplehdf5[gname + '/' + dname].shape
	
	
#	o['PartType0']['Masses'] = np.array([1,2,3])

	#Append data
	print o.keys()
	for gname in samplehdf5.keys()[0:]:
		for dname in samplehdf5[gname].keys():
			path =  samplehdf5[gname][dname].name
			print path
			first_entry = True
			for fname in hdf5files:
				print "\t" + fname
				f = h5py.File(fname, 'r')
				if first_entry:
					arr = f[path][:]
					first_entry = False
				else:
					t = f[path][:]
					arr = np.append(arr, t, axis = 0)
				f.close()
			print "Writing", path,  "..."
			o[gname].create_dataset(dname, data = arr)
#	print arr, arr.shape
	o.close()


if __name__=="__main__":
	main()
